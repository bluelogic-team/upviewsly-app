// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBOcwKjHMMcyqwvzVjhA43VB_MCE6mTMo4",
    authDomain: "upviewsly-9edda.firebaseapp.com",
    databaseURL: "https://upviewsly-9edda.firebaseio.com",
    projectId: "upviewsly-9edda",
    storageBucket: "upviewsly-9edda.appspot.com",
    messagingSenderId: "778010261535",
    appId: "1:778010261535:web:bf4f93d4beafe814"
  },
  GOOGLE_WEB_CLIENT_ID : "778010261535-dj1jn03i5eip09damhb4mgro9mohpfh4.apps.googleusercontent.com",
  FCM_KEY : "AAAAtSUGeB8:APA91bF7onTjjMRuTQR6gd_qupXrRgto0k09uD24e0YXbPz8sMc4kchvAVIEUsi4b0MIitMmsBheAoQ_AGmzOSfdv5e1fMOlDAMFi3g7wlYgR9hth2Z_g-JQDkDXcAQOKPmYjD5QAv67",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
