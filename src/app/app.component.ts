import { Component } from '@angular/core';
import { Platform, ToastController, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from "@ngx-translate/core";

import { AuthService } from './providers/auth/auth.service';

import { Router } from '@angular/router';
import { NotificationsService } from './providers/notifications/notifications.service';
import { FcmService } from './providers/fcm/fcm.service';

import { Network } from '@ionic-native/network/ngx';
import { ChatService } from './providers/chat/chat.service';
import { OnboardingInfoPage } from './pages/onboarding-info/onboarding-info.page';
import { Location } from '@angular/common';
import { Badge } from '@ionic-native/badge/ngx';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {

    _toast: any;
    isLostConnection: boolean = false;
    unreadCount: number = 0;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private router: Router,
        private authService: AuthService,
        private globalization: Globalization,
        private translateService: TranslateService,
        private notifService: NotificationsService,
        private fcmService: FcmService,
        private network: Network,
        private toast: ToastController,
        private chatService: ChatService,
        private modalCtrl: ModalController,
        private location: Location,
        private badge: Badge,
    ) {
        this.initializeApp();
        /*
        //import in page.module
        import { TranslateModule } from '@ngx-translate/core';
        
        //to translate in html
        {{'login.TITLE' | translate}}
        
        //import in page.ts
        import { TranslateService } from "@ngx-translate/core";
        constructor(private translate: TranslateService){}
        //to translate from page.ts
        this.translate.instant('login.TITLE')
        
        */

        this.platform.resume.subscribe(() => {
            this.badge.clear();
        });
    }

    ngOnDestroy() {
        this.notifService.countObservable.unsubscribe();
    }

    async initializeApp() {
        this.platform.ready().then(async () => {

            document.addEventListener('backbutton', () => {
                if(this.router.url === '/login' || this.router.url === '/register') {
                    navigator['app'].exitApp();
                } else {
                    this.location.back();
                }

            }, false)
           

            if (this.platform.is('android')) {
                // let status bar overlay webview
                this.statusBar.overlaysWebView(false);
                this.statusBar.backgroundColorByHexString('#820080');
            }
            if (this.platform.is('ios')) {
                // let status bar overlay webview
                this.statusBar.overlaysWebView(false);
                this.statusBar.backgroundColorByHexString('#820080');
            }
            //translation
            if (this.platform.is("cordova")) {
                this.globalization.getPreferredLanguage()
                    .then(res => {
                        let langSplit = res.value.split('-');
                        if (langSplit[0] == 'fr') {
                            this.initLang('fr');
                        } else if (langSplit[0] == "en") {
                            this.initLang('en');
                        } else {
                            this.initLang('fr');
                        }
                    })
                    .catch(e => console.log('error globalization', e));
            } else {
                let userLang = navigator.language.split('-');
                if (userLang[0] == "fr" || userLang[0] == "en") {
                    this.initLang(userLang[0]);
                } else {
                    this.initLang("fr");
                }
            }

            this.badge.clear();

            // Define time to display toast if connection is none
            setTimeout(() => {
                if (this.network.type === this.network.Connection.NONE) {
                    this.isLostConnection = true;
                    this.translateService.get('init').subscribe(async (text: string) => {
                        if (text) {
                            this._toast = await this.toast.create({
                                message: await this.translateService.instant('common.LOST_CONNECTION')
                            });
                            this._toast.present();
                        }
                    })
                }
            }, 3000)

            // Check network
            this.authService.networkOnDisconnect = this.network.onDisconnect().subscribe(async () => {
                this.isLostConnection = true;
                this._toast = await this.toast.create({
                    message: this.translateService.instant('common.LOST_CONNECTION')
                });
                this._toast.present();
            });

            // watch network for a connection
            this.authService.networkOnConnect = this.network.onConnect().subscribe(() => {
                if (this.isLostConnection) {
                    this.isLostConnection = false;
                    this._toast && this._toast.dismiss();
                }
            });

            //Auth check and redirect
            this.authService.getCurrentUser()
                .then(res => {
                    if (res) { //already logged

                        // Restart the listenner of notifications if user reload the page
                        if (typeof this.notifService.countObservable === "undefined") {
                            this.notifService.countObservable = this.notifService.countUserNotifications().subscribe(
                                (res: any) => {
                                    return this.notifService.countNotify = res.length;
                                }
                            )
                        }
                        // Initialize token to listen notification
                        this.fcmService.getToken(this.authService.getCurrentUserId());

                        // Listen notification with tokenId
                        this.fcmService.getPushNotification();
                        this.splashScreen.hide();

                        this.chatService.unreadMessagesObservable = this.chatService.getUnreadMessages().subscribe(res => {
                            res.forEach((e: any) => {
                                this.chatService.unreadLastMessageObservable = this.chatService.getLastMessage(e.id).subscribe(lastMessage => {
                                    lastMessage.forEach(last => {
                                        this.chatService.lastReadObservable = this.chatService.getChatLastRead(e.id).subscribe(read => {
                                            // Check that the element does not exist yet to increment the iterator of + 1
                                            if (typeof read.payload.data() !== 'undefined') {
                                                if (typeof read.payload.data()['timestamp'] !== 'undefined') {
                                                    if (read.payload.data()['timestamp'] < last.createdAt) {
                                                        if (last.uid !== this.authService.getCurrentUserId()) {
                                                            if (!this.chatService.unreadChatsList.includes(e.id)) {
                                                                this.chatService.unreadChatsList.push(e.id);
                                                                this.chatService.unreadMessages = this.chatService.unreadChatsList.length;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                    })
                                })
                            })
                        });
                        this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                            if (typeof user !== 'undefined') {
                                if (!user.firstName || !user.lastName || !user.address || !user.username) {
                                    let modal = await this.modalCtrl.create({
                                        component: OnboardingInfoPage,
                                        componentProps: {
                                            firstName: user.firstName,
                                            lastName: user.lastName,
                                            address: user.address,
                                            placeId: user.placeId,
                                            zipCode: user.zipCode,
                                            latitude: user.latitude,
                                            longitude: user.longitude,
                                            city: user.city,
                                            country: user.country
                                        },
                                        backdropDismiss: false
                                    });
                                    await modal.present();
                                }
                            }
                        }).catch(err => {
                            console.log(err)
                        })
                    } else {
                        this.splashScreen.hide();
                        this.router.navigate(['login']);
                    }
                }, err => {
                    this.splashScreen.hide();
                    this.router.navigate(['login']);
                });

        });


    }

    initLang(lang) {
        this.translateService.setDefaultLang('fr');
        this.translateService.use(lang);
    }


}
