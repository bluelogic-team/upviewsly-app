import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { IonicStorageModule } from '@ionic/storage';

import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
//import { FacebookLoginResponse } from '@ionic-native/facebook/ngx';

import { Contacts } from '@ionic-native/contacts/ngx'; 
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import {HttpClientModule, HttpClient} from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Globalization } from '@ionic-native/globalization/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { Badge } from '@ionic-native/badge/ngx';

import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';

import { Camera } from '@ionic-native/camera/ngx';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ContactsPageModule } from './pages/contacts/contacts.module';
import { EditProfilePageModule } from './pages/edit-profile/edit-profile.module';
import { AddUserPageModule } from './pages/add-user/add-user.module';
import { AddLocationPageModule } from './pages/add-location/add-location.module';
import { AddDatePageModule } from './pages/add-date/add-date.module';
import { MapPageModule } from './pages/map/map.module'; 
import { AddFriendsPageModule } from './pages/add-friends/add-friends.module';
import { AddEventPageModule } from './pages/add-event/add-event.module';
import { EventDetailsPageModule } from './pages/event-details/event-details.module';
import { EditFavoritePageModule } from './pages/edit-favorite/edit-favorite.module';
import { AddParticipantsPageModule } from './pages/add-participants/add-participants.module';
import { SelectFriendsPageModule } from './pages/select-friends/select-friends.module';
import { FcmService } from './providers/fcm/fcm.service';
import { FCM } from '@ionic-native/fcm/ngx';
import { AngularFirePerformanceModule } from '@angular/fire/performance'
import { File } from '@ionic-native/file/ngx';
import { AddFavoriteEventPageModule } from './pages/add-favorite-event/add-favorite-event.module';
import { Network } from '@ionic-native/network/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { OnboardingInfoPageModule } from './pages/onboarding-info/onboarding-info.module';
import { OnboardingUsernamePageModule } from './pages/onboarding-username/onboarding-username.module';
import { OnboardingFriendPageModule } from './pages/onboarding-friend/onboarding-friend.module';
import { OnboardingAddImagePageModule } from './pages/onboarding-add-image/onboarding-add-image.module';
import { SettingsPageModule } from './pages/settings/settings.module';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    BrowserAnimationsModule,
    IonicModule.forRoot(), 
    AppRoutingModule,
    MapPageModule,
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    ContactsPageModule,
    AddUserPageModule,
    AddLocationPageModule,
    AddDatePageModule,
    AddFriendsPageModule,
    AddEventPageModule,
    AddFavoriteEventPageModule,
    EventDetailsPageModule,
    HttpClientModule,
    AngularFirePerformanceModule,
    OnboardingInfoPageModule,
    OnboardingUsernamePageModule,
    OnboardingFriendPageModule,
    OnboardingAddImagePageModule,
    SettingsPageModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    EditProfilePageModule,
    EditFavoritePageModule,
    AddParticipantsPageModule,
    SelectFriendsPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Contacts,
    GooglePlus,
    Facebook,
    Diagnostic,
    SocialSharing,
    OpenNativeSettings,
    Globalization,
    HTTP,
    Badge,
    File,
    Camera,
    FcmService,
    FCM,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Network,
    LaunchNavigator
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
