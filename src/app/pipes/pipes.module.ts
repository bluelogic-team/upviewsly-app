import { NgModule } from '@angular/core';
import { DateAgoPipe } from './date-ago.pipe';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import { ReversePipe } from './reverse.pipe';
import { SortByPipe } from './sort-by.pipe';


@NgModule({
  imports: [
    CommonModule,
    IonicModule
  ],
  declarations: [DateAgoPipe, ReversePipe, SortByPipe],
  exports: [DateAgoPipe, ReversePipe, SortByPipe]
})
export class PipesModule {}
