import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'dateAgo',
  pure: true
})
export class DateAgoPipe implements PipeTransform {

  constructor(private translate: TranslateService) {}

  transform(value: any, args?: any): any {

    if (value) {
      const seconds = Math.floor((+new Date() - +new Date(value)) / 1000);
      if (seconds < 29) // less than 30 seconds ago will show as 'Just now'
        return this.translate.instant('date-pipe.NOW');

      const intervals = {
        [this.translate.instant('date-pipe.YEAR')]: 31536000,
        [this.translate.instant('date-pipe.MONTH')]: 2592000,
        [this.translate.instant('date-pipe.WEEK')]: 604800,
        [this.translate.instant('date-pipe.DAY')]: 86400,
        [this.translate.instant('date-pipe.HOUR')]: 3600,
        [this.translate.instant('date-pipe.MINUTE')]: 60,
        [this.translate.instant('date-pipe.SECOND')]: 1
      };
      let counter;
      for (const i in intervals) {
        counter = Math.floor(seconds / intervals[i]);
        if (counter > 0)
          if (counter === 1) {
            return this.translate.instant('date-pipe.AGO') + counter + ' ' + i; // singular (1 day ago)
          } else {
            if(i == this.translate.instant('date-pipe.MONTH')){
              return this.translate.instant('date-pipe.AGO') + counter + ' ' + i; // plural for month, no s
            } else {
              return this.translate.instant('date-pipe.AGO') + counter + ' ' + i + 's'; // plural (2 days ago)
            }
          }
      }
    }
    return value;
  }

}
