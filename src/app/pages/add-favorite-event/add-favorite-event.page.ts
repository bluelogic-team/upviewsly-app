import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { FavoriteService } from 'src/app/providers/favorite/favorite.service';

@Component({
    selector: 'app-add-favorite-event',
    templateUrl: './add-favorite-event.page.html',
    styleUrls: ['./add-favorite-event.page.scss'],
})
export class AddFavoriteEventPage implements OnInit {

    favorite = [];
    favoriteList = [];
    tagList = [];
    subscriberFavorite: any;

    constructor(
        private modalCtrl: ModalController,
        private translate: TranslateService,
        public authService: AuthService,
        private favoriteService: FavoriteService
    ) { }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.getAllFavorite();
    }

    selectFavorite(item) {
        this.modalCtrl.dismiss(item);
    }

    getAllFavorite() {
            this.favoriteService.getUserFavoritePlaces().subscribe(data => {
                this.favoriteList = data.map(e => {
                    this.tagList = e.payload.doc.data()['tags'];
                    return {
                        docId: e.payload.doc.id,
                        id: e.payload.doc.data()['id'],
                        name: e.payload.doc.data()['name'],
                        address: e.payload.doc.data()['address'],
                        lat: e.payload.doc.data()['lat'],
                        lng: e.payload.doc.data()['lng'],
                        tags: e.payload.doc.data()['tags']
                    };
                })
            });
    }

    back() {
        this.modalCtrl.dismiss();
    }

}
