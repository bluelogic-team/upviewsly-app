import { Component, OnInit } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { EventService } from "../../providers/event/event.service";
import { AuthService } from "../../providers/auth/auth.service";
import { AlertController, LoadingController } from '@ionic/angular';
import { NotificationsService } from 'src/app/providers/notifications/notifications.service';
import { Observable } from 'rxjs';
import { FcmService } from 'src/app/providers/fcm/fcm.service';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/providers/chat/chat.service';

@Component({
    selector: 'app-events',
    templateUrl: './events.page.html',
    styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {

    eventList = new Array();
    getEvents: any;
    list: any;
    ownerName: any;
    eventFiltered: any;
    _events: any;
    no_events = false;

    // Options to format date
    options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
    public displayType = "incoming";

    constructor(
        private translate: TranslateService,
        private eventService: EventService,
        private authService: AuthService,
        private alertController: AlertController,
        public loadingController: LoadingController,
        public notifService: NotificationsService,
        private fcmService: FcmService,
        private router: Router,
        private chatService: ChatService
    ) { }

    ngOnInit() {
        this.getUserEvents();
    }

    ionViewWillLeave() {
        // this.no_events = false;
        this.displayType = "incoming";
        // this.getEvents.unsubscribe();
    }

    ionViewWillEnter() {
        if (typeof this.chatService.unreadMessagesObservable !== 'undefined' &&
            typeof this.chatService.unreadLastMessageObservable !== 'undefined' &&
            typeof this.chatService.lastReadObservable !== 'undefined') {
            if (this.chatService.unreadMessagesObservable.closed &&
                this.chatService.unreadLastMessageObservable.closed &&
                this.chatService.lastReadObservable.closed) {
                this.chatService.unreadMessagesObservable = this.chatService.getUnreadMessages().subscribe(res => {
                    res.forEach((e: any) => {
                        this.chatService.unreadLastMessageObservable = this.chatService.getLastMessage(e.id).subscribe(lastMessage => {
                            lastMessage.forEach(last => {
                                this.chatService.lastReadObservable = this.chatService.getChatLastRead(e.id).subscribe(read => {
                                    // Check that the element does not exist yet to increment the iterator of + 1
                                    if (typeof read.payload.data() !== 'undefined') {
                                        if (typeof read.payload.data()['timestamp'] !== 'undefined') {
                                            if (read.payload.data()['timestamp'] < last.createdAt) {
                                                if (last.uid !== this.authService.getCurrentUserId()) {
                                                    if (this.chatService.unreadChatsList.includes(e.id)) {
                                                        this.chatService.unreadMessages = this.chatService.unreadChatsList.length;
                                                    } else {
                                                        this.chatService.unreadChatsList.push(e.id);
                                                        this.chatService.unreadMessages = this.chatService.unreadChatsList.length;
                                                    }

                                                }
                                            }
                                        }
                                    }
                                })
                            })
                        })
                    })
                });
            }
        }

        if (typeof this.authService.cacheEventsObservable !== 'undefined') {
            if (this.authService.cacheEventsObservable.closed) {
                this.getUserEvents();
            } else {
                this.eventList = this.authService.cache_events;
                this.filterEvents(this.displayType);
            }
        }
    }

    ionViewDidEnter() {
        setTimeout(() => this.filterEvents(this.displayType), 500);
    }

    async getUserEvents() {
        await this.getAllEvents();
    }

    async createNotification(participants, name, date) {
        const eventName = name;
        let arrayDevice = new Array();
        let displayDate = new Date(date);
        let recipient;
        let link = '/tabs/events/';
        // Get the full name of owner
        this.authService.getUserInfo(this.authService.getCurrentUserId()).then(
            owner => {
                return owner
            }
        )
            .then(
                (owner: any) => {
                    let title = this.translate.instant("notifications.YOU_HAVE_BEEN_INVITED");
                    let body = "";

                    let i = 0;
                    participants.forEach((element) => {
                        if (element.uid != this.authService.getCurrentUserId()) {

                            recipient = element.uid;

                            this.notifService.createNewNotification(recipient, title, body, link, null, false, null);
                            this.fcmService.getDeviceInfo(recipient).then(
                                (deviceToken: any) => {
                                     this.fcmService.sendPushNotification(title, body,
                                            deviceToken.data()['deviceToken'], recipient,
                                            { payload: { type: 'event', link: link } })
                                }
                            )
                        }
                    });
                }
            );

    }

    async getAllEvents() {
        const loading = await this.loadingController.create({
            message: this.translate.instant('common.LOADING')
        });
        await loading.present();

        let i = 0;
        let p = 0;
        this.authService.cacheEventsObservable = this.eventService.getEvents().subscribe(async (data: any) => {
            return new Observable(observer => {
                this.eventList = data.map(e => {
                    if (this.checkEventParticipant(e.participants)) {
                        p++;
                        return this.authService.getUserInfo(e.owner).then(
                            res => {
                                return res.displayName;
                            }
                        ).then(
                            res => {
                                let status;
                                e.participants.map(p => {
                                    if (p.uid === this.authService.getCurrentUserId()) {
                                        status = p.invitationAccepted;
                                    }
                                });

                                i++;
                                const data = {
                                    id: e.id,
                                    name: e.title,
                                    participants: e.participants,
                                    owner: e.owner,
                                    hour: e.hour,
                                    date: e.date,
                                    fullDate: new Date(e.date).toLocaleDateString('fr-FR', this.options),
                                    ownerName: res,
                                    eventPlaceId: e.eventPlaceId,
                                    eventAdresse: e.eventAddress,
                                    address: e.address,
                                    status: status
                                };

                                return { res, ...data }
                            }
                        ).then(
                            res => {
                                observer.next('fetch');
                                if (i === p) {
                                    if (typeof this.eventFiltered !== 'undefined') {
                                        observer.next('done');
                                        observer.complete();
                                    }
                                }
                                return res;
                            }
                        )
                    } else {
                        this.filterEvents(this.displayType);
                    }
                });
            }).subscribe(data => {
                // Use observable to display data 
                if (data) {
                    data === 'fetch' && (this.authService.cache_events = this.eventList);

                    // Return setTimeout with callback to get data from events asynchronously
                    data === 'done' && setTimeout((callback) => {
                        if (this.eventList) {
                            this.filterEvents(this.displayType)
                        } else {
                            callback;
                        }
                    });
                }
            })
        });
        loading.dismiss();
    }

    filterEvents(displayType) {
        if (displayType == 'incoming') {
            if (typeof this.eventList !== 'undefined') {
                this.eventList.sort((a, b) => { return a.date - b.date })
                let date = new Date();
                this.eventFiltered = this.eventList.filter((incoming) => {
                    if (typeof incoming !== 'undefined') {
                        let _event_date = new Date(incoming.__zone_symbol__value.date);
                        if ((_event_date.getDate() == date.getDate() && _event_date.getMonth() == date.getMonth() && _event_date.getFullYear() == date.getFullYear()) || (_event_date.getTime() > date.getTime())) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                });

                if (this.eventFiltered.length > 0) {
                    this.no_events = false;
                } else {
                    this.no_events = true;
                }
            }
        }

        if (displayType == 'past') {
            if (typeof this.eventList !== 'undefined') {
                this.eventList.sort((a, b) => { return b.date - a.date })
                let date = new Date();
                date.setHours(0, 0, 0);
                this.eventFiltered = this.eventList.filter((past) => {
                    if (typeof past !== 'undefined') {
                        let _event_date = new Date(past.__zone_symbol__value.date);
                        if (_event_date.getTime() < date.getTime()) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }).reverse()

                if (this.eventFiltered.length > 0) {
                    this.no_events = false;
                } else {
                    this.no_events = true;
                }
            }
        }
    }

    checkEventParticipant(participants) {

        let member = false;

        participants.forEach(element => {
            if (this.authService.getCurrentUserId() == element.uid) {
                member = true
            }
        });
        return member;
    }

    goToEventDetails(id: string) {
        this.router.navigate(['/tabs/event-details', id]);
    }

    async removeEvent(eventId, arrayId?: any) {
        const alert = await this.alertController.create({
            message: this.translate.instant('events.CONFIRM_DELETE'),
            buttons: [
                {
                    text: this.translate.instant('common.CANCEL'),
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: this.translate.instant('common.DELETE'),
                    handler: async () => {
                        let title = this.translate.instant('notifications.DELETE_EVENT_TITLE');
                        let name = this.eventFiltered[arrayId].__zone_symbol__value.name;
                        let fullDate = this.eventFiltered[arrayId].__zone_symbol__value.fullDate;
                        let participant = this.eventFiltered[arrayId].__zone_symbol__value.participants;
                        let i = 0;
                        let recipient;
                        let arrayDevice = new Array();
                        let link = '/tabs/events/';
                        for (let j = 0; j < participant.length; j++) {
                            if (participant[j].uid !== this.authService.getCurrentUserId()) {
                                recipient = participant[j].uid;

                                this.notifService.createNewNotification(recipient, title,
                                    this.translate.instant('notifications.THIS_EVENT') + name + this.translate.instant('notifications.FROM_DATE') + fullDate + this.translate.instant('notifications.DELETE_EVENT_CONFIRM'),
                                    link,
                                    null, false, null).then(() => {
                                        this.eventService.removeEvent(eventId)
                                            .then(() => {
                                                this.eventFiltered.splice(arrayId);
                                                this.getAllEvents().then((res: any) => { setTimeout(() => this.filterEvents(this.displayType), 500) });
                                            });
                                    });
                                await this.fcmService.getDeviceInfo(recipient).then(
                                    (deviceToken: any) => {
                                        this.fcmService.sendPushNotification(title, this.translate.instant('notifications.THIS_EVENT') + name + this.translate.instant('notifications.FROM_DATE') + fullDate + this.translate.instant('notifications.DELETE_EVENT_CONFIRM'),
                                                    deviceToken.data()['deviceToken'], recipient,
                                                    { payload: { type: 'event', link: link } });
                                    }
                                )
                            }
                        }
                    }
                }
            ]
        });
        await alert.present();
    }

}
