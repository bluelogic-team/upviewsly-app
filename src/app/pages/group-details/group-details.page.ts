import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Platform, NavController, LoadingController, ActionSheetController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

import { Group, GroupService } from '../../providers/group/group.service';
import { AuthService } from '../../providers/auth/auth.service';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import 'firebase/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import { SelectFriendsPage } from '../select-friends/select-friends.page';

import { TranslateService } from "@ngx-translate/core";
import { UserProfilePage } from '../user-profile/user-profile.page';

@Component({
    selector: 'app-group-details',
    templateUrl: './group-details.page.html',
    styleUrls: ['./group-details.page.scss'],
})
export class GroupDetailsPage implements OnInit {

    group: Group = {
        owner: "",
        name: "",
        imageURL: "",
        members: []
    }

    displayedMembers = [];
    groupId = null;
    canEdit: boolean = false;
    public base64Image;
    public imageChanged = false;


    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private groupService: GroupService,
        public authService: AuthService,
        private translate: TranslateService,
        private loadingController: LoadingController,
        public modalController: ModalController,
        public platform: Platform,
        public actionSheetCtrl: ActionSheetController,
        private db: AngularFirestore,
        private camera: Camera,
        private alertCtrl: AlertController
    ) { }

    ngOnInit() {

    }

    async ionViewDidEnter() {
        this.groupId = this.route.snapshot.params['id'];
        // if true, the group already exist, else we are creating a new group
        if (this.groupId) {
            await this.loadGroup();
        } else {
            this.canEdit = true;
        }
    }

    onKey(event) {
        if (event.key === "Enter") {
            this.saveGroup(true);
        }
    }

    async showUser(userId: string) {
        if (typeof userId !== 'undefined') {
            if(userId !== this.authService.getCurrentUserId()) {
                let modal = await this.modalController.create({
                    component: UserProfilePage,
                    componentProps: {
                        userId: userId
                    }
                });
                modal.present();
            } else {
                this.router.navigate(['tabs/profile']);
            }
        }
    }

    // get group from firebase, and retrieve members info from user collection
    async loadGroup() {
        const loading = await this.loadingController.create({
            message: this.translate.instant('common.LOADING')
        });
        await loading.present();

        // get group info
        this.groupService.getGroupInfo(this.groupId).then(async (res) => {
            loading.dismiss();
            this.group = res;

            let user = await this.authService.getCurrentUser();
            if (this.group.owner == user.uid) {
                this.canEdit = true;
            }
            if (this.group.imageURL) {
                this.base64Image = this.group.imageURL;
            }

            // retrieve info of each members
            this.group.members.forEach((member, i) => {

                this.authService.getUserInfo(this.group.members[i]).then((res) => {
                    if (res != undefined) { // ignore deleted accounts
                        this.displayedMembers[i] = Object.assign(
                            {},
                            { firebaseId: member },
                            { firstName: res.firstName },
                            { lastName: res.lastName },
                            { imageURL: res.imageURL }
                        );
                    }
                });
            });
        });
    }

    // save the group in firebase
    async saveGroup(navigate: boolean) {

        const loading = await this.loadingController.create({
            message: this.translate.instant('common.LOADING')
        });
        await loading.present();

        if (this.groupId) { //the group already exist
            if (this.imageChanged) {
                const picture = firebase.storage().ref('groups/avatars/' + 'group_avatar_' + this.groupId);
                picture.putString(this.base64Image, 'data_url').then((savedPicture) => {
                    picture.getDownloadURL().then(downloadUrl => {
                        this.group.imageURL = downloadUrl;
                        this.groupService.updateGroup(this.group, this.groupId).then(() => {
                            loading.dismiss();
                            if (navigate) this.router.navigate(['tabs/friends']);
                        });
                    });
                });
            } else {
                this.groupService.updateGroup(this.group, this.groupId).then(() => {
                    loading.dismiss();
                    if (navigate) this.router.navigate(['tabs/friends']);
                });
            }
        } else { //create new group
            if (this.imageChanged) {
                let newGroupId = this.db.createId();
                const picture = firebase.storage().ref('groups/avatars/' + 'group_avatar_' + newGroupId);
                picture.putString(this.base64Image, 'data_url').then((savedPicture) => {
                    picture.getDownloadURL().then(downloadUrl => {
                        this.group.imageURL = downloadUrl;
                        this.group.owner = this.authService.getCurrentUserObj().uid;
                        this.group.members.push(this.group.owner);
                        this.groupService.addGroupWithId(this.group, newGroupId).then(() => {
                            loading.dismiss();
                            if (navigate) this.router.navigate(['tabs/friends']);
                        });
                    });
                });
            } else {
                this.group.owner = this.authService.getCurrentUserObj().uid;
                this.group.members.push(this.group.owner);
                this.groupService.addGroup(this.group).then(() => {
                    loading.dismiss();
                    if (navigate) this.router.navigate(['tabs/friends']);
                });
            }
        }
    }

    async removeMember(member, fromMember?: boolean) {
        if (fromMember) {
            let alert = await this.alertCtrl.create({
                message: this.translate.instant('group-details.DELETE_ME'),
                buttons: [{
                    text: this.translate.instant('common.CANCEL'),
                    role: 'cancel'
                }, {
                    text: this.translate.instant('common.VALIDATE'),
                    cssClass: 'secondary',
                    handler: () => {
                        this.displayedMembers.splice(
                            this.displayedMembers.indexOf(member), 1
                        );
                        this.group.members.splice(
                            this.group.members.indexOf(member.firebaseId), 1
                        );
                        this.saveGroup(true);
                    }
                }]
            });
            alert.present();
        } else {
            this.displayedMembers.splice(
                this.displayedMembers.indexOf(member), 1
            );
            this.group.members.splice(
                this.group.members.indexOf(member.firebaseId), 1
            );
        }
    }



    async addMembers() {
        const modal = await this.modalController.create({
            component: SelectFriendsPage,
            componentProps: {
                participants: this.displayedMembers
            }
        });
        await modal.present();
        await modal.onWillDismiss().then(async (value: any) => {
            //true if validation button is clicked, false if back button is clicked
            if (value.data) {
                let selectedMembers = Object.values(value.data);
                selectedMembers.forEach((element: any) => {
                    // Object.assign(element, {firebaseId: element.id});
                    this.group.members.push(element.firebaseId);
                    this.displayedMembers.push(element);
                });

            }
        })
    }

    async editPicture() {
        var me = this;
        if (this.platform.is('cordova')) {
            const actionSheet = await this.actionSheetCtrl.create({
                header: this.translate.instant('common.EDIT_PICTURE_HEADER'),
                buttons: [
                    {
                        text: this.translate.instant('common.EDIT_PICTURE_PHOTO'),
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.CAMERA)
                        }
                    },
                    {
                        text: this.translate.instant('common.EDIT_PICTURE_GALLERY'),
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY)
                        }
                    },
                    {
                        text: this.translate.instant('common.CANCEL'),
                        role: 'cancel',
                        handler: () => {
                        }
                    }]
            });
            await actionSheet.present();
        }
        else {
            let input = document.createElement('input');
            input.type = 'file';
            input.click();
            var file;
            var reader = new FileReader();

            input.onchange = function() {
                file = input.files[0];
                if (file) {
                    reader.readAsDataURL(file);
                }
            };
            reader.onload = function() {
                me.base64Image = reader.result;
                me.imageChanged = true;
            };
            /*  reader.addEventListener("load", function () {
                  me.base64Image = reader.result;
                  me.imageChanged = true;
                }, false); */
        }
    }

    takePicture(sourceType) {
        const options: CameraOptions = {
            quality: 60,
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 1280,
            targetHeight: 1280,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: sourceType,
            correctOrientation: true,
        }

        this.camera.getPicture(options).then((imageData) => {
            this.base64Image = "data:image/jpeg;base64," + imageData;
            this.imageChanged = true;
        }, (err) => {
            throw new err;
        });
    }


}
