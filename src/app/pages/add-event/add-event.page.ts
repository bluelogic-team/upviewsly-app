import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { AuthService } from "../../providers/auth/auth.service"
import { EventService } from "../../providers/event/event.service"
import { NotificationsService } from "../../providers/notifications/notifications.service";
import { ChatService } from "../../providers/chat/chat.service";
import * as firebase from 'firebase';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AddParticipantsPage } from '../add-participants/add-participants.page';
import { ModalController, ToastController, LoadingController, AlertController } from '@ionic/angular';
import * as Config from '../../../utils/config';
import { FcmService } from 'src/app/providers/fcm/fcm.service';
import { AddFavoriteEventPage } from '../add-favorite-event/add-favorite-event.page';
import { FavoriteService } from 'src/app/providers/favorite/favorite.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var google;
declare var MarkerClusterer: any;
let mapEvent: any;
declare var mapIcons;
let infowindow: any;
let placeInfo: any;

@Component({
    selector: 'app-add-event',
    templateUrl: './add-event.page.html',
    styleUrls: ['./add-event.page.scss'],
})
export class AddEventPage implements OnInit {

    @ViewChild('mapEvent') mapElement: ElementRef;
    group: FormGroup;
    currentPlaceId: any;
    homePlaceId: any;
    eventName: any;
    eventPlace: any;
    eventId: any;
    eventPlaceAddress: any;
    selectedPlace: any;
    placeId: any;
    date: any;
    defaultDate: any;
    defaultEventdate: any;
    defaultEventHour: any;
    defaultHour: any;
    beginHour: any;
    beginDate: any;
    autoComplete: any;
    pos: any;
    actionType: any;
    docId: any;
    title: any;
    zoom: any;
    btnSubmit: any;
    street: any = '';
    streetNumber: any = '';
    zipCode: any = '';
    city: any = '';
    country: any = '';
    isService: boolean = false;
    participants: any;
    newMembers = [];
    markers = [];
    markersParticipants = new Array();
    markerCluster: any;
    isSending: boolean = false;
    paginationTicks: number = 0;
    chatId: string;
    $chatId: any;
    favoriteList = [];
    tagList: any;
    _participants = new Array();
    public isLoading: boolean = false;
    // Options cluster
    mcOptions = {
        gridSize: 100,
        minimumClusterSize: 4,
        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
    };

    // Options to format date
    options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
    dayShortNames: any;

    constructor(
        private translate: TranslateService,
        private authService: AuthService,
        private eventService: EventService,
        private chatService: ChatService,
        private route: ActivatedRoute,
        private _location: Location,
        private modalCtrl: ModalController,
        private router: Router,
        private notificationService: NotificationsService,
        public toastController: ToastController,
        public loadingController: LoadingController,
        public notifService: NotificationsService,
        private fcmService: FcmService,
        private alertCtrl: AlertController,
        private favoriteService: FavoriteService,
        public formBuilder: FormBuilder
    ) { 
        this.group = formBuilder.group({
            eventName: ["", [Validators.required, Validators.maxLength(50)]]
        })

        this.dayShortNames = this.getDayShortNames();
    }

    ngOnInit() {
    }

    async ionViewWillEnter() {

        const loading = await this.loadingController.create({
            message: this.translate.instant('common.LOADING')
        });
        await loading.present();

        await this.init().then(async () => {
            await this.initAutocomplete();
        }).then(async () => {
            this.authService.getUserCoords(firebase.auth().currentUser.uid).then(res => {
                if (res) {
                    if (res.latitude != '' && res.longitude != '') {
                        this.pos = { lat: res.latitude, lng: res.longitude }
                        this.zoom = Config.DEFAULT_ZOOM_HIGH;
                        this.homePlaceId = res.address_placeId;
                    } else {
                        this.pos = { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG }
                        this.zoom = Config.DEFAULT_ZOOM_LOW;
                    }
                } else {
                    this.pos = { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG }
                    this.zoom = Config.DEFAULT_ZOOM_LOW;
                }
            }).then(async () => {
                await this.initMap();
                this.createHomeMarker();
            });
            await loading.dismiss();
        });
    }

    ionViewDidEnter() {
        this.getAllFavorite();
        this.handleChangeFromPicker();
    }

    ionViewWillLeave() {
        this.clearFields();
        this.markerCluster = [];
        this.markersParticipants = [];
        this.markers = [];
        // Unsubscribe chat to prevent error
        this.$chatId && this.$chatId.unsubscribe();
    }

    clearMarkers() {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        for (let i = 0; i < this.markersParticipants.length; i++) {
            this.markersParticipants[i].setMap(null);
        }
    }

    getAllFavorite() {
            this.favoriteService.getUserFavoritePlaces().subscribe(data => {
                this.favoriteList = data.map(e => {
                    this.tagList = e.payload.doc.data()['tags'];
                    return {
                        docId: e.payload.doc.id,
                        id: e.payload.doc.data()['id'],
                        name: e.payload.doc.data()['name'],
                        address: e.payload.doc.data()['address'],
                        lat: e.payload.doc.data()['lat'],
                        lng: e.payload.doc.data()['lng'],
                        tags: e.payload.doc.data()['tags']
                    };
                })
            });
        
    }

    // zoom definition
    getZoom(mapViewBox) {

        var GLOBE_WIDTH = 256; // a constant in Google's map projection
        var PIXEL_SCREEN = 300; // constant mid size smartphone display
        var west = mapViewBox[0].lngSw;
        var east = mapViewBox[1].lngNe;
        var angle = east - west;
        if (angle < 0) {
            angle += 360;
        }
        return Math.round(Math.log(PIXEL_SCREEN * 360 / angle / GLOBE_WIDTH) / Math.LN2);
    }

    deg2rad(x) {
        return Math.PI * x / 180;
    }

    // Radius definition
    getDistanceM($lat1, $lng1, $lat2, $lng2) {
        let $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
        let $rlo1 = this.deg2rad($lng1);    // CONVERSION
        let $rla1 = this.deg2rad($lat1);
        let $rlo2 = this.deg2rad($lng2);
        let $rla2 = this.deg2rad($lat2);
        let $dlo = ($rlo2 - $rlo1) / 2;
        let $dla = ($rla2 - $rla1) / 2;
        let $a = (Math.sin($dla) * Math.sin($dla)) + Math.cos($rla1) * Math.cos($rla2) * (Math.sin($dlo) * Math.sin($dlo));
        let $d = 2 * Math.atan2(Math.sqrt($a), Math.sqrt(1 - $a));
        return ($earth_radius * $d);
    }

    // get radius size
    getRadius(mapViewBox) {
        return this.getDistanceM(mapViewBox[0].latSw, mapViewBox[0].lngSw, mapViewBox[1].latNe, mapViewBox[1].lngNe);
    }

    checkFavoriteFromPoi(place) {
        if (typeof this.favoriteList !== 'undefined') {
            if (this.favoriteList.length != 0) {
                for (let i = 0; i < this.favoriteList.length; i++) {
                    if (place.reference === this.favoriteList[i].id) {
                        return true;
                    }
                }
                return false;
            }
        }
    }

    getIcon(place) {
        var types = place.types;
        var iconBase = '../../../assets/images/icon-upviewsly.svg';

        if (typeof types !== 'undefined') {
            var typeRestaurant = (types.indexOf("restaurant") > -1);
            var typeBar = (types.indexOf("bar") > -1);
            var typeCafe = (types.indexOf("cafe") > -1);


            if (typeBar) {
                iconBase = '<span class="map-icon map-icon-night-club"></span>';
                this.isService = true;
            }
            else if (typeRestaurant) {
                iconBase = '<span class="map-icon map-icon-restaurant"></span>';
                this.isService = true;
            }
            else if (typeCafe) {
                iconBase = '<span class="map-icon map-icon-cafe"></span>';
                this.isService = true;
            } else {
                this.isService = false;
            }
        }
        return iconBase;
    }

    /**
     * 
     * @param place 
     * @param participants only for displaying in map
     */
    async createMarker(place, participants?: boolean) {
        let that = this;
        var icon = this.getIcon(place);
        var btn = '';

        var image = {
            path: mapIcons.shapes.MAP_PIN,
            fillColor: this.checkFavoriteFromPoi(place) ? '#F400FE' : '#820080',
            fillOpacity: 1,
            strokeColor: '',
            strokeWeight: 0,
            scale: 1
        };

        if (!participants) {
            let placeLoc = place.geometry.location;

            if(this.isService && !participants || this.checkFavoriteFromPoi(place)) {
                    var marker = await new mapIcons.Marker({
                        map: mapEvent,
                        icon: image,
                        position: placeLoc,
                        map_icon_label: icon,
                        zIndex: 9
                    });
                } else {
                    var marker = await new google.maps.Marker({
                        position: placeLoc,
                        icon: icon,
                        map: mapEvent
                    });
                }

            this.checkFavoriteFromPoi(place)

            this.markers.push(marker);

            await new google.maps.event.addListener(marker, 'click', function() {

                function round(value, precision) {
                    var multiplier = Math.pow(10, precision || 0);
                    return Math.round(value * multiplier) / multiplier;
                }

                if (place.price_level) {
                var priceLevel;
                switch (place.price_level) {
                    case 0:
                        priceLevel = "€";
                        break;
                    case 1:
                        priceLevel = "€";
                        break;
                    case 2:
                        priceLevel = "€€";
                        break;
                    case 3:
                        priceLevel = "€€€";
                        break;
                    case 5:
                        priceLevel = "€€€€";
                        break;
                    default:
                        priceLevel = "€";
                        break;
                }
            }

            let ratingHtml = "";
            if(place.rating){
                ratingHtml = that.translate.instant('map.RATING') + round(place.rating, 1).toFixed(1) + ' / 5  ' + '(' + place.user_ratings_total +
                    that.translate.instant('map.RATING_NUMBER') + ')' + " &bull; ";
                if(place.price_level){
                    ratingHtml +=  priceLevel;
                }
            }

            var infowindowContent =
                    '<div class="info-window-content" style="min-width: 185px; min-height: 80px; padding: 0 10px 0 0;">' +
                    '<strong style="display: inline-block; margin: 2px 0 6px;">' + place.name + '</strong><br>' +
                    place.vicinity + "<br>" +
                    ratingHtml +
                    '<ul class="info-window--list-actions" style="list-style: none; margin: .75em 0; padding: 0;">' +
                    '<li style="margin: 0 0 .3rem 0;"><a class="ion-no-margin ion-no-padding ion-button" size="small" fill="clear" color="primary" id="createButton_' + place.place_id + '" style="font-weight: 400; text-decoration: none;">' + that.translate.instant('map.USE_PLACE') + '</ion-button></li>' +
                    '</ul></div>';

                infowindow.setContent(infowindowContent);
                infowindow.open(mapEvent, this);

                new google.maps.event.addListenerOnce(infowindow, 'domready', () => {

                    document.getElementById('createButton_' + place.place_id).addEventListener("click", async () => {
                        let service = new google.maps.places.PlacesService(mapEvent);
                        return await service.getDetails({
                            placeId: place.place_id
                        }, async function(result) {

                            that.selectedPlace = result
                            that.placeId = place.place_id;
                            that.setAddress(result);
                            infowindow.close();
                        });
                    });
                });

            })
        } else {
            if (typeof place !== 'undefined' && typeof place !== 'undefined') {
                if (place.latitude && place.longitude) {
                    let location = { lat: place.latitude, lng: place.longitude };
                    let placeLoc = location;
                    let defaultAvatar = '../../../assets/images/default-avatar.png';
                    /*let avatar = `<div style='display: inline-block; position: relative; width: 60px; height: 60px; background-color: lightgray; overflow: hidden; border-radius: 50%; box-shadow: 0 0px 10px rgba(0, 0, 0, .25);'>
                                    <ion-avatar style="width: 60px; height: 60px;">
                                        <img src=${place.imageURL ? place.imageURL : defaultAvatar} />
                                    </ion-avatar>
                                  </div>`;
                                  */
                    let avatar;
                    if(place.imageURL){
                         avatar = place.imageURL;
                    } else {
                         avatar = defaultAvatar;
                    }

                    var markerParticipants = new google.maps.Marker({
                        map: mapEvent,
                        position: placeLoc,
                        zIndex: 10,
                        icon:{
                           url: avatar,
                           optimized:false,
                           scaledSize: new google.maps.Size(60, 60),
                         }
                    });
                     
                    this.markersParticipants.push(markerParticipants);
                }
            }
        }
    }

    async setLocationFromParticipants() {
        let bounds = new google.maps.LatLngBounds();
        this.paginationTicks = 0;
        var getNextPage = null;
        let that = this;
        var centerLat = 0;
        var centerLng = 0;
        let participants_coords = [];
        let mapViewBox = [
            { latSw: 0, lngSw: 0 },
            { latNe: 0, lngNe: 0 }
        ];
        this.clearMarkers();
        this.markers = [];
        this.markersParticipants = [];
        if (typeof this.markerCluster !== 'undefined') {
            if (this.markerCluster.length == 0) {
            } else {
                if (this.markerCluster.clusters_.length > 0) {
                    this.markerCluster.clearMarkers();
                }
            }
        }

        let j = 0;
        if (typeof this.participants !== 'undefined') {
            if (this.participants.length >= 0) {
                for (let i = 0; i < this.participants.length; i++) {
                    this.authService.getUserCoords(this.participants[i].uid).then(res => {
                        j++;
                        if (res.latitude != "" && res.longitude != "") {
                            participants_coords.push(res);
                            that.createMarker(res, true);
                            bounds.extend(new google.maps.LatLng(res.latitude, res.longitude));
                        }
                        if (j === this.participants.length) {
                            // draw circle avatars after small timeout
                            var myoverlay = new google.maps.OverlayView();
                            myoverlay.draw = function () {
                               this.getPanes().markerLayer.id='markerLayer';
                            };
                            myoverlay.setMap(mapEvent);                         
                            setTimeout(this.drawCircleAvatars, 200);
                            // Calculate the map center
                            if (participants_coords.length > 1) {
                                //                     there is at least 1 member in the group
                                // get NE and SW points for viewbox
                                mapViewBox[0].latSw = Math.min.apply(Math, participants_coords.map(function(o) { return o.latitude; }));
                                mapViewBox[0].lngSw = Math.min.apply(Math, participants_coords.map(function(o) { return o.longitude; }));
                                mapViewBox[1].latNe = Math.max.apply(Math, participants_coords.map(function(o) { return o.latitude; }));
                                mapViewBox[1].lngNe = Math.max.apply(Math, participants_coords.map(function(o) { return o.longitude; }));
                                centerLng = (mapViewBox[0].lngSw + mapViewBox[1].lngNe) / 2;
                                centerLat = (mapViewBox[0].latSw + mapViewBox[1].latNe) / 2;
                                let newCenter = { lat: centerLat, lng: centerLng }
                                mapEvent.setCenter(newCenter);
                                var service = new google.maps.places.PlacesService(mapEvent);
                                service.nearbySearch({
                                    location: { lat: mapEvent.getCenter().lat(), lng: mapEvent.getCenter().lng() },
                                    radius: 1500,
                                    types: ['restaurant', 'bar', 'cafe']
                                }, (results, status, pagination) => {
                                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                                        mapEvent.fitBounds(bounds);
                                        for (var i = 0; i < results.length; i++) {
                                            // bounds.extend(new google.maps.LatLng(results[i].geometry.location.lat(), results[i].geometry.location.lng()))
                                            this.createMarker(results[i]);
                                            if (i + 1 === results.length) {
                                                if (pagination.hasNextPage) {
                                                    pagination.nextPage();
                                                }

                                                if (!pagination.hasNextPage) {
                                                    // mapEvent.fitBounds(bounds)
                                                    setTimeout(() => { this.markerCluster = new MarkerClusterer(mapEvent, this.markers, this.mcOptions);}, 500);
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }).catch(e => console.log(e));
                }
            } else {
                let alert = await this.toastController.create({
                    message: this.translate.instant('add-event.EMPTY_PARTICIPANT'),
                    duration: 5000
                });
                alert.present();
            }
        } else {
            let alert = await this.toastController.create({
                message: this.translate.instant('add-event.EMPTY_PARTICIPANT'),
                duration: 5000
            });
            alert.present();
        }

    }

    async openFavorite() {
        const modal = await this.modalCtrl.create({
            component: AddFavoriteEventPage
        });
        modal.present();

        this.clearMarkers();
        this.markers = [];
        this.markersParticipants = [];
        if (typeof this.markerCluster !== 'undefined') {
            if (this.markerCluster.length == 0) {
            } else {
                if (this.markerCluster.clusters_.length > 0) {
                    this.markerCluster.clearMarkers();
                }
            }
        }
        this.createHomeMarker();

        modal.onDidDismiss().then(async res => {
            var service = new google.maps.places.PlacesService(mapEvent);
            let that = this;
            return await service.getDetails({
                placeId: await res.data.id
            }, async function(result) {
                that.selectedPlace = result
                that.placeId = result.place_id;
                that.setAddress(result);
                let newCenter = { lat: result.geometry.location.lat(), lng: result.geometry.location.lng() }
                mapEvent.setCenter(newCenter);
                mapEvent.setZoom(Config.DEFAULT_ZOOM_HIGH);
                that.createMarker(result);
            })
        })
    }

    async setLocationFromProfile() {
        this.clearMarkers();
        this.markers = [];
        this.markersParticipants = [];
        if (typeof this.markerCluster !== 'undefined') {
            if (this.markerCluster.length == 0) {
            } else {
                if (this.markerCluster.clusters_.length > 0) {
                    this.markerCluster.clearMarkers();
                }
            }
        }
        this.createHomeMarker();
        return this.authService.getUserCoords(this.authService.getCurrentUserId()).then(async res => {
            if (res.latitude != '' && res.longitude != '') {
                var service = new google.maps.places.PlacesService(mapEvent);
                let that = this;
                return await service.getDetails({
                    placeId: await res.placeId
                }, async function(result) {
                    that.selectedPlace = result
                    that.placeId = result.place_id;
                    that.setAddress(result);
                    let newCenter = { lat: result.geometry.location.lat(), lng: result.geometry.location.lng() }
                    mapEvent.setCenter(newCenter);
                    mapEvent.setZoom(Config.DEFAULT_ZOOM_HIGH);
                    //that.createMarker(result);
                })
            } else {
                let alert = await this.alertCtrl.create({
                    message: this.translate.instant('add-event.MISSING_PLACEID'),
                    buttons: [{
                        text: this.translate.instant('common.CANCEL'),
                        role: 'cancel'
                    }, {
                        text: this.translate.instant('common.YES'),
                        handler: () => {
                            this.router.navigate(['/tabs/profile']);
                        }
                    }]
                });
                alert.present();
            }
        })
    }

    async init() {

        this.actionType = this.route.snapshot.paramMap.get("type");
        this.docId = this.route.snapshot.paramMap.get("docId");
        this.placeId = this.route.snapshot.paramMap.get("placeId");
        this.eventId = this.route.snapshot.paramMap.get("eventId");

        if (this.actionType == 'edit') {
            this.$chatId = this.chatService.getChatFromEvent(this.docId).subscribe(
                // return chat.id
                (chat: any) => {
                    chat.forEach(el => {
                        el.id && (this.chatId = el.id);
                    });
                })
            this.title = this.translate.instant('add-event.TITLE_EDIT');
            this.btnSubmit = this.translate.instant('common.SAVE');

            await this.getEvent(this.docId).then(async res => {
                let newDate = new Date(res.date);
                let member = [];

                let today = new Date(Date.now());

                if (!this.defaultDate) {
                    today.setHours(today.getHours() + 1)
                    this.defaultDate = today.toISOString();
                    this.beginDate = today.toISOString();
                }

                this.eventPlace = res.eventAddress;
                this.selectedPlace = res;
                this.date = newDate.toISOString();
                this.beginDate = this.date;
                this.defaultEventdate = this.date;
                this.eventName = res.title;
                this.participants = res.participants;
                for (let index = 0; index < res.participants.length; index++) {

                    let status = res.participants[index].invitationAccepted
                    this.getParticipant(res.participants[index].uid).then(async res => {

                        member.push({
                            uid: res.uid,
                            firstName: res.firstName,
                            lastName: res.lastName,
                            imageURL: res.imageURL,
                            invitationAccepted: status
                        })

                        return member;
                    }).then((members) => {
                        this.participants = members;
                    })

                    // Why notification is build here ?
                    // .then(() => {
                    //     this.createNotification(this.participants)
                    // })
                }
            });
        }
        else {

            let today = new Date(Date.now());

            if (!this.defaultDate) {
                today.setHours(today.getHours() + 1)
                this.defaultDate = today.toISOString();
                this.beginDate = today.toISOString();
            }

            this.title = this.translate.instant('add-event.TITLE');
            this.btnSubmit = this.translate.instant('common.SAVE');
        }
    }

        // Center control map
        centerControl(controlDiv, map) {
            let self = this;
            this.isLoading = false;
            // Set CSS for the control border.
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = '#fff';
            controlUI.style.border = '2px solid #fff';
            controlUI.style.borderRadius = '3px';
            controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
            controlUI.style.cursor = 'pointer';
            controlUI.style.marginBottom = '22px';
            controlUI.style.textAlign = 'center';
            controlUI.title = this.translate.instant('common.SEARCH_POI');
            controlDiv.appendChild(controlUI);
    
            // Set CSS for the control interior.
            var controlText = document.createElement('div');
            controlText.style.color = 'rgb(25,25,25)';
            controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
            controlText.style.fontSize = '16px';
            controlText.style.lineHeight = '38px';
            controlText.style.paddingLeft = '5px';
            controlText.style.paddingRight = '5px';
            controlText.innerHTML = this.translate.instant('common.SEARCH_POI');
            controlUI.appendChild(controlText);
            // Setup the click event listeners: simply set the map to Chicago.
            controlUI.addEventListener('click', function() {
                if (self.isLoading) {
                    return
                }
                controlText.innerHTML = self.translate.instant('common.LOADING');
                var getNextPage = null;
                var service = new google.maps.places.PlacesService(map);
           
                self.clearMarkers();
                self.markers = [];
                if (typeof self.markerCluster !== 'undefined' && self.markerCluster.length > 0) {
                    self.markerCluster.clearMarkers();
                }
                self.createHomeMarker();
                var bounds = new google.maps.LatLngBounds(map.getBounds().getSouthWest(), map.getBounds().getNorthEast());
                return service.nearbySearch({
                    //location: { lat: map.getCenter().lat(), lng: map.getCenter().lng() },
                    //radius: self.getDistanceM(map.getBounds().getNorthEast().lat(), map.getBounds().getNorthEast().lng(), map.getBounds().getSouthWest().lat(), map.getBounds().getSouthWest().lng()),
                    bounds: bounds,
                    types: ['restaurant', 'bar', 'cafe']
                }, (results, status, pagination) => {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        self.isLoading = true;
                        for (var i = 0; i < results.length; i++) {
                            self.createMarker(results[i]);
                            // bounds.extend(new google.maps.LatLng(results[i].geometry.location.lat(), results[i].geometry.location.lng()))
                            // On ending request for one query
                            if (i + 1 === results.length) {
                                if (pagination.hasNextPage) {
                                    pagination.nextPage();
                                }
    
                                if (!pagination.hasNextPage) {
                                    // map.fitBounds(bounds)
                                    controlText.innerHTML = self.translate.instant('common.SEARCH_POI');
                                    self.isLoading = false;
                                    // Timemout wait for last page of markers created;
                                    setTimeout(() => { self.markerCluster = new MarkerClusterer(map, self.markers, self.mcOptions);}, 500);
                                    
                                }
                            }
                        }
    
                    } else {
                        controlText.innerHTML = self.translate.instant('common.SEARCH_POI');
                    }
                })
            });
        }

    // This function will begin override the 'min' atttribute on element when day is D + 1
    handleChangeFromPicker() {
        let _minFullDate = document.querySelector('ion-datetime[type="full_date"]');

        if (!_minFullDate.getAttribute('min')) {
            this.defaultEventdate && _minFullDate.setAttribute('min', this.defaultDate);
        }
    }

    async submitEvent() {

        if (typeof this.participants !== 'undefined' && this.participants.length != 0) {
            if (this.eventPlace != null || this.eventPlace != '') {
                this.isSending = true;

                let addressComponents = {
                    street: this.street,
                    streetNumber: this.streetNumber,
                    zipCode: this.zipCode,
                    city: this.city,
                    country: this.country
                }

                let participant_id = [];
                await this.participants.forEach(p => {
                    participant_id.push(p.uid)
                });

                let event = {
                    owner: firebase.auth().currentUser.uid,
                    title: this.eventName,
                    date: new Date(this.beginDate).getTime(),
                    eventPlaceId: this.placeId,
                    eventAddress: this.eventPlace,
                    address: addressComponents,
                    participants: this.participants,
                    participant_id: participant_id
                }

                if (this.actionType == 'edit') {
                    await this.createNotification(this.participants, 'edit');
                    await this.presentToast('edit');
                    this.eventService.updateEvent(this.docId, event).then(async () => {
                        const chat_data = {
                            title: this.eventName,
                            date: new Date(this.beginDate).getTime(),
                            participants: participant_id
                        }
                        this.chatService.updateChat(this.docId, chat_data).then(() => {
                            setTimeout(() => {
                                this.router.navigate(['tabs/events']);
                                this.isSending = false; this.clearFields();
                                this._participants.forEach(e => {
                                    firebase.firestore().collection('chats').doc(this.chatId).collection('lastRead').doc(e.uid).set({ "timestamp": null });
                                });
                            }, 500);
                        });
                    });
                }
                else {
                    await this.eventService.createNewEvent(event).then(async (doc: any) => {
                        this.eventId = doc.id;
                        let chatMembers = [];
                        await this.participants.forEach(element => {
                            chatMembers.push(element.uid)
                        });
                        await this.chatService.createChat(event.title, event.date, event.date, chatMembers, doc.id, "event");
                    }).then(async () => {
                        await this.createNotification(this.participants)
                        await this.presentToast('add');
                        this.clearFields();
                        setTimeout(() => { this.router.navigate(['tabs/events']); this.isSending = false; }, 500);
                    });
                }
            }
            else {
                this.presentToast('invalid_address');
            }
        }
        else {
            this.presentToast('missing_participants');
        }
    }

    setDay() {
        switch (new Date(this.beginDate).getDay()) {
            case 1:
                return this.translate.instant('dates.MONDAY');
            case 2:
                return this.translate.instant('dates.TUESDAY');
            case 3:
                return this.translate.instant('dates.WEDNESDAY');
            case 4:
                return this.translate.instant('dates.THURSDAY');
            case 5:
                return this.translate.instant('dates.FRIDAY');
            case 6:
                return this.translate.instant('dates.SATURDAY');
            case 7:
                return this.translate.instant('dates.SUNDAY');
        }
    }

    setMonth() {
        switch (new Date(this.beginDate).getMonth()) {
            case 1:
                return this.translate.instant('dates.JANUARY');
            case 2:
                return this.translate.instant('dates.FEBRUARY');
            case 3:
                return this.translate.instant('dates.MARCH');
            case 4:
                return this.translate.instant('dates.APRIL');
            case 5:
                return this.translate.instant('dates.MAY');
            case 6:
                return this.translate.instant('dates.JUNE');
            case 7:
                return this.translate.instant('dates.JULY');
            case 8:
                return this.translate.instant('dates.AUGUST');
            case 9:
                return this.translate.instant('dates.SEPTEMBER');
            case 10:
                return this.translate.instant('dates.OCTOBER');
            case 11:
                return this.translate.instant('dates.NOVEMBER');
            case 12:
                return this.translate.instant('dates.DECEMBER');
        }
    }

    async createNotification(participants, type?: string) {
        const eventName = this.eventName;
        let arrayDevice = new Array();
        let displayDate = new Date(this.beginDate);
        let recipient;
        let link = '/tabs/event-details/';
        let eventId = this.eventId ? this.eventId : this.docId;
        // Get the full name of owner
        this.authService.getUserInfo(this.authService.getCurrentUserId()).then(
            owner => {
                return owner
            }
        )
            .then(
                (owner: any) => {
                    let title = this.translate.instant("notifications.YOU_HAVE_BEEN_INVITED");
                    let body = this.translate.instant("notifications.YOU_HAVE_BEEN_INVITED_BY") + owner.displayName + this.translate.instant("notifications.EVENT_BY") + eventName + ' ' + new Date(displayDate).toLocaleDateString('fr-FR', this.options) + this.translate.instant("notifications.FROM_HOUR") + (displayDate.getHours() < 10 ? "0" + displayDate.getHours() : displayDate.getHours()) + ':' + (displayDate.getMinutes() < 10 ? "0" + displayDate.getMinutes() : displayDate.getMinutes());

                    switch (type) {
                        case 'edit':
                            title = this.translate.instant("notifications.UPDATED_EVENT");
                            body = this.translate.instant("notifications.THIS_EVENT") + eventName + ' ' + new Date(displayDate).toLocaleDateString('fr-Fr', this.options) + this.translate.instant("notifications.FROM_HOUR") + displayDate.getHours() + ':' + (displayDate.getMinutes() < 10 ? '0' + displayDate.getMinutes() : displayDate.getMinutes()) + this.translate.instant("notifications.UPDATE_CONFIRM");
                            break;
                        default:
                            title = title;
                            body = body;
                            break;
                    }

                    for (let j = 0; j < participants.length; j++) {
                        if (participants[j].uid !== this.authService.getCurrentUserId()) {

                            recipient = participants[j].uid;
                            this.notificationService.createNewNotification(recipient, title, body, link, eventId, false, null);
                            this.fcmService.getDeviceInfo(recipient).then(
                                (deviceToken: any) => {
                                    this.fcmService.sendPushNotification(title,body,deviceToken.data()['deviceToken'], recipient, { payload: { type: 'event', link: link, linkData: eventId } })
                                }
                            )
                        }
                    }
                }
            );

    }

    async addParticipants() {

        let modalEdit = await this.modalCtrl.create({
            component: AddParticipantsPage,
            componentProps: {
                eventParticipants: this.participants
            }
        });
        await modalEdit.present();
        await modalEdit.onWillDismiss().then(async newUsers => {
            let finalGroup = [];
            let actualMembers = [];
            let newMembers = [];
            this._participants = [];
            if (this.participants) {
                actualMembers = this.participants;
                if (newUsers) {
                    if (newUsers.data.members) { // If it's a group
                        await newUsers.data.members.forEach(async element => {
                            let pushable = true;
                            let memberUid = element.firebaseId;

                            actualMembers.forEach(element => {
                                if (memberUid == element.uid) {
                                    pushable = false;
                                }
                            });
                            if (pushable) {
                                let member = { uid: element.firebaseId };
                                actualMembers.push(member);
                                newMembers.push(member);
                                this._participants.push(member);
                            }
                        });
                    } else { // If it's a friend
                        await newUsers.data.forEach(async element => {
                            let pushable = true;
                            let memberUid = element.firebaseId;

                            actualMembers.forEach(element => {
                                if (memberUid == element.uid) {
                                    pushable = false;
                                }
                            });
                            if (pushable) {
                                let member = { uid: element.firebaseId };
                                actualMembers.push(member);
                                newMembers.push(member);
                                this._participants.push(member);
                            }
                        });
                    }
                }
                this.newMembers = newMembers;
            } else {
                if (newUsers.data.members) { // If it's a group
                    await newUsers.data.members.forEach(async element => {
                        let member = { uid: element.firebaseId };
                        actualMembers.push(member);
                        this.participants = actualMembers;
                    });
                } else { // If it's a friend
                    await newUsers.data.forEach(async element => {
                        let member = { uid: element.firebaseId };
                        actualMembers.push(member);
                        this.participants = actualMembers;
                    });
                    actualMembers.push({ uid: this.authService.getCurrentUserId() });
                }
            }

            actualMembers.forEach(async (element) => {
                if (element.invitationAccepted) {
                    if (element.invitationAccepted != 'PENDING') {
                        await this.getParticipant(element.uid).then(res => {
                            finalGroup.push(Object.assign(res, { invitationAccepted: element.invitationAccepted }));
                        });
                    }
                    else {
                        await this.getParticipant(element.uid).then(res => {
                            finalGroup.push(Object.assign(res, { invitationAccepted: 'PENDING' }));
                        });
                    }
                }
                else {
                    await this.getParticipant(element.uid).then(res => {
                        finalGroup.push(Object.assign(res, { invitationAccepted: this.authService.getCurrentUserId() === element.uid ? 'CONFIRMED' : 'PENDING' }));
                    });
                }
                this.participants = finalGroup;
            });
        });
    }

    async getParticipant(uid) {

        return new Promise<any>((resolve) => {
            this.authService.getUserInfo(uid).then(res => {
                let member = {
                    firstName: res.firstName,
                    lastName: res.lastName,
                    imageURL: res.imageURL,
                    uid: uid
                }

                return member;
            }).then(member => {
                resolve(member);
            });
        });
    }

    async getEvent(eventDocId) {

        return new Promise<any>(async (resolve) => {
            this.eventService.getOneEvent(eventDocId).subscribe(data => {
                let event = data.payload.data();
                resolve(event);
            });
        });
    }

    async presentToast(type) {

        let msg;

        switch (type) {
            case 'edit':
                msg = this.translate.instant('add-event.SUCCESS_EDIT');
                break;
            case 'missing_participants':
                msg = this.translate.instant('add-event.MISSING_PARTICIPANTS');
                break;
            case 'invalid_address':
                msg = this.translate.instant('add-event.INVALID_ADDRESS');
                break;
            case 'add':
                msg = this.translate.instant('add-event.SUCCESS_CREATE')
                break;
        }


        const toast = await this.toastController.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    async initMap(lat?: number, lng?: any) {

        mapEvent = await new google.maps.Map(this.mapElement.nativeElement, {
            center: { lat: lat ? lat : this.pos.lat, lng: lng ? lng : this.pos.lng },
            zoom: this.zoom,
            streetViewControl: false,
            zoomControl: false,
            mapTypeControl: false,
            draggable: true,
            styles: [
                {
                    featureType: 'poi',
                    stylers: [{ visibility: 'off' }]
                },
                {
                    featureType: 'transit',
                    stylers: [{ visibility: 'off' }]
                }
            ]
        });

        let that = this;

        // Create the DIV to hold the control and call the CenterControl()
        // constructor passing in this DIV.
        var centerControlDiv: any = document.createElement('div');
        that.centerControl(centerControlDiv, mapEvent);

        centerControlDiv.index = 1;
        mapEvent.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControlDiv);

        var icon = '<span class="map-icon map-icon-night-club"></span>';

        var image = {
            path: mapIcons.shapes.MAP_PIN,
            fillColor: '#820080',
            fillOpacity: 1,
            strokeColor: '',
            strokeWeight: 0,
            scale: 1
        };

        infowindow = new google.maps.InfoWindow();

        if (this.placeId) {
            let that = this;
            var service = new google.maps.places.PlacesService(mapEvent);
            if (that.placeId) {
                that.currentPlaceId = that.placeId
            } else {
                that.currentPlaceId = that.homePlaceId
            }
            return await service.getDetails({
                placeId: that.currentPlaceId
            }, async function(result) {

                that.selectedPlace = result
                that.setAddress(result);

                that.createMarker(result);

                let newCenter = { lat: result.geometry.location.lat(), lng: result.geometry.location.lng() }
                mapEvent.setCenter(newCenter);
                mapEvent.setZoom(Config.DEFAULT_ZOOM_HIGH);
            });
        }
    }

    async initAutocomplete() {
            if(this.actionType == "edit"){
                var input = document.getElementById('input-address-event-edit').getElementsByTagName('input')[0];
            } else {
                var input = document.getElementById('input-address-event').getElementsByTagName('input')[0];
            }
            var options = {
                types: ['geocode', 'establishment'],
                componentRestrictions: { country: "fr" }
            };

            this.autoComplete = new google.maps.places.Autocomplete(input, options);

            this.autoComplete.addListener('place_changed', async () => {
                this.placeId = this.autoComplete.getPlace().place_id;
                this.selectedPlace = this.autoComplete.getPlace();
                this.eventPlace = '';

                this.setAddress(this.autoComplete.getPlace());
                this.initMap()
            });
    }

    setAddress(place) {

        let isEstablishment = false;
        this.eventPlace = place.name;
        this.streetNumber = '';
        this.street = '';
        this.city = '';
        if (place.types.indexOf("establishment") != -1) {
            isEstablishment = true;
            this.eventPlace = place.name
        }

        if (place.address_components) {
            for (let i = 0; i < place.address_components.length; i++) {
                let component = place.address_components[i];
                let addressType = component.types[0];

                switch (addressType) {
                    case 'street_number':
                        this.streetNumber = component.long_name;
                        break;
                    case 'route':
                        this.street = component.short_name;
                        break;
                    case 'locality':
                        this.city = component.long_name;
                        break;
                    case 'postal_code':
                        this.zipCode = component.long_name;
                        break;
                    case 'country':
                        this.country = component.long_name;
                        break;
                }
            }
        }

        if (!this.streetNumber) { this.streetNumber = '' }
        if (!this.street) { this.street = '' }
        if (!this.city) { this.city = '' }
        if (!this.zipCode) { this.zipCode = '' }
        if (!this.country) { this.country = '' }

        if (this.streetNumber != '' && this.street != '' && this.city != '') {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.streetNumber + ' ' + this.street + ', ' + this.city;
            } else {
                this.eventPlace = this.streetNumber + ' ' + this.street + ', ' + this.city;
            }
        }
        else if (this.street != '' && this.city != '') {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.street + ', ' + this.city
            } else {
                this.eventPlace = this.street + ', ' + this.city
            }
        }
        else if (this.city != '') {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.city;
            } else {
                this.eventPlace = this.city;
            }
        }
        else {
            this.eventPlace = '';
        }
    }

    clearFields() {
        this.eventName = "";
        this.beginDate = this.defaultDate;
        this.beginHour = this.defaultHour;
        this.participants = false;
        this.eventPlace = "";
        this.placeId = "";
        this.selectedPlace = "";
    }

    back() {
        this._location.back();
    }

    drawCircleAvatars(){
        var imgs = document.getElementById("markerLayer").getElementsByTagName('img');
        for(let i = 0;i < imgs.length; i++)
        {
            //imgs[i].parentElement.style.display = "inline-block";
            //imgs[i].parentElement.style.position = "relative";
            //imgs[i].parentElement.style.width = "60px";
            //imgs[i].parentElement.style.height ="60px";
            //imgs[i].parentElement.style.overflow = "hidden";
            imgs[i].parentElement.style.borderRadius = "50%";
            //imgs[i].parentElement.style.borderColor = "black";
            imgs[i].parentElement.style.borderWidth = "1px";
            imgs[i].style.objectFit = "cover";
        }
    }

    createHomeMarker(){
        var image = {
                    path: mapIcons.shapes.MAP_PIN,
                    fillColor: '#F400FE',
                    fillOpacity: 1,
                    strokeColor: '',
                    strokeWeight: 0,
                    scale: 1
                };
        var marker = new mapIcons.Marker({
                    map: mapEvent,
                    position: this.pos,
                    zIndex: 5,
                    icon: image,
                    map_icon_label: '<span class="map-icon map-icon-insurance-agency"></span>'
                });
        this.markers.push(marker);
    }

    getDayShortNames(){
        return [
              this.translate.instant('common.SUNDAY_SHORT'),
              this.translate.instant('common.MONDAY_SHORT'),
              this.translate.instant('common.TUESDAY_SHORT'),
              this.translate.instant('common.WEDNESDAY_SHORT'),
              this.translate.instant('common.THURSDAY_SHORT'),
              this.translate.instant('common.FRIDAY_SHORT'),
              this.translate.instant('common.SATURDAY_SHORT'),
      ];
    }

}
