import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/providers/auth/auth.service';
import * as Config from '../../../utils/config';
import { FavoriteService } from 'src/app/providers/favorite/favorite.service';
import { AddFavoriteEventPage } from '../add-favorite-event/add-favorite-event.page';

declare var google;
declare var MarkerClusterer: any;
let mapLocation: any;
declare var mapIcons;
let infowindow: any;
let placeInfo: any;


@Component({
    selector: 'app-add-location',
    templateUrl: './add-location.page.html',
    styleUrls: ['./add-location.page.scss'],
})
export class AddLocationPage implements OnInit {

    @ViewChild('mapLocation') mapElement: ElementRef;

    autoComplete: any;
    placeId: any;
    selectedPlace: any;
    markerCluster: any;


    address: any;
    zipCode: any;
    city: any;
    country: any;
    streetNumber: any;
    street: any;
    eventPlace: any;

    uid: any;
    pos: any;
    zoom: any;
    isService: boolean = false;
    markers = [];
    favoriteList = [];
    tagList;

    public isLoading: boolean = false;
    // Options cluster
    mcOptions = {
        gridSize: 100,
        minimumClusterSize: 4,
        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
    };


    constructor(
        private modalCtrl: ModalController,
        private loadingController: LoadingController,
        private translate: TranslateService,
        private authService: AuthService,
        private favoriteService: FavoriteService
    ) { }

    ngOnInit() {
    }

    async ionViewWillEnter() {

        this.uid = await this.authService.getCurrentUserId();

        this.getAllFavorite();

        await this.init();

    }

    async init() {

        const loading = await this.loadingController.create({
            message: this.translate.instant('common.LOADING')
        });
        await loading.present();

        await this.initAutocomplete().then(() => {
            this.authService.getUserCoords(this.uid).then(async res => {
                if (res) {
                    if (res.latitude != '' && res.longitude != '') {
                        this.pos = { lat: res.latitude, lng: res.longitude }
                        this.zoom = Config.DEFAULT_ZOOM_HIGH;
                    } else {
                        this.pos = { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG }
                        this.zoom = Config.DEFAULT_ZOOM_LOW;
                    }
                } else {
                    this.pos = { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG }
                    this.zoom = Config.DEFAULT_ZOOM_LOW;
                }
            }).then(async () => {
                await this.initMap();
                var image = {
                path: mapIcons.shapes.MAP_PIN,
                fillColor: '#F400FE',
                fillOpacity: 1,
                strokeColor: '',
                strokeWeight: 0,
                scale: 1
            };

            var marker = new mapIcons.Marker({
                map: mapLocation,
                position: this.pos,
                zIndex: 9,
                icon: image,
                map_icon_label: '<span class="map-icon map-icon-insurance-agency"></span>'
            });
                    
                await loading.dismiss();
            });
        });
    }

    getIcon(place) {
        var iconBase = '../../../assets/images/icon-upviewsly.svg';
        if (typeof place !== 'undefined') {
            if (typeof place.types !== 'undefined') {
                var types = place.types;
                var typeRestaurant = (types.indexOf("restaurant") > -1);
                var typeBar = (types.indexOf("bar") > -1);
                var typeCafe = (types.indexOf("cafe") > -1);

                if (typeBar) {
                    iconBase = '<span class="map-icon map-icon-night-club"></span>';
                    this.isService = true;
                }
                else if (typeRestaurant) {
                    iconBase = '<span class="map-icon map-icon-restaurant"></span>';
                    this.isService = true;
                }
                else if (typeCafe) {
                    iconBase = '<span class="map-icon map-icon-cafe"></span>';
                    this.isService = true;
                } else {
                    this.isService = false;
                }
                return iconBase;
            }
        }
        return iconBase;
    }

    clearMarkers() {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
    }

    async initMap() {
        mapLocation = new google.maps.Map(this.mapElement.nativeElement, {
            center: { lat: this.pos.lat, lng: this.pos.lng },
            zoom: this.zoom,
            streetViewControl: false,
            zoomControl: false,
            mapTypeControl: false,
            draggable: true,
            styles: [
                {
                    featureType: 'poi',
                    stylers: [{ visibility: 'off' }]
                },
                {
                    featureType: 'transit',
                    stylers: [{ visibility: 'off' }]
                }
            ]
        });

        var centerControlDiv: any = document.createElement('div');
        this.centerControl(centerControlDiv, mapLocation);
        centerControlDiv.index = 1;
        mapLocation.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControlDiv);

        infowindow = new google.maps.InfoWindow();
    }

            // Center control map
        centerControl(controlDiv, map) {
            let self = this;
            this.isLoading = false;
            // Set CSS for the control border.
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = '#fff';
            controlUI.style.border = '2px solid #fff';
            controlUI.style.borderRadius = '3px';
            controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
            controlUI.style.cursor = 'pointer';
            controlUI.style.marginBottom = '22px';
            controlUI.style.textAlign = 'center';
            controlUI.title = this.translate.instant('common.SEARCH_POI');
            controlDiv.appendChild(controlUI);
    
            // Set CSS for the control interior.
            var controlText = document.createElement('div');
            controlText.style.color = 'rgb(25,25,25)';
            controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
            controlText.style.fontSize = '16px';
            controlText.style.lineHeight = '38px';
            controlText.style.paddingLeft = '5px';
            controlText.style.paddingRight = '5px';
            controlText.innerHTML = this.translate.instant('common.SEARCH_POI');
            controlUI.appendChild(controlText);
            // Setup the click event listeners: simply set the map to Chicago.
            controlUI.addEventListener('click', function() {
                if (self.isLoading) {
                    return
                }
                controlText.innerHTML = self.translate.instant('common.LOADING');
                var getNextPage = null;
                var service = new google.maps.places.PlacesService(map);
                
                self.clearMarkers();
                self.markers = [];
                if (typeof self.markerCluster !== 'undefined' && self.markerCluster.length > 0) {
                    self.markerCluster.clearMarkers();
                }
                self.createHomeMarker();
                var bounds = new google.maps.LatLngBounds(map.getBounds().getSouthWest(), map.getBounds().getNorthEast());
                return service.nearbySearch({
                    //location: { lat: map.getCent er().lat(), lng: map.getCenter().lng() },
                    //radius: self.getDistanceM(map.getBounds().getNorthEast().lat(), map.getBounds().getNorthEast().lng(), map.getBounds().getSouthWest().lat(), map.getBounds().getSouthWest().lng()),
                    bounds: bounds,
                    types: ['restaurant', 'bar', 'cafe']
                }, (results, status, pagination) => {
                    if (status === google.maps.places.PlacesServiceStatus.OK) {
                        self.isLoading = true;
                        for (var i = 0; i < results.length; i++) {
                            self.createMarker(results[i]);
                            // bounds.extend(new google.maps.LatLng(results[i].geometry.location.lat(), results[i].geometry.location.lng()))
                            // On ending request for one query
                            if (i + 1 === results.length) {
                                if (pagination.hasNextPage) {
                                    pagination.nextPage();
                                }
    
                                if (!pagination.hasNextPage) {
                                    // map.fitBounds(bounds)
                                    controlText.innerHTML = self.translate.instant('common.SEARCH_POI');
                                    self.isLoading = false;
                                    setTimeout(() => { self.markerCluster = new MarkerClusterer(map, self.markers, self.mcOptions);}, 500);
                                }
                            }
                        }
    
                    } else {
                        controlText.innerHTML = self.translate.instant('common.SEARCH_POI');
                    }
                })
            });
        }

    async initAutocomplete() {

        if (document.getElementById('input-address')) {

            var input = document.getElementById('input-address').getElementsByTagName('input')[0];
            var options = {
                types: ['geocode', 'establishment'],
                componentRestrictions: { country: "fr" }
            };

            this.autoComplete = new google.maps.places.Autocomplete(await input, options);

            this.autoComplete.addListener('place_changed', async () => {
                this.selectedPlace = this.autoComplete.getPlace();
                this.eventPlace = '';
                this.placeId = this.autoComplete.getPlace().place_id;

                this.setAddress(this.autoComplete.getPlace());
                this.createMarker(this.autoComplete.getPlace());
            });
        }
    }

    setAddress(place) {

        let isEstablishment = false;

        if (place.types.indexOf("establishment") != -1) {
            isEstablishment = true;
            this.eventPlace = place.name
        }

        for (let i = 0; i < place.address_components.length; i++) {
            let component = place.address_components[i];
            let addressType = component.types[0];

            switch (addressType) {
                case 'street_number':
                    this.streetNumber = component.long_name;
                    break;
                case 'route':
                    this.street = component.short_name;
                    break;
                case 'locality':
                    this.city = component.long_name;
                    break;
                case 'postal_code':
                    this.zipCode = component.long_name;
                    break;
                case 'country':
                    this.country = component.long_name;
                    break;
            }
        }

        if (!this.streetNumber) { this.streetNumber = null }
        if (!this.street) { this.street = null }
        if (!this.city) { this.city = null }
        if (!this.zipCode) { this.zipCode = null }
        if (!this.country) { this.country = null }

        if (this.streetNumber != null && this.street != null && this.city != null) {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.streetNumber + ' ' + this.street + ', ' + this.city;
            } else {
                this.eventPlace = this.streetNumber + ' ' + this.street + ', ' + this.city;
            }
        }
        else if (this.street != null && this.city != null) {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.street + ', ' + this.city
            } else {
                this.eventPlace = this.street + ', ' + this.city
            }
        }
        else if (this.city != null) {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.city;
            } else {
                this.eventPlace = this.city;
            }
        }
        else {
            this.eventPlace = null;
        }
        let newCenter = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() }
        mapLocation.setCenter(newCenter);
        mapLocation.setZoom(Config.DEFAULT_ZOOM_HIGH);
    }

    createHomeMarker(){
        var image = {
                    path: mapIcons.shapes.MAP_PIN,
                    fillColor: '#F400FE',
                    fillOpacity: 1,
                    strokeColor: '',
                    strokeWeight: 0,
                    scale: 1
                };
        var marker = new mapIcons.Marker({
                    map: mapLocation,
                    position: this.pos,
                    zIndex: 5,
                    icon: image,
                    map_icon_label: '<span class="map-icon map-icon-insurance-agency"></span>'
                });
        this.markers.push(marker);
    }

    validateModal() {

        let newPlace = {
            address: this.eventPlace,
            placeId: this.placeId
        }
        this.modalCtrl.dismiss(newPlace);
    }

    cancel() {
        this.modalCtrl.dismiss();
    }

        checkFavoriteFromPoi(placeId) {
        if (typeof this.favoriteList !== 'undefined') {
            if (this.favoriteList.length != 0) {
                for (let i = 0; i < this.favoriteList.length; i++) {
                    if (placeId === this.favoriteList[i].id) {
                        return true;
                    }
                }
                return false;
            }
        }
    }

        async openFavorite() {
        const modal = await this.modalCtrl.create({
            component: AddFavoriteEventPage
        });
        modal.present();


        modal.onDidDismiss().then(async res => {
            var service = new google.maps.places.PlacesService(mapLocation);
            let that = this;
            return await service.getDetails({
                placeId: await res.data.id
            }, async function(result) {
                that.selectedPlace = result
                that.placeId = result.place_id;
                that.setAddress(result);
                that.setAddress(result);
                let newCenter = { lat: result.geometry.location.lat(), lng: result.geometry.location.lng() }
                mapLocation.setCenter(newCenter);
                mapLocation.setZoom(Config.DEFAULT_ZOOM_HIGH);
                that.createMarker(result);
            })
        })
    }

    createMarker(place){
         var icon = this.getIcon(place)

         let image = {
            path: mapIcons.shapes.MAP_PIN,
            fillColor: this.checkFavoriteFromPoi(this.placeId) ? '#F400FE' : '#820080',
            fillOpacity: 1,
            strokeColor: '',
            strokeWeight: 0,
            scale: 1
        };
                if(this.isService){
                    var marker = new mapIcons.Marker({
                        map: mapLocation,
                        icon: image,
                        position: place.geometry.location,
                        map_icon_label: icon,
                        zIndex: 9,
                    });
                } else {
                    var marker = new google.maps.Marker({
                        position: place.geometry.location,
                        icon: icon,
                        map: mapLocation
                    });
                }
                
                this.markers.push(marker);

                var that = this;
                google.maps.event.addListener(marker, 'click', async function() {
                    function round(value, precision) {
                        var multiplier = Math.pow(10, precision || 0);
                        return Math.round(value * multiplier) / multiplier;
                    }
                    if (place.price_level) {
                        var priceLevel;
                        switch (place.price_level) {
                            case 0:
                                priceLevel = "€";
                                break;
                            case 1:
                                priceLevel = "€";
                                break;
                            case 2:
                                priceLevel = "€€";
                                break;
                            case 3:
                                priceLevel = "€€€";
                                break;
                            case 5:
                                priceLevel = "€€€€";
                                break;
                            default:
                                priceLevel = "€";
                                break;
                        }
                    }

            let ratingHtml = "";
            if(place.rating){
                ratingHtml = that.translate.instant('map.RATING') + round(place.rating, 1).toFixed(1) + ' / 5  ' + '(' + place.user_ratings_total +
                    that.translate.instant('map.RATING_NUMBER') + ')' + " &bull; ";
                if(place.price_level){
                    ratingHtml +=  priceLevel;
                }
            }
            var infowindowContent =
                    '<div class="info-window-content" style="min-width: 185px; min-height: 80px; padding: 0 10px 0 0;">' +
                    '<strong style="display: inline-block; margin: 2px 0 6px;">' + place.name + '</strong><br>' +
                    place.vicinity + "<br>" +
                    ratingHtml +
                    '<ul class="info-window--list-actions" style="list-style: none; margin: .75em 0; padding: 0;"><li style="margin: 0 0 .3rem 0;"><ion-text><a style="font-weight: 400; text-decoration: none" class="font-size-0_8" target="_blank" href="https://www.google.com/maps/search/?api=1&query=' + place.name + '&query_place_id=' + place.place_id + '">'
                    + 'Voir sur Google' + '</a></ion-text></li>' +
                    '<li style="margin: 0 0 .1rem 0;"><a class="ion-no-margin ion-no-padding ion-button" size="small" fill="clear" color="primary" id="useButton_' + place.place_id + '" style="font-weight: 400; text-decoration: none;">' + that.translate.instant('map.USE_PLACE') + '</a></li>' +
                    '</ul></div>';

                        infowindow.setContent(infowindowContent);
                        infowindow.open(mapLocation, this);

                        new google.maps.event.addListenerOnce(infowindow, 'domready', () => {

                        document.getElementById('useButton_' + place.place_id).addEventListener("click", async () => {
                            let service = new google.maps.places.PlacesService(mapLocation);
                            return await service.getDetails({
                                placeId: place.place_id
                            }, async function(result) {

                                that.selectedPlace = result
                                that.placeId = place.place_id;
                                that.setAddress(result);
                                infowindow.close();
                            });
                        });
                });
                })
                
    }

        getAllFavorite() {
            this.favoriteService.getUserFavoritePlaces().subscribe(data => {
                this.favoriteList = data.map(e => {
                    this.tagList = e.payload.doc.data()['tags'];
                    return {
                        docId: e.payload.doc.id,
                        id: e.payload.doc.data()['id'],
                        name: e.payload.doc.data()['name'],
                        address: e.payload.doc.data()['address'],
                        lat: e.payload.doc.data()['lat'],
                        lng: e.payload.doc.data()['lng'],
                        tags: e.payload.doc.data()['tags']
                    };
                })
            });
        
    }

}
