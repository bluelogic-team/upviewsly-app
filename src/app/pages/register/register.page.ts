import { Component, OnInit } from '@angular/core';
import { Platform, NavController, ToastController, LoadingController, ModalController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { AuthService } from '../../providers/auth/auth.service';

import { TranslateService } from "@ngx-translate/core";
import { OnboardingInfoPage } from '../onboarding-info/onboarding-info.page';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

    registerForm: FormGroup;
    email: string = '';
    password: string = '';
    errorMessage: string = '';
    successMessage: string = '';
    loading: any;


    constructor(
        private router: Router,
        private navCtrl: NavController,
        private splashscreen: SplashScreen,
        public authService: AuthService,
        public platform: Platform,
        public storage: Storage,
        public toastController: ToastController,
        public loadingController: LoadingController,
        public alertController: AlertController,
        private translate: TranslateService,
        private modalCtrl: ModalController
    ) {
        this.registerForm = new FormGroup({
            password: new FormControl('', Validators.required),
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern(
                    "[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
                )
            ]))
        });
    }

    ngOnInit() {
    }

    async ionViewDidEnter() {
        this.splashscreen.hide();
        this.registerForm.controls.email.setValue("");
        this.registerForm.controls.password.setValue("");
    }

    ionViewWillLeave() {
        if (this.loading != undefined) {
            this.loading.dismiss();
        }
    }

    async presentLoading() {
        this.loading = await this.loadingController.create({
            message: this.translate.instant('common.LOADING'),
            //duration: 10000
        });
        await this.loading.present();
    }

    onSubmit(value) {
        if (this.registerForm.valid) {
            //this.router.navigate(['tabs']);
            this.tryRegister(value);
        }
        else {
            this.errorMessage = this.translate.instant('common.INPUT_MISSING');
            //this.presentToast();
        }
    }


    loginWithGoogle() {
        this.presentLoading();
        if (this.platform.is('cordova')) {
            this.authService.nativeGoogleLogin()
                .then(res => {
                    this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                        if (typeof user !== 'undefined') {
                            if (!user.firstName || !user.lastName || !user.address || !user.username) {
                                let modal = await this.modalCtrl.create({
                                    component: OnboardingInfoPage,
                                    componentProps: {
                                        firstName: user.firstName,
                                        lastName: user.lastName,
                                        address: user.address,
                                        zipCode: user.zipCode,
                                        latitude: user.latitude,
                                        longitude: user.longitude,
                                        placeId: user.placeId,
                                        city: user.city,
                                        country: user.country
                                    },
                                    backdropDismiss: false
                                });
                                await modal.present();
                                this.loading.dismiss();
                            } else {
                                this.router.navigate(['tabs']);
                                this.loading.dismiss();
                            }
                        }
                    }).catch(err => {
                        console.log(err)
                        this.loading.dismiss();
                    })
                })
                .catch(err => {
                    this.errorMessage = err.message;
                    this.loading.dismiss();
                })
        } else {
            this.authService.webGoogleLogin().then(() => {
                this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                    if (typeof user !== 'undefined') {
                        if (!user.firstName || !user.lastName || !user.address || !user.username) {
                            let modal = await this.modalCtrl.create({
                                component: OnboardingInfoPage,
                                componentProps: {
                                    firstName: user.firstName,
                                    lastName: user.lastName,
                                    address: user.address,
                                    zipCode: user.zipCode,
                                    latitude: user.latitude,
                                    longitude: user.longitude,
                                    placeId: user.placeId,
                                    city: user.city,
                                    country: user.country
                                },
                                backdropDismiss: false
                            });
                            await modal.present();
                            this.loading.dismiss();
                        } else {
                            this.router.navigate(['tabs']);
                            this.loading.dismiss();
                        }
                    }
                }).catch(err => {
                    console.log(err)
                    this.loading.dismiss();
                })
            });
        }
    }

    loginWithFacebook() {
        this.presentLoading();
        if (this.platform.is('cordova')) {
            this.authService.nativeFacebookLogin()
                .then(res => {
                    this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                        if (typeof user !== 'undefined') {
                            if (!user.firstName || !user.lastName || !user.address || !user.username) {
                                let modal = await this.modalCtrl.create({
                                    component: OnboardingInfoPage,
                                    componentProps: {
                                        firstName: user.firstName,
                                        lastName: user.lastName,
                                        address: user.address,
                                        zipCode: user.zipCode,
                                        latitude: user.latitude,
                                        longitude: user.longitude,
                                        placeId: user.placeId,
                                        city: user.city,
                                        country: user.country
                                    },
                                    backdropDismiss: false
                                });
                                await modal.present();
                                this.loading.dismiss();
                            } else {
                                this.router.navigate(['tabs']);
                                this.loading.dismiss();
                            }
                        }
                    }).catch(err => {
                        console.log(err)
                        this.loading.dismiss();
                    })
                })
                .catch(err => {
                    this.loading.dismiss();
                    if (err.code == 'auth/account-exists-with-different-credential') {
                        this.errorMessage = this.translate.instant('common.ACCOUNT_ALREADY_EXISTS');
                    }
                    else {
                        this.errorMessage = err.message;
                    }
                })
        } else {
            this.authService.nativeFacebookLogin()
                .then(res => {
                    this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                        if (typeof user !== 'undefined') {
                            if (!user.firstName || !user.lastName || !user.address || !user.username) {
                                let modal = await this.modalCtrl.create({
                                    component: OnboardingInfoPage,
                                    componentProps: {
                                        firstName: user.firstName,
                                        lastName: user.lastName,
                                        address: user.address,
                                        zipCode: user.zipCode,
                                        latitude: user.latitude,
                                        longitude: user.longitude,
                                        placeId: user.placeId,
                                        city: user.city,
                                        country: user.country
                                    },
                                    backdropDismiss: false
                                });
                                await modal.present();
                                this.loading.dismiss();
                            } else {
                                this.router.navigate(['tabs']);
                                this.loading.dismiss();
                            }
                        }
                    }).catch(err => {
                        this.loading.dismiss();
                        console.log(err)
                    })
                })
                .catch(err => {
                    this.loading.dismiss();
                    if (err.code == 'auth/account-exists-with-different-credential') {
                        this.errorMessage = this.translate.instant('common.ACCOUNT_ALREADY_EXISTS');
                    }
                    else {
                        this.errorMessage = err.message;
                    }
                })
        }
    }

    tryFacebookLogin() {/*
    this.authService.nativeFacebookLogin()
    .then(res =>{
      this.router.navigate(['/user']);
    }, err => console.log(err)
    )*/
    }
    /*
      tryGoogleLogin(){
        this.authService.doGoogleLogin()
        .then(res =>{
          this.router.navigate(['/user']);
        }, err => console.log(err)
        )
      }*/

    tryRegister(value) {
        this.authService.doRegister(value)
            .then(res => {
                this.errorMessage = "";
                // this.presentToast();
                this.successMessage = this.translate.instant('register.ACCOUNT_CREATED');
                this.tryLogin(value);
            }, err => {
                err.code === "auth/email-already-in-use" && (this.errorMessage = this.translate.instant('common.ACCOUNT_ALREADY_EXISTS'));
                err.code === "auth/weak-password" && (this.errorMessage = this.translate.instant('common.ACCOUNT_WEAK_PASSWORD'));
                this.successMessage = "";
            })
    }

    async presentToast() {
        const toast = await this.toastController.create({
            message: this.translate.instant('register.ACCOUNT_CREATED'),
            duration: 3000
        });
        toast.present();
    }

    tryLogin(value) {
        this.presentLoading();
        this.authService.doLogin(value)
            .then(res => {
                this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                    if (typeof user !== 'undefined') {
                        if (!user.firstName || !user.lastName || !user.address || !user.username) {
                            let modal = await this.modalCtrl.create({
                                component: OnboardingInfoPage,
                                componentProps: {
                                    firstName: user.firstName,
                                    lastName: user.lastName,
                                    address: user.address,
                                    zipCode: user.zipCode,
                                    latitude: user.latitude,
                                    longitude: user.longitude,
                                    placeId: user.placeId,
                                    city: user.city,
                                    country: user.country
                                },
                                backdropDismiss: false
                            });
                            await modal.present();
                            this.loading.dismiss();
                        }
                    }
                }).catch(err => {
                    console.log(err)
                    this.loading.dismiss();
                })
            }, err => {
                this.loading.dismiss();
                if (err.code == 'auth/wrong-password') {
                    this.errorMessage = this.translate.instant('common.INCORRECT_CREDENTIAL');
                }
                else if (err.code == 'auth/user-not-found') {
                    this.errorMessage = this.translate.instant('common.ACCOUNT_UNKNOWN');
                }
                else {
                    this.errorMessage = err.message;
                }
            })
    }

}