import { Component, OnInit } from '@angular/core';
import { Platform, ModalController, NavParams, LoadingController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TranslateService } from "@ngx-translate/core";
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthService, User } from '../../providers/auth/auth.service';
import { FriendsService } from '../../providers/friends/friends.service';

@Component({
    selector: 'app-add-user',
    templateUrl: './add-user.page.html',
    styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit {

    private usersCollection: AngularFirestoreCollection<User>;
    private users;
    private usersPseudo;
    private usersLastname;

    displayedUsers = [];
    filteredUsers = [];
    selectedUsers = [];
    searchUserString = "";

    emailForm: FormGroup;
    email: string = "";
    errorMessage: string = '';
    succesMessage: string = '';

    constructor(
        private authService: AuthService,
        private db: AngularFirestore,
        private modalCtrl: ModalController,
        private translate: TranslateService,
        private friendsService: FriendsService,
        private loadingCtrl: LoadingController
    ) {
        this.emailForm = new FormGroup({
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern(
                    "[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
                )
            ]))
        });

    }

    ngOnInit() {
    }

    ionViewDidEnter() {
        // this.getUsers();
    }

    closeModal() {
        this.modalCtrl.dismiss();
    }

    sendFriendRequest(friend) {
        this.friendsService.sendFriendRequest(friend);
    }

    // getUserByEmail(email): AngularFirestoreCollection {
    //   return this.db.collection('users', 
    //   ref => ref.where('email', '==', email));

    // }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }

    lowercaseFirstLetter(string){
        return string.charAt(0).toLowerCase() + string.slice(1).toLowerCase();
    }

    async searchUser(ev: any) {
        if (ev.detail.value.length >= 3) {
            let loading = await this.loadingCtrl.create({
                message: this.translate.instant('friends.SEARCH')
            });
            loading.present();
            this.displayedUsers = [];
            this.filteredUsers = [];
            this.getUsers(ev.detail.value);
        } else {
            this.displayedUsers = [];
            this.filteredUsers = [];
        }
        // set val to the value of the searchbar
        const val = ev.detail.value;
        // this.filteredUsers = this.displayedUsers;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.filteredUsers = this.filteredUsers.filter((item) => {
                if (item.displayName) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                else if (item.firstName) {
                    return (item.firstName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                else {
                    return (item.lastName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
            })
        } else {
            this.filteredUsers = this.displayedUsers;
        }
    }

    async getUsers(user) {
        this.usersCollection = this.db.collection<User>('users', ref => ref.orderBy("firstName").startAt(this.capitalizeFirstLetter(user)).endAt(this.capitalizeFirstLetter(user) + '\uf8ff'));
        let searchUserByLastname = this.db.collection<User>('users', ref => ref.orderBy("lastName").startAt(this.capitalizeFirstLetter(user)).endAt(this.capitalizeFirstLetter(user) + '\uf8ff'));
        let searchUserByPseudonyme = this.db.collection<User>('users', ref => ref.orderBy("username").startAt(this.lowercaseFirstLetter(user)).endAt(this.lowercaseFirstLetter(user) + '\uf8ff'));

        this.users = this.usersCollection.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const firebaseId = a.payload.doc.id;
                    return { firebaseId, ...data };
                });
            })
        );

        this.usersPseudo = searchUserByPseudonyme.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const firebaseId = a.payload.doc.id;
                    return { firebaseId, ...data };
                });
            })
        )

        this.usersLastname = searchUserByLastname.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const firebaseId = a.payload.doc.id;
                    return { firebaseId, ...data };
                });
            })
        )

        let combine = combineLatest(this.users, this.usersPseudo, this.usersLastname).pipe(
            map((actions: any[]) => {
                return actions[0].concat(actions[1].concat(actions[2]));
            })
        );

        combine.subscribe(value => {
            //remove duplicates
            value = value.filter((obj, index, array) =>
                 index === array.findIndex((findObj) =>
                    findObj.firebaseId === obj.firebaseId
                 )
            );
            value.forEach(element => {
                if (element.firebaseId != this.authService.getCurrentUserId()) {
                    this.checkAlreadyFriends(element.firebaseId).then((isFriend) => {
                        this.checkAlreadyRequested(element.firebaseId).then((isAlreadyRequested) => {
                            let newFriend = Object.assign(element, { isAlreadyFriend: isFriend }, { isAlreadyRequested: isAlreadyRequested });
                            this.displayedUsers.push(newFriend);
                        });
                    });
                }
            });
            this.loadingCtrl.dismiss();
            this.filteredUsers = this.displayedUsers;
        });
    }

    // Check or uncheck friend
    toggleUser(user) {
        // if (contact.checked && !(this.selectedContacts.filter(sc => sc.email === contact.email).length > 0)) {
        if (!user.checked) {
            this.selectedUsers.push(user);
        } else {
            let index = this.selectedUsers.indexOf(user);
            if (index > -1) {
                this.selectedUsers.splice(index, 1);
            }
        }
    }

    checkAlreadyFriends(friendId) {
        return new Promise(resolve => {
            let userId = this.authService.getCurrentUserId();
            this.friendsService.checkUserIsAlreadyFriend(userId, friendId)
                .subscribe(
                    (data: any) => {
                        if (data.length == 0) {
                            resolve(false);
                        } else {
                            resolve(true);
                        }
                    })
        })
    }

    checkAlreadyRequested(friendId) {
        return new Promise(resolve => {
            let userId = this.authService.getCurrentUserId();
            this.friendsService.checkUserAlreadyFriendRequested(userId, friendId)
                .subscribe(
                    (data: any) => {
                        if (data.length == 0) {
                            resolve(false);
                        } else {
                            resolve(true);
                        }
                    })
        })
    }
}
