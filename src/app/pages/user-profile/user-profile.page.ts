import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ProfileService } from 'src/app/providers/profile/profile.service';
import { NavParams, ModalController } from '@ionic/angular';
import { FriendsService } from 'src/app/providers/friends/friends.service';
import { AuthService } from 'src/app/providers/auth/auth.service';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.page.html',
    styleUrls: ['./user-profile.page.scss'],
})

export class UserProfilePage implements OnInit {
    constructor(public translate: TranslateService,
        private profileService: ProfileService,
        private navParam: NavParams,
        private modalCtrl: ModalController,
        private friendService: FriendsService,
        private authService: AuthService) { }

    private userId: string = this.navParam.get('userId');
    private profileObservable: any;
    private checkUserIsAlreadyFriend: any;
    private checkUserAlreadyFriendRequested: any;
    public user: any;
    public alreadyFriends: boolean = false;
    public alreadyFriendRequested: boolean = false;

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.checkUserIsAlreadyFriend = this.friendService.checkUserIsAlreadyFriend(this.authService.getCurrentUserId(), this.userId).subscribe((isAlreadyFriend: any) => {
            if (typeof isAlreadyFriend !== 'undefined') {
                if(typeof isAlreadyFriend[0] !== 'undefined') {
                    if (isAlreadyFriend[0].id) {
                        this.alreadyFriends = true;
                    }
                }
            }
        })

        this.profileObservable = this.profileService.getUserDetails(this.userId).subscribe(user => {
            this.user = user;
        })

        this.checkUserAlreadyFriendRequested = this.friendService.checkUserAlreadyFriendRequested(this.authService.getCurrentUserId(), this.userId).subscribe((checkUserAlreadyFriendRequested: any) => {
            if (typeof this.alreadyFriendRequested !== 'undefined') {
                if(typeof checkUserAlreadyFriendRequested[0] !== 'undefined') {
                    if (checkUserAlreadyFriendRequested[0].id) {
                        this.alreadyFriendRequested = true;
                    }
                }
            }
        })
    }

    sendFriendRequest() {
        const friend = { firebaseId: this.userId };
        this.friendService.sendFriendRequest(friend).then(() => this.alreadyFriendRequested = true).catch(() => this.alreadyFriendRequested = false);
    }

    closeModal() {
        this.modalCtrl.dismiss();
    }

    ionViewWillLeave() {
        this.profileObservable && this.profileObservable.unsubscribe();
        this.checkUserIsAlreadyFriend && this.checkUserIsAlreadyFriend.unsubscribe();
        this.checkUserAlreadyFriendRequested && this.checkUserAlreadyFriendRequested.unsubscribe();
    }

}
