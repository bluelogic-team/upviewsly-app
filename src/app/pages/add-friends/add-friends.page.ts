import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { AuthService } from '../../providers/auth/auth.service';
import { FriendsService } from '../../providers/friends/friends.service';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { AddUserPage } from '../add-user/add-user.page';
import { ContactsPage } from '../contacts/contacts.page';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-add-friends',
  templateUrl: './add-friends.page.html',
  styleUrls: ['./add-friends.page.scss'],
})
export class AddFriendsPage implements OnInit {

  selectedInvites = [];
  selectedUsers = [];

  fbFriends = [];
  fbFriendsDisplayed = false;
  hasFacebookLogin;
  noFbFriends = false;
  loader: any;


  constructor(
    public modalController: ModalController,
    public loadingController: LoadingController,
    public authService: AuthService,
    public friendsService: FriendsService,
    public afdb: AngularFirestore,
    public http: HttpClient,
    public route: ActivatedRoute,
    public router: Router,
    private translate: TranslateService,
    private socialSharing: SocialSharing,
  ) {
    this.checkUserHasFacebookLogin();
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    // this.getUserFbFriends();
  }

  async addFriendsFromAppUsers() {
    const modal = await this.modalController.create({
      component: AddUserPage,
      componentProps: {}
    });

    await modal.present();
  }

  // open modal to import member from the phone contacts
  async addFriendsFromContacts(contactsFromApp) {
    const modal = await this.modalController.create({
      animated: true,
      backdropDismiss: true,
      showBackdrop: true,
      component: ContactsPage,
      componentProps: { contactsFromApp: contactsFromApp }
    });
    await modal.present();
  }


  closeModal() {
    this.modalController.dismiss();
  }

  async sendFriendRequest(friend) {
    await this.friendsService.sendFriendRequest(friend);
  }

  //get friends who already use the app with facebook
  async getUserFbFriends() {
    if (!this.fbFriendsDisplayed) {
      let userAuth = await this.authService.getCurrentUser();

      var userDbRef = this.afdb.collection("users").doc(userAuth.uid);
      userDbRef.ref.get().then(async (doc) => {

        let fbIndex = -1;
        userAuth.providerData.forEach((provider, i) => {
          if (provider.providerId == "facebook.com") {
            fbIndex = i;
          }
        });
        if (fbIndex == -1) {
        } else {
          let graphResult: any = await this.callGraphApi(userAuth.providerData[fbIndex].uid, doc.data().facebookAccessToken);

          this.getFacebookFriendsProfiles(graphResult.data);
        }
        // this.loadingController.dismiss();
      }).catch(err => {
        // this.loadingController.dismiss();
      })
      this.fbFriendsDisplayed = true;
    }
  }

  // call facebook graphc api to get friends who logged once with facebook in the app
  callGraphApi(fbUid, fbAccessToken) {
    return new Promise(async (resolve, reject) => {
      this.http.get(
        "https://graph.facebook.com/v3.3/" + fbUid + "/friends?access_token=" + fbAccessToken
      ).subscribe(
        (data) => {
          if (data != null) {
            resolve(data);
          } else {
            reject('getUserProducts http request data null');
          }
        }, (err) => {
          reject(err);
        }
      );
    });
  }

  // query firestore for each friend detected to retrieve their profile with image and firebase id
  async getFacebookFriendsProfiles(fbFriends) {
    var ref = firebase.firestore().collection('users');
    var query: firebase.firestore.Query;
    let noFbFriends = false;
    fbFriends.forEach(friend => {
      query = ref.where("facebookUid", "==", friend.id);
      query.get().then(snapshot => {
        if (snapshot.size == 0) {
          noFbFriends = true;
        }
        snapshot.forEach(doc => {
          this.checkAlreadyFriends(doc.id).then((isFriend) => {
            this.checkAlreadyRequested(doc.id).then((isAlreadyRequested) => {
              let newFriend = Object.assign(doc.data(), { firebaseId: doc.id }, { isAlreadyFriend: isFriend }, { isAlreadyRequested: isAlreadyRequested });
              this.fbFriends.push(newFriend);
            });
          });
        }, err => {
          console.log(err);
          throw err;
        });
      })
    });
    if (noFbFriends) {
      this.noFbFriends = true;
    }
  }

  checkAlreadyFriends(friendId) {
    return new Promise(resolve => {
      let userId = this.authService.getCurrentUserId();
      this.friendsService.checkUserIsAlreadyFriend(userId, friendId)
        .subscribe(
          (data: any) => {
            if (data.length == 0) {
              resolve(false);
            } else {
              resolve(true);
            }
          })
    })
  }

  checkAlreadyRequested(friendId) {
    return new Promise(resolve => {
      let userId = this.authService.getCurrentUserId();
      this.friendsService.checkUserAlreadyFriendRequested(userId, friendId)
        .subscribe(
          (data: any) => {
            if (data.length == 0) {
              resolve(false);
            } else {
              resolve(true);
            }
          })
    })
  }

  async checkUserHasFacebookLogin() {
    let userAuth = await this.authService.getCurrentUser();
    let fbIndex = -1;
    userAuth.providerData.forEach((provider, i) => {
      if (provider.providerId == "facebook.com") {
        fbIndex = i;
      }
    });
    if (fbIndex == -1) {
      this.hasFacebookLogin = false;
    } else {
      this.hasFacebookLogin = true;
    }
  }

  async share(){
    this.loader = await this.loadingController.create({
      message: this.translate.instant('common.LOADING'),
      spinner: 'crescent', 
    });
    await this.loader.present();
    this.authService.getCurrentUser().then((res) => {
      var docRef = this.afdb.collection("users").doc(res.uid);
            docRef.ref.get().then(doc => {
                if (doc.exists) {
                     this.translate.get("contacts.SHARE_MESSAGE_SMS", { username: doc.data()['username'] }).subscribe((res: string) => {
                     this.socialSharing.share(res).then(() => {
                      console.log('ok');
                      this.loader.dismiss();
                    }).catch(() => {
                      console.log('Fail');
                      this.loader.dismiss();
                    })
                  });
                } else {
                    console.log("error user missing in database");
                    this.loader.dismiss();
                }
            }).catch(err => {
              this.loader.dismiss();
                throw new err;
          });

      }, (error) => {
        this.loader.dismiss();
            throw new error;
      });
  }

}
