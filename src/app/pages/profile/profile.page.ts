import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../providers/auth/auth.service';
import { Storage } from '@ionic/storage';
import { ModalController, NavParams, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { EditProfilePage } from '../edit-profile/edit-profile.page';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { TranslateService } from "@ngx-translate/core";
import { NotificationsService } from 'src/app/providers/notifications/notifications.service';
import { ChatService } from 'src/app/providers/chat/chat.service';
import { SettingsPage } from '../settings/settings.page';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
    userData: any;
    userId: any;
    footer_config: any;

    constructor(
        public modalCtrl: ModalController,
        public alertController: AlertController,
        public router: Router,
        public storage: Storage,
        public authService: AuthService,
        private db: AngularFirestore,
        public translate: TranslateService,
        private notifService: NotificationsService,
        private chatService: ChatService
    ) {
        this.footer_config = {
            isFooter: true
        }
    }

    logout() {
        this.notifService.countObservable && this.notifService.countObservable.unsubscribe();
        this.chatService.unreadMessagesObservable && this.chatService.unreadMessagesObservable.unsubscribe();
        this.chatService.unreadLastMessageObservable && this.chatService.unreadLastMessageObservable.unsubscribe();
        this.chatService.lastReadObservable && this.chatService.lastReadObservable.unsubscribe();
        this.chatService.unreadMessages = 0;
        this.chatService.unreadChatsList = [];
        this.authService.doLogout();
    }

    ionViewWillEnter() {
        //  this.currentUser = this.authService.getCurrentUser();


        this.authService.getCurrentUser().then((res) => {
            this.userId = res.uid;
            var docRef = this.db.collection("users").doc(this.userId);
            docRef.ref.get().then(doc => {
                if (doc.exists) {
                    this.userData = doc.data();

                } else {
                    console.log("error user missing in database");
                }
            }).catch(err => {
                throw new err;
            })
        }, (error) => {
            throw new error;
        });

    }

    async editProfile() {
        let modalEdit = await this.modalCtrl.create({
            component: EditProfilePage,
            componentProps: Object.assign(this.userData, { userId: this.userId })
        });
        await modalEdit.present();
        await modalEdit.onWillDismiss().then(async value => {
            // true if validation button is clicked, false if back button is pressed
            if (value.data) {
                // update userData for the view
                this.db.collection("users").doc(this.userId).ref.get()
                    .then(doc => {
                        this.userData = doc.data();
                    }).catch(err => {
                        throw new err;
                    })
            }
        })

    }


    async presentDeleteProfile() {
        const alert = await this.alertController.create({
            header: this.translate.instant('profile.ALERT_HEADER_CONFIRM_DELETE'),
            message: this.translate.instant('profile.ALERT_MESSAGE_CONFIRM_DELETE'),
            inputs: [
                // {
                //   name: 'email',
                //   type: 'text',
                //   placeholder: this.translate.instant('common.EMAIL')
                // },
                {
                    name: 'password',
                    type: 'password',
                    placeholder: this.translate.instant('common.PASSWORD')
                }
            ],
            buttons: [{
                text: this.translate.instant('common.CANCEL'),
                handler: () => {
                    console.log('Confirm Cancel');
                }
            }, {
                text: this.translate.instant('common.OK'),
                handler: async (data) => {
                    var password = Object.assign({}, { password: data.password });
                   
                    let uId = this.userId;
                    this.authService.deleteProfile(password).then(async (res) => {
                        if (res) {
                            this.router.navigate(['login']);
                        }
                    }, err => {
                        console.log("delete profile page error", err);
                    })

                }
            }

            ]
        });
        await alert.present();
    }

    async openSettings() {
        let modal = await this.modalCtrl.create({
            component: SettingsPage
        });
        await modal.present();
    }

    ngOnInit() {
    }

}
