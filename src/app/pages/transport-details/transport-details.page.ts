import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../../providers/event/event.service';
import { AuthService } from '../../providers/auth/auth.service';
import { LoadingController, ToastController, AlertController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import * as Config from '../../../utils/config';
import * as firebase from 'firebase';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { UserProfilePage } from '../user-profile/user-profile.page';


let mapEvent: any;
declare var google;

@Component({
    selector: 'app-transport-details',
    templateUrl: './transport-details.page.html',
    styleUrls: ['./transport-details.page.scss'],
})
export class TransportDetailsPage implements OnInit {

    @ViewChild('mapTransportDetail') mapElement: ElementRef;

    markers: any[];
    city: any;
    street: any;
    streetNumber: any;
    zipCode: any;
    eventId: any;
    transportId: any;
    transport: any;
    passengers: any;
    isPassenger: any;
    isOwner: any;
    userStatus: any;
    transportOwner: any;
    country: any;
    uid: any;
    pos: any;
    zoom: any;
    isService: boolean = false;
    // Options to format date
    options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

    constructor(
        private route: ActivatedRoute,
        private eventService: EventService,
        private loadingController: LoadingController,
        private translate: TranslateService,
        private authService: AuthService,
        private _location: Location,
        private router: Router,
        private alertController: AlertController,
        public toastController: ToastController,
        private launchNavigator: LaunchNavigator,
        private modalCtrl: ModalController
    ) { }

    ngOnInit() {
    }

    async ionViewWillEnter() {

        this.isOwner = false;
        this.isPassenger = false;

        const loading = await this.loadingController.create({
            message: this.translate.instant('common.LOADING')
        });
        await loading.present();

        this.eventId = await this.route.snapshot.paramMap.get('eventId');
        this.transportId = await this.route.snapshot.paramMap.get('transportId');
        this.userStatus = await this.route.snapshot.paramMap.get('userStatus');
        this.uid = await this.authService.getCurrentUserId();

        await this.getOneTransport().then(async () => {
            await loading.dismiss();
        }).then(async () => {
          this.initMap();
        })
    }

    async initMap(lat?: number, lng?: any) {

        mapEvent = new google.maps.Map(this.mapElement.nativeElement, {
            center: { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG },
            zoom: this.zoom,
            streetViewControl: false,
            zoomControl: false,
            mapTypeControl: false,
            draggable: true,
            styles: [
                {
                    featureType: 'poi',
                    stylers: [{ visibility: 'off' }]
                },
                {
                    featureType: 'transit',
                    stylers: [{ visibility: 'off' }]
                }
            ]
        });

        var iconDefault = '../../../assets/images/icon-upviewsly.svg';

        var marker = new google.maps.Marker({
          map: mapEvent,
          icon: iconDefault,
          position: { lat: this.transport.latitude, lng: this.transport.longitude},
          zIndex: 9,
        });

        let newCenter = { lat: this.transport.latitude, lng: this.transport.longitude }
          mapEvent.setCenter(newCenter);
          mapEvent.setZoom(Config.DEFAULT_ZOOM_HIGH);
    }

    clearMarkers() {
        if (typeof this.markers !== 'undefined') {
            for (let i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
        }
    }

    async getOneTransport() {

        return new Promise<any>(async (resolve) => {
            let transport;
            await this.eventService.getOneTransport(this.eventId, this.transportId).subscribe(async data => {

                let newDate = new Date(data.payload.data()['transportDate']);

                if (data.payload.data()['fromAddress'].address == null) {
                    this.country = data.payload.data()['fromAddress'].country;
                }

                let transportType;

                switch (data.payload.data()['type']) {
                    case 'BUS':
                        transportType = this.translate.instant("add-transport.BUS");
                        break;
                    case 'CAR':
                        transportType = this.translate.instant("add-transport.CAR");
                        break;
                    case 'TAXI':
                        transportType = this.translate.instant("add-transport.TAXI");
                        break;
                    case 'TRAIN':
                        transportType = this.translate.instant("add-transport.TRAIN");
                        break;
                    case 'SUBWAY':
                        transportType = this.translate.instant("add-transport.SUBWAY");
                        break;
                    case 'WALK':
                        transportType = this.translate.instant('add-transport.WALK');
                        break;
                    case 'MOTO':
                        transportType = this.translate.instant('add-transport.MOTO');
                        break;
                }

                transport = {
                    address: data.payload.data()['fromAddress'].address,
                    city: data.payload.data()['fromAddress'].city,
                    zipCode: data.payload.data()['fromAddress'].zipCode,
                    country: data.payload.data()['fromAddress'].country,
                    latitude: data.payload.data()['fromAddress'].latitude,
                    longitude: data.payload.data()['fromAddress'].longitude,
                    passengers: data.payload.data()['passengers'],
                    seats: data.payload.data()['seats'],
                    _date: newDate,
                    date: new Date(newDate).toLocaleDateString('fr-FR', this.options),
                    displayType: transportType,
                    type: data.payload.data()['type'],
                    owner: data.payload.data()['userId']
                }
                this.transport = transport;
                this.passengers = transport.passengers;

                // Getting owner infos
                this.authService.getUserInfo(transport.owner).then(res => {
                    if (transport.owner == this.uid) {
                        this.isOwner = true;
                    }

                    let transportOwner = {
                        name: res.displayName,
                        profilePic: res.imageURL,
                        uid: res.firebaseId
                    }

                    this.transportOwner = transportOwner;
                })

                //checking participation
                await this.passengers.forEach(async element => {
                    if (element.uid == this.uid) {
                        this.isPassenger = true;
                    }
                });
            });
            resolve(transport)
        });
    }

    async showUser(user) {
        if (typeof user.uid !== 'undefined') {
            if(user.uid !== this.authService.getCurrentUserId()) {
                let modal = await this.modalCtrl.create({
                    component: UserProfilePage,
                    componentProps: {
                        userId: user.uid
                    }
                });
                modal.present();
            } else {
                this.router.navigate(['tabs/profile']);
            }
        }
    }

    async removeTransport() {
        const alert = await this.alertController.create({
            message: this.translate.instant('transports.CONFIRM_REMOVE'),
            buttons: [
                {
                    text: this.translate.instant('common.CANCEL'),
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: this.translate.instant('common.DELETE'),
                    handler: async () => {
                        await this.eventService.removeTransport(this.eventId, this.transportId).then(async () => {
                            await this.presentToast("success_remove");
                            await this.back();
                        });
                        console.log('Confirm Okay');
                    }
                }
            ]
        });
        await alert.present();
    }

    async subscribeTransport() {

        let passengers = this.passengers;
        let user;

        await this.authService.getUserInfo(this.uid).then(async res => {
            user = {
                uid: this.uid,
                name: res.displayName,
                profilePic: res.imageURL
            }
            await passengers.push(user)
            this.transport.passengers = passengers;
            this.passengers = passengers;
        }).then(async () => {
            await this.eventService.updateTransportSubscription(this.eventId, this.transportId, this.transport).then(async () => {
                if (this.transport.seats != -1) {
                    await this.transport.seats--;
                }
                await this.eventService.updateTransport(this.eventId, this.transportId, this.transport).then(async () => {
                    await this.presentToast("success_subscribe");
                });
            });
        });
    }

    async unsubscribeTransport() {

        for (let i = 0; i < this.passengers.length; i++) {
            if (this.passengers[i].uid === this.authService.getCurrentUserId()) {
                this.passengers.splice(i, 1);
                this.transport.passengers = this.passengers;
            }
        }
        if (this.transport.seats != -1) {
            await this.transport.seats++;
        }
        await this.eventService.updateTransport(this.eventId, this.transportId, this.transport).then(async () => {
            await this.presentToast("success_unsubscribe");
        }).then(async () => {
            await this.ionViewWillEnter();
        })
    }

    async presentToast(message) {

        let msg;

        switch (message) {
            case 'success_subscribe':
                msg = this.translate.instant("transports.SUBSCRIPTION_SUCCESS");
                break;
            case 'success_unsubscribe':
                msg = this.translate.instant("transports.UNSUBSCRIPTION_SUCCESS");
                break;
            case 'success_remove':
                msg = this.translate.instant("transports.REMOVE_SUCCESS");
                break;
        }

        const toast = await this.toastController.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    back() {
        this._location.back();
    }

    launchNavigatorApp(){
        let options: LaunchNavigatorOptions = {
          appSelection: {
              list: [this.launchNavigator.APP.GOOGLE_MAPS, this.launchNavigator.APP.WAZE, this.launchNavigator.APP.APPLE_MAPS, this.launchNavigator.APP.CITYMAPPER ],
              dialogHeaderText: this.translate.instant('common.NAVIGATOR_PROMPT_HEADER'),
              cancelButtonText : this.translate.instant('common.CANCEL'),
              rememberChoice: {
                  prompt: {
                      headerText: this.translate.instant('common.NAVIGATOR_PROMPT_REMEMBER_HEADER'),
                      bodyText: this.translate.instant('common.NAVIGATOR_PROMPT_REMEMBER_BODY'),
                      yesButtonText: this.translate.instant('common.YES'),
                      noButtonText: this.translate.instant('common.NO')
                  }
              }
          }

        }
        this.launchNavigator.navigate([this.transport.latitude, this.transport.longitude], options)
          .then(
            success => console.log('Launched navigator'),
            error => console.log('Error launching navigator', error)
          );
    }

}
