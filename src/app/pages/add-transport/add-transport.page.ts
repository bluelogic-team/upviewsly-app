import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { EventService } from "../../providers/event/event.service"
import { AuthService } from "../../providers/auth/auth.service";
import { ToastController, AngularDelegate } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService } from 'src/app/providers/notifications/notifications.service';
import { FcmService } from 'src/app/providers/fcm/fcm.service';
import * as Config from '../../../utils/config';
import * as firebase from 'firebase';

let mapEvent: any;
declare var google;
declare var mapIcons;

@Component({
    selector: 'app-add-transport',
    templateUrl: './add-transport.page.html',
    styleUrls: ['./add-transport.page.scss'],
})
export class AddTransportPage implements OnInit {
    autoComplete: any;
    eventId: any;
    event: any;
    uid: any;
    address: any;
    street: any;
    streetNumber: any;
    zipCode: any;
    city: any;
    country: any;
    latitude: any;
    longitude: any;
    transportPlaces: any;
    transportType: any;
    addressDeparture: any;
    eventSubscriber: any;
    private owner: any;
    setter: boolean = false;
    markers: any[];
    markersParticipants = [];
    selectedPlace: any;
    placeId: any;
    eventPlace: any;
    isService: boolean = false;
    pos: any;
    zoom: any;
    homePlaceId: any;
    currentPlaceId: any;
    isSending: boolean = false;

    transportDate: any;
    eventDateMax : any;
    today: any;
    // Options to format date
    options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

    @ViewChild('mapEventTransport') mapElement: ElementRef;

    constructor(
        private route: ActivatedRoute,
        private eventService: EventService,
        private authService: AuthService,
        private translate: TranslateService,
        private _location: Location,
        private toastController: ToastController,
        private notificationService: NotificationsService,
        private fcmService: FcmService
    ) {
    }

    ngOnInit() {
    }

    ionViewWillLeave() {
        this.eventSubscriber && this.eventSubscriber.unsubscribe();
        this.setter = false;
        this.markersParticipants = [];
        this.markers = [];
    }

    async ionViewWillEnter() {
        this.transportPlaces = -1;
        this.eventId = await this.route.snapshot.paramMap.get('eventId');
        this.uid = this.authService.getCurrentUserId();
        let timestamp = this.route.snapshot.paramMap.get('date');
        let eventDate = new Date(parseInt(this.route.snapshot.paramMap.get('date')));
        let eventDateMax = new Date(parseInt(this.route.snapshot.paramMap.get('date')));
        let today = new Date();

        this.transportDate = new Date(eventDate.getTime() - eventDate.getTimezoneOffset()).toISOString();
        this.today = this.convertToIsoString(today)
        eventDateMax.setHours(eventDate.getHours() + 1);
        this.eventDateMax = this.convertToIsoString(eventDateMax)

        if(this.today > this.transportDate){
            this.transportDate = this.today;
        }

        this.initAutocomplete();
        this.authService.getUserCoords(firebase.auth().currentUser.uid).then(res => {
            if (res) {
                if (res.latitude != '' && res.longitude != '') {
                    this.pos = { lat: res.latitude, lng: res.longitude }
                    this.zoom = Config.DEFAULT_ZOOM_HIGH;
                    this.homePlaceId = res.address_placeId;
                } else {
                    this.pos = { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG }
                    this.zoom = Config.DEFAULT_ZOOM_LOW;
                }
            } else {
                this.pos = { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG }
                this.zoom = Config.DEFAULT_ZOOM_LOW;
            }
        }).then(async () => {
            await this.initMap();
            var image = {
                path: mapIcons.shapes.MAP_PIN,
                fillColor: '#F400FE',
                fillOpacity: 1,
                strokeColor: '',
                strokeWeight: 0,
                scale: 1
            };

            var marker = new mapIcons.Marker({
                map: mapEvent,
                position: this.pos,
                zIndex: 9,
                icon: image,
                map_icon_label: '<span class="map-icon map-icon-insurance-agency"></span>'
            });
        });
    }

    convertToIsoString(date){
            var tzo = date.getTimezoneOffset(),
                dif = tzo >= 0 ? '+' : '-',
                pad = function(num) {
                    var norm = Math.floor(Math.abs(num));
                    return (norm < 10 ? '0' : '') + norm;
                };
            return date.getFullYear() +
                '-' + pad(date.getMonth() + 1) +
                '-' + pad(date.getDate()) +
                'T' + pad(date.getHours()) +
                ':' + pad(date.getMinutes()) +
                ':' + pad(date.getSeconds()) +
                dif + pad(tzo / 60) +
                ':' + pad(tzo % 60);
    }

    async initMap(lat?: number, lng?: any) {

        mapEvent = new google.maps.Map(this.mapElement.nativeElement, {
            center: { lat: lat ? lat : this.pos.lat, lng: lng ? lng : this.pos.lng },
            zoom: this.zoom,
            streetViewControl: false,
            zoomControl: false,
            mapTypeControl: false,
            draggable: true,
            styles: [
                {
                    featureType: 'poi',
                    stylers: [{ visibility: 'off' }]
                },
                {
                    featureType: 'transit',
                    stylers: [{ visibility: 'off' }]
                }
            ]
        });

        var icon = '<span class="map-icon map-icon-night-club"></span>';
        var iconDefault = '../../../assets/images/icon-upviewsly.svg';

        var image = {
            path: mapIcons.shapes.MAP_PIN,
            fillColor: '#820080',
            fillOpacity: 1,
            strokeColor: '',
            strokeWeight: 0,
            scale: 1
        };


        if (this.placeId) {
            let that = this;
            var service = new google.maps.places.PlacesService(mapEvent);
            if (that.placeId) {
                that.currentPlaceId = that.placeId
            } else {
                that.currentPlaceId = that.homePlaceId
            }
            return await service.getDetails({
                placeId: that.currentPlaceId
            }, async function(result) {

                that.selectedPlace = result
                that.setAddress(result);
                
                if(result.types.includes("restaurant", "bar", "cafe")){
                    var marker = new mapIcons.Marker({
                        map: mapEvent,
                        icon: image,
                        position: result.geometry.location,
                        map_icon_label: icon,
                        zIndex: 9,
                        place: {
                            placeId: that.currentPlaceId,
                            location: result.geometry.location
                        }
                    });
                } else {
                    var marker = new google.maps.Marker({
                        map: mapEvent,
                        icon: iconDefault,
                        position: result.geometry.location,
                        zIndex: 9,
                    });
                }

                let newCenter = { lat: result.geometry.location.lat(), lng: result.geometry.location.lng() }
                mapEvent.setCenter(newCenter);
                mapEvent.setZoom(Config.DEFAULT_ZOOM_HIGH);
            });
        }
    }

    clearMarkers() {
        if (typeof this.markers !== 'undefined') {
            for (let i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
        }
    }

    setLocationFromProfile() {
        this.clearMarkers();
        this.markers = [];
        return this.authService.getUserCoords(this.authService.getCurrentUserId()).then(async res => {
            if (res.latitude != '' && res.longitude != '') {
                this.addressDeparture = res.address;
                this.address = res.address;
                this.zipCode = res.zipCode;
                this.city = res.city;
                this.country = res.country;
                this.latitude = res.latitude;
                this.longitude = res.longitude;
                var service = new google.maps.places.PlacesService(mapEvent);
                let that = this;
                return await service.getDetails({
                    placeId: await res.placeId
                }, async function(result) {
                    that.selectedPlace = result
                    that.placeId = result.place_id;
                    that.setAddress(result);
                    let newCenter = { lat: result.geometry.location.lat(), lng: result.geometry.location.lng() }
                    mapEvent.setCenter(newCenter);
                    mapEvent.setZoom(Config.DEFAULT_ZOOM_HIGH);
                })
            }

        })
    }

    setAddress(place) {

        let isEstablishment = false;
        this.eventPlace = place.name;
        this.streetNumber = '';
        this.street = '';
        this.city = '';
        if (place.types.indexOf("establishment") != -1) {
            isEstablishment = true;
            this.eventPlace = place.name
        }

        if (place.address_components) {
            for (let i = 0; i < place.address_components.length; i++) {
                let component = place.address_components[i];
                let addressType = component.types[0];

                switch (addressType) {
                    case 'street_number':
                        this.streetNumber = component.long_name;
                        break;
                    case 'route':
                        this.street = component.short_name;
                        break;
                    case 'locality':
                        this.city = component.long_name;
                        break;
                    case 'postal_code':
                        this.zipCode = component.long_name;
                        break;
                    case 'country':
                        this.country = component.long_name;
                        break;
                }
            }
            this.latitude = place.geometry.location.lat();
            this.longitude = place.geometry.location.lng();
        }

        if (!this.streetNumber) { this.streetNumber = '' }
        if (!this.street) { this.street = '' }
        if (!this.city) { this.city = '' }
        if (!this.zipCode) { this.zipCode = '' }
        if (!this.country) { this.country = '' }

        if (this.streetNumber != '' && this.street != '' && this.city != '') {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.streetNumber + ' ' + this.street + ', ' + this.city;
            } else {
                this.eventPlace = this.streetNumber + ' ' + this.street + ', ' + this.city;
            }
        }
        else if (this.street != '' && this.city != '') {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.street + ', ' + this.city
            } else {
                this.eventPlace = this.street + ', ' + this.city
            }
        }
        else if (this.city != '') {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.city;
            } else {
                this.eventPlace = this.city;
            }
        }
        else {
            this.eventPlace = '';
        }
    }


    getIcon(place) {
        var iconBase = '../../../assets/images/icon-upviewsly.svg';

        if (typeof place !== 'undefined') {
            if (typeof place.types !== 'undefined') {
                var types = place.types;
                var typeRestaurant = (types.indexOf("restaurant") > -1);
                var typeBar = (types.indexOf("bar") > -1);
                var typeCafe = (types.indexOf("cafe") > -1);

                if (typeBar) {
                    iconBase = '<span class="map-icon map-icon-night-club"></span>';
                    this.isService = true;
                }
                else if (typeRestaurant) {
                    iconBase = '<span class="map-icon map-icon-restaurant"></span>';
                    this.isService = true;
                }
                else if (typeCafe) {
                    iconBase = '<span class="map-icon map-icon-cafe"></span>';
                    this.isService = true;
                } else {
                    this.isService = false;
                }
                return iconBase;
            }
        }
        return iconBase;
    }

    /**
     * 
     * @param place 
     * @param participants only for displaying in map
     */
    async createMarker(place, participants?: boolean) {
        let that = this;
        var icon = this.getIcon(place);
        var btn = '';

        var image = {
            path: mapIcons.shapes.MAP_PIN,
            fillColor: '#820080',
            fillOpacity: 1,
            strokeColor: '',
            strokeWeight: 0,
            scale: 1
        };

        if (!participants) {
            let placeLoc = place.geometry.location;
            if(this.isService && !participants){
                var marker = new mapIcons.Marker({
                    map: mapEvent,
                    position: placeLoc,
                    zIndex: 9,
                    icon: image,
                    map_icon_label: icon
                });
            } else {
                var marker = new google.maps.Marker({
                    map: mapEvent,
                    position: placeLoc,
                    zIndex: 9,
                    icon: icon,
                });
            }
            this.markers.push(marker);
        }
    }

    async addTransport() {
        this.isSending = true;
        if (this.transportType === 'MOTO') {
            this.transportPlaces = 1;
        }
        if (this.transportType == "BUS" || this.transportType == "SUBWAY" || this.transportType == "TRAIN" || this.transportType == "WALK") {
            this.transportPlaces = -1;
        }
        if (this.transportPlaces == -1 && this.transportType == "CAR" ||
            this.transportPlaces == -1 && this.transportType == "TAXI" ||
            this.transportPlaces == -1 && this.transportType == "MOTO"
        ) {
            await this.presentToast("missing_seats");
        } else {

            let user;
            let passengers = [];

            if (this.address != null) {

                await this.authService.getUserInfo(this.uid).then(async res => {
                    user = {
                        uid: this.uid,
                        name: this.owner = res.displayName,
                        profilePic: res.imageURL
                    }
                    // Don't append owner on passenger
                    // passengers.push(user)
                });

                let transport = {
                    userId: this.uid,
                    type: this.transportType,
                    fromAddress: {
                        address: this.address,
                        zipCode: this.zipCode,
                        city: this.city,
                        country: this.country,
                        latitude: this.latitude,
                        longitude: this.longitude
                    },
                    transportDate: new Date(this.transportDate).getTime(),
                    seats: this.transportPlaces,
                    passengers: passengers
                }
                await this.eventService.addTransport(this.eventId, transport).then(async () => {
                    await this.createNotification(this.eventId);
                    await this.presentToast("success_add");
                }).then(() => {
                    this.isSending = false;
                    this.back();
                });
            }
            else {
                await this.presentToast("invalid_address");
                this.isSending = false;
            }
        }
    }

    async createNotification(eventId) {

        this.eventSubscriber = this.eventService.getOneEvent(eventId).subscribe(async data => {
            let event: any = await data.payload.data();
            let newDate = new Date(event.date)
            let displayHour = newDate.getHours() + ':' + (newDate.getMinutes() < 10 ? '0' + newDate.getMinutes() : newDate.getMinutes());
            let displayDate = new Date(newDate).toLocaleDateString('fr-FR', this.options);
            let transportName;

            switch (this.transportType) {
                case 'CAR':
                    transportName = this.translate.instant("add-transport.CAR");
                    break;
                case 'SUBWAY':
                    transportName = this.translate.instant("add-transport.SUBWAY");
                    break;
                case 'BUS':
                    transportName = this.translate.instant("add-transport.BUS");
                    break;
                case 'TRAIN':
                    transportName = this.translate.instant("add-transport.TRAIN");
                    break;
                case 'TAXI':
                    transportName = this.translate.instant("add-transport.TAXI");
                    break;
                case 'WALK':
                    transportName = this.translate.instant('add-transport.WALK');
                    break;
                case 'MOTO':
                    transportName = this.translate.instant('add-transport.MOTO');
                    break;
            }

            let i = 0;
            let arrayDevice = new Array();

            this.authService.getUserInfo(this.authService.getCurrentUserId()).then(
                owner => {
                    return owner;
                }
            ).then(owner => {
                event.participants.forEach(element => {
                    if (element.uid != this.uid) {

                        let recipient = element.uid;
                        let title = this.translate.instant("notifications.NEW_TRANSPORT");
                        let body = this.translate.instant("notifications.TRANSPORT_BY") + transportName + this.translate.instant("notifications.TRANSPORT_ADD_BY") + owner.displayName + this.translate.instant("notifications.TRANSPORT_EVENT") + event.title + this.translate.instant("notifications.FROM_DATE") + displayDate + this.translate.instant("notifications.FROM_HOUR") + displayHour;
                        let link = '/tabs/event-details/';
                        let eventId = this.eventId;
                        this.notificationService.createNewNotification(recipient, title, body, link, eventId, false, null);

                        this.fcmService.getDeviceInfo(recipient).then(
                            (deviceToken: any) => {
                                this.fcmService.sendPushNotification(title,
                                        body,
                                        deviceToken.data()['deviceToken'],
                                        recipient,
                                        { payload: { type: 'transport', link: link, linkData: eventId } }
                                    )
                            }
                        )
                    }
                });
            })
        });
    }

    onSelect() {
        if (this.transportType == "SUBWAY" || this.transportType == "BUS" || this.transportType == "TRAIN" || this.transportType == "WALK" || this.transportType == "MOTO") {
            document.getElementById('nbPlaces').hidden = true;
            this.transportPlaces = -1;
        }
        else {
            document.getElementById('nbPlaces').hidden = false;
            this.transportPlaces = -1
        }
    }

    async presentToast(message) {

        let msg;

        switch (message) {
            case 'success_add':
                msg = this.translate.instant("transports.ADD_SUCCESS")
                break;
            case 'missing_seats':
                msg = this.translate.instant("transports.ADD_MISS_SEATS")
                break;
            case 'invalid_address':
                msg = this.translate.instant('add-event.INVALID_ADDRESS');
                break;
        }

        const toast = await this.toastController.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    back() {
        this._location.back();
    }

    async initAutocomplete() {

        if (document.getElementById('input-address-transport')) {

            var options = {
                types: ['geocode', 'establishment'],
                componentRestrictions: { country: "fr" }
            };

            var input = document.getElementById('input-address-transport').getElementsByTagName('input')[0];
            this.autoComplete = new google.maps.places.Autocomplete(await input, options);

            this.autoComplete.addListener('place_changed', () => {
                this.clearMarkers();
                this.markers = [];
                let isEstablishment = false;
                this.address = '';

                if (this.autoComplete.getPlace().types.indexOf("establishment") != -1) {
                    isEstablishment = true;
                    this.address = this.autoComplete.getPlace().name
                }

                this.placeId = this.autoComplete.getPlace().place_id;
                this.selectedPlace = this.autoComplete.getPlace();
                this.eventPlace = '';

                this.selectedPlace = this.autoComplete.getPlace()
                this.setAddress(this.autoComplete.getPlace());
                let newCenter = { lat: this.autoComplete.getPlace().geometry.location.lat(), lng: this.autoComplete.getPlace().geometry.location.lng() }
                mapEvent.setCenter(newCenter);
                mapEvent.setZoom(Config.DEFAULT_ZOOM_HIGH);
                this.createMarker(this.autoComplete.getPlace());

                for (let i = 0; i < this.autoComplete.getPlace().address_components.length; i++) {
                    let component = this.autoComplete.getPlace().address_components[i];
                    let addressType = component.types[0];

                    switch (addressType) {
                        case 'street_number':
                            this.streetNumber = component.long_name;
                            break;
                        case 'route':
                            this.street = component.short_name;
                            break;
                        case 'locality':
                            this.city = component.long_name;
                            break;
                        case 'postal_code':
                            this.zipCode = component.long_name;
                            break;
                        case 'country':
                            this.country = component.long_name;
                            break;
                    }
                }

                if (!this.streetNumber) { this.streetNumber = null }
                if (!this.street) { this.street = null }
                if (!this.city) { this.city = null }
                if (!this.zipCode) { this.zipCode = null }
                if (!this.country) { this.country = null }

                if (this.streetNumber != null && this.street != null && this.city != null) {
                    if (isEstablishment) {
                        this.address += ', ' + this.streetNumber + ' ' + this.street + ', ' + this.city;
                    } else {
                        this.address = this.streetNumber + ' ' + this.street + ', ' + this.city;
                    }
                }
                else if (this.street != null && this.city != null) {
                    if (isEstablishment) {
                        this.address += ', ' + this.street + ', ' + this.city
                    } else {
                        this.address = this.street + ', ' + this.city
                    }
                }
                else if (this.city != null) {
                    if (isEstablishment) {
                        this.address += ', ' + this.city;
                    } else {
                        this.address = this.city;
                    }
                }
                else {
                    this.address = null;
                }
            });
        }
    }

}
