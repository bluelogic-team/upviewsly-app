import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTransportPage } from './add-transport.page';

describe('AddTransportPage', () => {
  let component: AddTransportPage;
  let fixture: ComponentFixture<AddTransportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTransportPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTransportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
