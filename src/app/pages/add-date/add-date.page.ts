import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-add-date',
  templateUrl: './add-date.page.html',
  styleUrls: ['./add-date.page.scss'],
})
export class AddDatePage implements OnInit {

  date: any;
  hour: any;
  today: string;

  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams,
  ) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    let tempDate = new Date(this.navParams.get('date'));
    let todayDate = new Date();
    //todayDate.setHours(todayDate.getHours() + 1);
    this.today = this.convertToIsoString(todayDate);
    this.date = new Date(tempDate.getTime() - tempDate.getTimezoneOffset()).toISOString();
    this.hour = tempDate.getHours() + ':' + (tempDate.getMinutes() < 10 ? '0' + tempDate.getMinutes() : tempDate.getMinutes());
  }

  convertToIsoString(date){
            var tzo = date.getTimezoneOffset(),
                dif = tzo >= 0 ? '+' : '-',
                pad = function(num) {
                    var norm = Math.floor(Math.abs(num));
                    return (norm < 10 ? '0' : '') + norm;
                };
            return date.getFullYear() +
                '-' + pad(date.getMonth() + 1) +
                '-' + pad(date.getDate()) +
                'T' + pad(date.getHours()) +
                ':' + pad(date.getMinutes()) +
                ':' + pad(date.getSeconds()) +
                dif + pad(tzo / 60) +
                ':' + pad(tzo % 60);
    }

  validateModal() {

    let hours = this.hour.split(':');
    var newHour = new Date();
    newHour.setHours(hours[0], hours[1]);

    let newDate = new Date(this.date).getTime()

    let finalDate = {
      hour: newDate,
      date: newDate
    }
    
    this.modalCtrl.dismiss(finalDate);
  }

  cancel() {
    this.modalCtrl.dismiss();
  }

}
