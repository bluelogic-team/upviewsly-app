import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../../providers/chat/chat.service';
import { AuthService } from '../../providers/auth/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { EventService } from 'src/app/providers/event/event.service';
import { ModalController } from '@ionic/angular';
import { AddParticipantsPage } from '../add-participants/add-participants.page';
import { NotificationsService } from 'src/app/providers/notifications/notifications.service';

@Component({
    selector: 'app-chat-list',
    templateUrl: './chat-list.page.html',
    styleUrls: ['./chat-list.page.scss'],
})
export class ChatListPage implements OnInit {

    uid: any;
    chats: any[];
    chatTitle: any;
    _date = Date.now();
    private _chats: any;
    _chatsContent = new Array();
    _chatsLastMessage: any;

    constructor(
        private router: Router,
        private chatService: ChatService,
        private translate: TranslateService,
        private eventService: EventService,
        public authService: AuthService,
        private modalCtrl: ModalController,
        public notifService: NotificationsService,
        private navController: Router
    ) { }

    ngOnInit() {
        this.authService.chatListObservable = this.chatService.getChatsList().subscribe(async (res: any) => {
            console.log('observable');
            this.chats = await res;
            res.forEach(chat => {
                this.chatService.lastMessage(chat.id).subscribe(
                    (message) => {
                        if (typeof message[0] !== 'undefined') {
                            this.authService.getUserInfo(message[0].payload.doc.data()['uid']).then(
                                user => {
                                    return user.firstName;
                                }
                            ).then(
                                firstName => {
                                    this._chatsLastMessage = this.chatService.getChatLastRead(chat.id).subscribe(async lastRead => {
                                        if (typeof lastRead.payload.data() !== 'undefined') {
                                            if (typeof lastRead.payload.data()['timestamp'] !== 'undefined') {
                                                let data = Object.assign(chat,
                                                    { "lastMessage": message[0].payload.doc.data() },
                                                    { "firstname": firstName },
                                                    { "timestamp": lastRead.payload.data()['timestamp'] }
                                                );

                                                this.chats = await res;

                                                    this.chats.sort((a, b) => {
                                                        if (typeof a.lastMessage !== 'undefined' && typeof b.lastMessage !== 'undefined') {
                                                            if (typeof a.lastMessage.createdAt !== 'undefined' && typeof b.lastMessage.createdAt !== 'undefined') {
                                                                return b.lastMessage.createdAt - a.lastMessage.createdAt
                                                            }
                                                        } else if(!a.lastMessage && b.lastMessage) {
                                                            if(b.lastMessage.createdAt > a.createdAt){
                                                                return 1;
                                                            } else {
                                                                return -1;
                                                            }
                                                        } else if( a.lastMessage && !b.lastMessage) {
                                                            if(a.lastMessage.createdAt > b.createdAt){
                                                                return -1;
                                                            } else {
                                                                return 1;
                                                            }
                                                        } else {
                                                            if(b.createdAt > a.createdAt){
                                                                return 1;
                                                            } else {
                                                                return -1;
                                                            }
                                                        }
                                                    })
                                                return data;
                                            }
                                        }
                                    })
                                }
                            )
                        }
                    }
                )
            })
            this.authService.cache_chatslist = this.chats;
        });
    }

    ionViewWillEnter() {
        if (typeof this.authService.chatListObservable !== 'undefined') {
            if (this.authService.chatListObservable.closed) {
                this.chats = [];
                this.authService.chatListObservable = this.chatService.getChatsList().subscribe(async (res: any) => {
                    this.chats = await res;
                    res.forEach(chat => {
                        this.chatService.lastMessage(chat.id).subscribe(
                            (message) => {
                                if (typeof message[0] !== 'undefined') {
                                    this.authService.getUserInfo(message[0].payload.doc.data()['uid']).then(
                                        user => {
                                            return user.firstName;
                                        }
                                    ).then(
                                        firstName => {
                                            this._chatsLastMessage = this.chatService.getChatLastRead(chat.id).subscribe(async lastRead => {
                                                if (typeof lastRead.payload.data() !== 'undefined') {
                                                    if (typeof lastRead.payload.data()['timestamp'] !== 'undefined') {
                                                        let data = Object.assign(chat,
                                                            { "lastMessage": message[0].payload.doc.data() },
                                                            { "firstname": firstName },
                                                            { "timestamp": lastRead.payload.data()['timestamp'] }
                                                        );
        
                                                        this.chats = await res;
        
                                                        this.chats.sort((a, b) => {
                                                            if (typeof a.lastMessage !== 'undefined' && typeof b.lastMessage !== 'undefined') {
                                                                if (typeof a.lastMessage.createdAt !== 'undefined' && typeof b.lastMessage.createdAt !== 'undefined') {
                                                                    return b.lastMessage.createdAt - a.lastMessage.createdAt
                                                                }
                                                            }
                                                        })
                                                        return data;
                                                    }
                                                }
                                            })
                                        }
                                    )
                                }
                            }
                        )
                    })
                    this.authService.cache_chatslist = this.chats;
                });
            }
        }
    }

    decrementUnreadMessages(itemId) {
        return this.chatService.decrementUnreadMessages(itemId);
    }

    isToday(date) {
        let _date = new Date(Date.now());
        let _itemDate = new Date(date)
        if (_date.getDate() == _itemDate.getDate() && _date.getMonth() == _itemDate.getMonth() && _date.getFullYear() == _itemDate.getFullYear()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Unsubscribe _chats to getting no ambigous errors
     */
    ionViewWillLeave() {
        // this._chatsLastMessage && this._chatsLastMessage.unsubscribe();
        // this._chats && this._chats.unsubscribe();
    }

    async createChat(chats) {

        let chatMembers = [];
        let groupId;
        this.chatTitle = '';

        let modalEdit = await this.modalCtrl.create({
            component: AddParticipantsPage,
            componentProps: {
                type: 'chat',
                chats: chats
            }
        });
        await modalEdit.present();
        await modalEdit.onWillDismiss().then(async members => {

            if (members.data) {

                if (members.data.groupId) { // Create chat from a group

                    let alreadyExists = false;
                    let chatId;

                    // check if group conversation already exists
                    this.chats.forEach(async chat => {
                        if (typeof chat.groupId !== 'undefined' && chat.groupId == members.data.groupId) {
                            alreadyExists = true;
                            chatId = chat.id;
                        }
                    });

                    if (alreadyExists) {
                        this.router.navigate(['chat/', chatId])
                    }
                    else {
                        await members.data.members.forEach(element => {
                            chatMembers.push(element.firebaseId);
                        });
                        this.chatTitle = members.data.groupName;
                        groupId = members.data.groupId;

                        await this.chatService.createChat(this.chatTitle, null, null, chatMembers, null, groupId);
                    }
                }
                else { // Create chat from friend(s)

                    this.chatTitle = '';
                    groupId = null;
                    let alreadyExists = false;
                    let chatId;

                    chatMembers.push(this.authService.getCurrentUserId());

                    await members.data.forEach(async element => {
                        chatMembers.push(element)
                    });

                    // check if conversation already exists between 2 users
                    if (typeof this.chats !== 'undefined') {
                        this.chats.forEach(async chat => {
                            if (chat.participants.length == 2 && chatMembers.length == 2) {
                                if (chat.participants.indexOf(chatMembers[0]) != -1 && chat.participants.indexOf(chatMembers[1]) != -1) {
                                    if (chat.eventId) {
                                        alreadyExists = false;
                                        chatId = null;
                                    } else if (chat.groupId) {
                                        alreadyExists = false;
                                        chatId = null;
                                    } else {
                                        alreadyExists = true;
                                        chatId = chat.id;
                                    }
                                }
                            }
                        });
                    }
                    if (alreadyExists && chatId) {
                        this.router.navigate(['chat/', chatId])
                    }
                    else {
                        for (let i = 0; i < chatMembers.length; i++) {
                            await this.authService.getUserInfo(chatMembers[i]).then(res => {
                                this.chatTitle += res.firstName;
                                if (chatMembers.length > 1 && i != chatMembers.length - 1) {

                                    this.chatTitle += ', ';
                                }
                            });
                        }
                        await this.chatService.createChat(this.chatTitle, null, null, chatMembers, null, null);
                    }
                }
            }
        });
    }

    async getChatName(members) {

        let names = [];
        return new Promise<any>(async (resolve) => {
            await members.forEach(async (element) => {
                await this.authService.getUserInfo(element).then(async (res) => {
                    if (res.firstName) {
                        names.push(res.firstName);
                    }
                });
            });
            resolve(names)
        });
    }

    removeChat(chat) {
        this.chatService.removeChat(chat.id);
    }

}
