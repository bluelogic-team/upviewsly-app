import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

    constructor(private modalCtrl: ModalController, private nativeSettings: OpenNativeSettings, private translate: TranslateService) { }
    
    ngOnInit() {
    }

    cancel() {
        this.modalCtrl.dismiss();
    }

    openNativeSetting() {
      this.nativeSettings.open('notification_id').then(() => { console.log('native settings opened')}).catch(() => {
        this.translate.instant('settings.ACTIVATION_ERROR');
      });
    }

}
