import { Component, OnInit } from '@angular/core';
import { Platform, NavController, ToastController, LoadingController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { AuthService } from '../../providers/auth/auth.service';
import { TranslateService } from "@ngx-translate/core";


@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {

  resetPasswordForm: FormGroup;
  email: string = "";
  errorMessage: string = '';
  successMessage: string = '';

  constructor(
    private router: Router,
    private translate: TranslateService,
    public authService: AuthService
  ) {
    this.resetPasswordForm = new FormGroup({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(
          "[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
        )
      ]))
    });      

   }

  ngOnInit() {
  }

  onSubmit(value){
    this.tryResetPassword(value.email);
  }

  tryResetPassword(emailAdress){
    this.authService.resetPassword(emailAdress)
    .then( res => {
      this.successMessage =this.translate.instant('reset-password.EMAIL_SENT'), res;
    }, err => {
      this.errorMessage = err;
    })
  }

}
