import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../providers/auth/auth.service';
import { ProfileService } from '../../providers/profile/profile.service';
import { Platform, ModalController, NavParams, LoadingController, ActionSheetController } from '@ionic/angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { TranslateService } from "@ngx-translate/core";

declare var google;

@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.page.html',
    styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

    placeId: any;
    user;
    userId: any;
    lastName: string = "";
    firstName: string = "";
    autoComplete: any;

    addressComplete: any;
    address: any;
    zipCode: any;
    city: any;
    country: any;
    latitude: any;
    longitude: any;

    displayName: any;
    imageURL: any;
    email: string;

    public base64Image;
    public imageChanged = false;

    constructor(
        public authService: AuthService,
        private profileService: ProfileService,
        private navParams: NavParams,
        private modalCtrl: ModalController,
        private db: AngularFirestore,
        private camera: Camera,
        private translate: TranslateService,
        public platform: Platform,
        public actionSheetCtrl: ActionSheetController,
        public loadingController: LoadingController,

    ) {

    }

    ngOnInit() {
    }

    async takePhoto() {

        try {
            const options: CameraOptions = {
                quality: 50,
                destinationType: this.camera.DestinationType.DATA_URL,
                encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.PICTURE
            }

            const result = await this.camera.getPicture(options);

            const image = `data:image/jpeg;base64,${result}`;
            const pictures = firebase.storage().ref('avatars/' + 'avatar_' + this.db.createId());
            pictures.putString(image, 'data_url');
        }
        catch (e) {
            return e;
        }
    }


    async ionViewDidEnter() {
        this.initAutocomplete();
    }

    ionViewWillEnter() {
        this.userId = this.navParams.get('userId');
        this.displayName = this.navParams.get('displayName');
        this.imageURL = this.navParams.get('imageURL');
        this.address = this.navParams.get('address');
        this.country = this.navParams.get('country');
        this.zipCode = this.navParams.get('zipCode');
        this.city = this.navParams.get('city');
        this.latitude = this.navParams.get('latitude');
        this.longitude = this.navParams.get('longitude');
        this.placeId = this.navParams.get('placeId');
        this.addressComplete = this.address;
        this.email = this.navParams.get('email');
    }

    cancel() {
        this.modalCtrl.dismiss(false);
    }

    async saveProfile() {
        const loading = await this.loadingController.create({
            message: this.translate.instant('common.LOADING')
        });
        await loading.present();
        if (this.latitude == undefined || this.longitude == undefined) {
            this.latitude = "";
            this.longitude = "";
        }
        if (this.imageChanged) {
            const picture = firebase.storage().ref('users/avatars/' + 'user_avatar_' + this.userId);
            await picture.putString(this.base64Image, 'data_url');
            let downloadUrl = await picture.getDownloadURL();
            this.imageURL = downloadUrl;
        }

        this.db.collection("users").doc(this.userId).update({
            displayName: this.capitalizeFirstLetter(this.firstName) + ' ' + this.capitalizeFirstLetter(this.lastName),
            firstName: this.capitalizeFirstLetter(this.firstName),
            lastName: this.capitalizeFirstLetter(this.lastName),
            address: this.address,
            zipCode: this.zipCode,
            city: this.city,
            country: this.country,
            latitude: this.latitude,
            longitude: this.longitude,
            placeId: this.placeId ? this.placeId : '',
            imageURL: this.imageURL,
            email: this.email
        }).then(success => {
            loading.dismiss();
            this.modalCtrl.dismiss(
                true
            );
        }).catch(err => {
            console.log(err);
        })
    }

    capitalizeFirstLetter(string) {
        if(typeof string !== "undefined") {
            return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
        }
    }

    async editPicture() {
        var me = this;
        if (this.platform.is('cordova')) {
            const actionSheet = await this.actionSheetCtrl.create({
                header: this.translate.instant('common.EDIT_PICTURE_HEADER'),
                buttons: [
                    {
                        text: this.translate.instant('common.EDIT_PICTURE_PHOTO'),
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.CAMERA)
                        }
                    },
                    {
                        text: this.translate.instant('common.EDIT_PICTURE_GALLERY'),
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY)
                        }
                    },
                    {
                        text: this.translate.instant('common.CANCEL'),
                        role: 'cancel',
                        handler: () => {
                        }
                    }]
            });
            await actionSheet.present();
        }
        else {
            let input = document.createElement('input');
            input.type = 'file';
            input.click();
            var file;
            var reader = new FileReader();

            input.onchange = function() {
                file = input.files[0];
                if (file) {
                    reader.readAsDataURL(file);
                }
            };
            reader.onload = function() {
                me.base64Image = reader.result;
                me.imageChanged = true;
            };
        }
    }

    takePicture(sourceType) {
        const options: CameraOptions = {
            quality: 60,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: sourceType,
            correctOrientation: true,
        }

        this.camera.getPicture(options).then((imageData) => {
            this.base64Image = "data:image/jpeg;base64," + imageData;
            this.imageChanged = true;
        }, (err) => {
            throw new err;
        });
    }

    async initAutocomplete() {

        if (document.getElementById('input-address-profile')) {

            var options = {
                types: ['geocode'],
                componentRestrictions: { country: "fr" }
            };

            var input = document.getElementById('input-address-profile').getElementsByTagName('input')[0];
            this.autoComplete = new google.maps.places.Autocomplete(input, options);

            this.autoComplete.addListener('place_changed', () => {
                this.latitude = this.autoComplete.getPlace().geometry.location.lat();
                this.longitude = this.autoComplete.getPlace().geometry.location.lng();
                this.placeId = this.autoComplete.getPlace().place_id;
                let route;
                let streetNumber;
                let countryLong
                this.city = "";
                this.zipCode = "";
                this.country = "";

                this.autoComplete.getPlace().address_components.forEach(async addressElement => {
                    if (addressElement.types.includes('street_number')) {
                        streetNumber = addressElement.short_name;
                    }
                    if (addressElement.types.includes('route')) {
                        route = addressElement.short_name;
                    }
                    if (addressElement.types.includes('locality')) {
                        this.city = addressElement.short_name;
                    }
                    if (addressElement.types.includes('country')) {
                        this.country = addressElement.short_name;
                        countryLong = addressElement.long_name;
                    }
                    if (addressElement.types.includes('postal_code')) {
                        this.zipCode = addressElement.short_name;
                    }
                });
                this.address = "";
                if(streetNumber){
                    this.address += streetNumber;
                }

                if(route && streetNumber){
                    this.address += " " + route;
                }

                if(route && !streetNumber){
                    this.address += route;
                }

                if(this.city && route){
                    this.address += ", " +this.city;
                }

                if(this.city && !route){
                    this.address += this.city;
                }

                if(!this.city && this.country){
                    this.address += countryLong;
                }
            });
        }
    }

}
