import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { AuthService } from 'src/app/providers/auth/auth.service';
import * as firebase from 'firebase';
import { NavParams, ToastController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { OnboardingFriendPage } from '../onboarding-friend/onboarding-friend.page';
import { OnboardingAddImagePage } from '../onboarding-add-image/onboarding-add-image.page';

@Component({
    selector: 'app-onboarding-username',
    templateUrl: './onboarding-username.page.html',
    styleUrls: ['./onboarding-username.page.scss'],
})
export class OnboardingUsernamePage implements OnInit {
    public pseudo: string = this.navParams.get('username');
    public name: string = this.navParams.get('name');
    pseudoSubject = new Subject<string>();
    patternValidator = "[a-zA-Z0-9]{5,15}";
    available: boolean;
    callOneTime: boolean = false;
    isSearch: boolean = false;
    constructor(private formBuilder: FormBuilder,
        private authService: AuthService, 
        private navParams: NavParams, 
        private toastCtrl: ToastController, 
        private translate: TranslateService, 
        private router: Router,
        private modalCtrl: ModalController) {
        this.group = formBuilder.group({
            username: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(15), Validators.pattern(this.patternValidator)])]
        })
    }

    public username: string = '';
    group: any;

    inputChange() {
        this.isSearch = true;
    }

    fetchPseudo(value) {
        firebase.firestore().collection('users').where('username', '==', value).get().then(async user => {
            // Available username
            if (user.empty) {
                // Set callOneTime to true once firebase return first available pseudo
                if(!this.callOneTime) {
                    this.callOneTime = true;
                    this.available = true;
                    this.isSearch = false;
                } else {
                    this.isSearch = false;
                    this.available = true;
                }
            }
            // Not available username
            else {
                user.docs.forEach(async e => {
                    if(!this.callOneTime) {
                        this.isSearch = false;
                        let reg = /\d+/g;
                        let usernameWihtoutNumber = e.data()['username'].replace(/\d+/g, '');
                        let number = await e.data()['username'];
                        let result = number.match(reg);
                        let newSearch = result !== null ? `${usernameWihtoutNumber}${parseInt(result[0]) + 1}` : value + '1';
                        // Callback to check new user
                        this.fetchPseudo(newSearch)
                        this.available = false; 
                    } else {
                        this.isSearch = false;
                        this.username = e.data()['username'].toLocaleLowerCase();
                        if(e.data()['username'] === this.pseudo) {
                            this.available = true;
                        } else {
                            this.available = false;
                        }
                    }
                })
            }
        }).catch(userErr => {
            console.log(userErr.empty)
        });
    }

    ngOnInit() {
        this.username = this.pseudo;

        this.pseudoSubject.pipe(
            debounceTime(800),
            distinctUntilChanged()).subscribe(value => {
                if (value !== ''.trim()) {
                    this.fetchPseudo(value)
                }
            })
    }

    async forward() {
        if(this.group.valid && this.available) {
            firebase.firestore().collection('users').doc(this.authService.getCurrentUserId()).update({ "username": this.username }).then(async () => {
                this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                    this.modalCtrl.dismiss();
                    if(user.imageURL){
                        let modal = await this.modalCtrl.create({
                            component: OnboardingFriendPage,
                            backdropDismiss: false
                        });
                        await modal.present();
                    } else {
                        let modal = await this.modalCtrl.create({
                            component: OnboardingAddImagePage,
                            backdropDismiss: false
                        });
                        await modal.present();
                    }
                }).catch(err => {
                    console.log(err)
                });
            });
        } else {
            let toast = await this.toastCtrl.create({ 
                message: this.translate.instant('onboarding.INVALID_USERNAME'),
                duration: 3000
            });
            await toast.present();
        }
    }

}
