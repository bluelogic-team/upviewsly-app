import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, ToastController } from '@ionic/angular';
import { OnboardingUsernamePage } from '../onboarding-username/onboarding-username.page';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'src/app/providers/auth/auth.service';
import * as firebase from 'firebase';
import { FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

declare var google;

@Component({
    selector: 'app-onboarding-info',
    templateUrl: './onboarding-info.page.html',
    styleUrls: ['./onboarding-info.page.scss'],
})

export class OnboardingInfoPage implements OnInit {

    constructor(private navCtrl: NavParams,
        private modalCtrl: ModalController,
        private toastCtrl: ToastController,
        private translate: TranslateService,
        private authService: AuthService,
        private formBuilder: FormBuilder) {
        this.group = formBuilder.group({
            firstName: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50)])],
            lastName: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50)])],
            address: ['', Validators.required]
        })
    }
    pseudoSubject = new Subject<string>();
    // patternValidator = "^[a-zA-Zéàöçè_ -]{2,50}$";
    group: any;
    selectedPlace: any;
    address: any;

    autoComplete: any;
    zipCode: string = this.navCtrl.get('zipCode');
    city: string = this.navCtrl.get('city');
    country: string = this.navCtrl.get('country');
    latitude: string = this.navCtrl.get('latitude');
    longitude: string = this.navCtrl.get('logitude');
    placeId: string = this.navCtrl.get('placeId');
    firstName: string = this.navCtrl.get('firstName');
    lastName: string = this.navCtrl.get('lastName');
    adresse: string = this.navCtrl.get('address');
    username: string = "";
    firstActionOnFirstname: boolean = true;
    firstActionOnLastname: boolean = true;
    public isSending: boolean = false;

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.initAutocomplete();
    }

    async initAutocomplete() {
        if (document.getElementById('input-personal-address')) {

            var input = document.getElementById('input-personal-address').getElementsByTagName('input')[0];
            var options = {
                types: ['geocode'],
                componentRestrictions: { country: "fr" }
            };


            this.autoComplete = await new google.maps.places.Autocomplete(input, options);

            this.autoComplete.addListener('place_changed', async () => {
                this.latitude = this.autoComplete.getPlace().geometry.location.lat();
                this.longitude = this.autoComplete.getPlace().geometry.location.lng();
                this.placeId = this.autoComplete.getPlace().place_id;
                let route;
                let streetNumber;
                let countryLong
                this.city = "";
                this.zipCode = "";
                this.country = "";

                this.autoComplete.getPlace().address_components.forEach(async addressElement => {
                    if (addressElement.types.includes('street_number')) {
                        streetNumber = addressElement.short_name;
                    }
                    if (addressElement.types.includes('route')) {
                        route = addressElement.short_name;
                    }
                    if (addressElement.types.includes('locality')) {
                        this.city = addressElement.short_name;
                    }
                    if (addressElement.types.includes('country')) {
                        this.country = addressElement.short_name;
                        countryLong = addressElement.long_name;
                    }
                    if (addressElement.types.includes('postal_code')) {
                        this.zipCode = addressElement.short_name;
                    }
                });
                this.address = "";
                if (streetNumber) {
                    this.address += streetNumber;
                }

                if (route && streetNumber) {
                    this.address += " " + route;
                }

                if (route && !streetNumber) {
                    this.address += route;
                }

                if (this.city && route) {
                    this.address += ", " + this.city;
                }

                if (this.city && !route) {
                    this.address += this.city;
                }

                if (!this.city && this.country) {
                    this.address += countryLong;
                }
            });
        }
    }

    capitalizeFirstLetter(string) {
        if (typeof string !== "undefined") {
            return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
        }
    }

    changeState(e, inputType) {
        if(inputType === 'firstname') {
            if (e.detail.value.length >= 2 && this.firstActionOnFirstname) {
                this.firstActionOnFirstname = false;
            }
        }

        if(inputType === 'lastname') {
            if(e.detail.value.length >= 2 && this.firstActionOnLastname) {
                this.firstActionOnLastname = false;
            }
        }
    }

    fetchPseudo(value): Promise<void> {
        return new Promise<any>(async (resolve, reject) => {
            if (value !== '') {
                firebase.firestore().collection('users').where('username', '==', value.toLocaleLowerCase()).get().then(async user => {
                    // Available username
                    if (user.empty) {
                        // Set callOneTime to true once firebase return first available pseudo
                        this.username = value.replace(/[\W_]+/g, "").toLocaleLowerCase();
                        resolve(true);
                    }
                    // Not available username
                    else {
                        user.docs.forEach(async e => {
                            let reg = /\d+/g;
                            let usernameWihtoutNumber = e.data()['username'].replace(/\d+/g, '');
                            let number = await e.data()['username'];
                            let result = number.match(reg);
                            let newSearch = result !== null ? `${usernameWihtoutNumber}${parseInt(result[0]) + 1}` : value + '1';
                            // Callback to check new user
                            resolve(this.fetchPseudo(newSearch))
                        })
                    }

                }).catch(userErr => {
                    console.log(userErr.empty)
                });
            }
        });
    }

    async forward() {
        if (this.isSending) {
            return
        };

        if (this.group.valid && this.placeId) {
            this.isSending = true;
            this.fetchPseudo(this.firstName).then(async res => {
                this.modalCtrl.dismiss();
                let modal = await this.modalCtrl.create({
                    component: OnboardingUsernamePage,
                    componentProps: {
                    username: this.username,
                    name: this.firstName
                    },
                    backdropDismiss: false
                });
                await modal.present();

                firebase.firestore().collection("users").doc(this.authService.getCurrentUserId()).update({
                    displayName: this.capitalizeFirstLetter(this.firstName) + ' ' + this.capitalizeFirstLetter(this.lastName),
                    firstName: this.capitalizeFirstLetter(this.firstName),
                    lastName: this.capitalizeFirstLetter(this.lastName),
                    address: this.address ? this.address : '',
                    zipCode: this.zipCode ? this.zipCode : '',
                    city: this.city ? this.city : '',
                    country: this.country ? this.country : '',
                    latitude: this.latitude ? this.latitude : '',
                    longitude: this.longitude ? this.longitude : '',
                    placeId: this.placeId ? this.placeId : '',
                    username: this.username
                })
            }).catch(err => {
                console.log(err);
            });
        } else {
            this.isSending = false;
            let toast = await this.toastCtrl.create({
                message: this.translate.instant('onboarding.MISSING_PARAMETERS'),
                duration: 3000
            });
            await toast.present();
        };
    }
}
