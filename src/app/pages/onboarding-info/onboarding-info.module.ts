import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OnboardingInfoPage } from './onboarding-info.page';
import { TranslateModule } from '@ngx-translate/core';
import { OnboardingUsernamePageModule } from '../onboarding-username/onboarding-username.module';

const routes: Routes = [
  {
    path: '',
    component: OnboardingInfoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    OnboardingUsernamePageModule,
    TranslateModule.forChild(),
    ReactiveFormsModule
  ],
  declarations: [OnboardingInfoPage]
})
export class OnboardingInfoPageModule {}
