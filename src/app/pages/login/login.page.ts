import { Component, OnInit } from '@angular/core';
import { Platform, NavController, ToastController, LoadingController, ModalController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { TranslateService } from "@ngx-translate/core";

import { AuthService } from '../../providers/auth/auth.service';
import { NotificationsService } from 'src/app/providers/notifications/notifications.service';
import { OnboardingInfoPage } from '../onboarding-info/onboarding-info.page';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    loginForm: FormGroup;
    subscription: any;
    loading: any;
    email: string = '';
    password: string = '';
    errorMessage: string = '';

    constructor(
        private router: Router,
        private navCtrl: NavController,
        private splashscreen: SplashScreen,
        public authService: AuthService,
        public platform: Platform,
        public storage: Storage,
        public toastController: ToastController,
        public loadingController: LoadingController,
        public alertController: AlertController,
        private translate: TranslateService,
        private notifservice: NotificationsService,
        private modalCtrl: ModalController
    ) {
        this.loginForm = new FormGroup({
            password: new FormControl('', Validators.required),
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern(
                    "[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"
                )
            ]))
        });
    }

    ngOnInit() {
    }

    async ionViewDidEnter() {
        this.splashscreen.hide();
        this.loginForm.controls.email.setValue("");
        this.loginForm.controls.password.setValue("");

        //fix the ionic4 hardware back button to be able to leave the app
        this.subscription = this.platform.backButton.subscribe(() => {
            navigator['app'].exitApp();
        });
    }

    ionViewDidLeave() {
        this.subscription.unsubscribe();
        if (this.loading != undefined) {
            this.loading.dismiss();
        }
    }

    async presentLoading() {
        this.loading = await this.loadingController.create({
            message: this.translate.instant('common.LOADING'),
            //duration: 10000
        });
        await this.loading.present();
    }

    onSubmit(value) {
        if (this.loginForm.valid) {
            //this.router.navigate(['tabs']);
            this.tryLogin(value);
        }
        else {
            this.errorMessage = this.translate.instant('common.INPUT_MISSING');
            //this.presentToast();
        }
    }

    tryLogin(value) {
        this.presentLoading();
        this.authService.doLogin(value)
            .then(res => {
                this.notifservice.countObservable = this.notifservice.countUserNotifications().subscribe(
                    (res: any) => {
                        return this.notifservice.countNotify = res.length;
                    }
                )
                this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                    if (typeof user !== 'undefined') {
                        if (!user.firstName || !user.lastName || !user.address || !user.username) {
                            let modal = await this.modalCtrl.create({
                                component: OnboardingInfoPage,
                                componentProps: {
                                    firstName: user.firstName,
                                    lastName: user.lastName,
                                    address: user.address,
                                    placeId : user.placeId,
                                    zipCode: user.zipCode,
                                    latitude: user.latitude,
                                    longitude: user.longitude,
                                    city: user.city,
                                    country: user.country
                                },
                                backdropDismiss: false
                            });
                            await modal.present();
                            this.loading.dismiss();
                        } else {
                            this.router.navigate(['tabs']);
                            this.loading.dismiss();
                        }
                    }
                }).catch(err => {
                    console.log(err)
                    this.loading.dismiss();
                })
            }, err => {
                this.loading.dismiss();
                if (err.code == 'auth/wrong-password') {
                    this.errorMessage = this.translate.instant('common.INCORRECT_CREDENTIAL');
                }
                else if (err.code == 'auth/user-not-found') {
                    this.errorMessage = this.translate.instant('common.ACCOUNT_UNKNOWN');
                }
                else {
                    this.errorMessage = err.message;
                }
            })
    }

    loginWithGoogle() {
        this.presentLoading();
        if (this.platform.is('cordova')) {
            this.authService.nativeGoogleLogin()
                .then(usr => {
                    if (usr) {
                        this.notifservice.countObservable = this.notifservice.countUserNotifications().subscribe(
                            (res: any) => {
                                return this.notifservice.countNotify = res.length;
                            }
                        )
                        this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                            if (typeof user !== 'undefined') {
                                if (!user.firstName || !user.lastName || !user.address || !user.username) {
                                    let modal = await this.modalCtrl.create({
                                        component: OnboardingInfoPage,
                                        componentProps: {
                                            firstName: usr.user.displayName.split(' ', 2)[0],
                                            lastName: usr.user.displayName.split(' ', 2)[1],
                                            address: user.address,
                                            placeId : user.placeId,                                    
                                            zipCode: user.zipCode,
                                            latitude: user.latitude,
                                            longitude: user.longitude,
                                            city: user.city,
                                            country: user.country
                                        },
                                        backdropDismiss: false
                                    });
                                    await modal.present();
                                    this.loading.dismiss();
                                } else {
                                    this.router.navigate(['tabs']);
                                    this.loading.dismiss();
                                }
                            }
                        }).catch(err => {
                            console.log(err)
                            this.loading.dismiss();
                        })
                    } else {
                        console.log('google log returned:', usr);
                        this.loading.dismiss();
                    }
                })
                .catch(err => {
                    this.loading.dismiss();
                    this.errorMessage = err.message;
                })
        } else {
            this.authService.webGoogleLogin()
                .then((res) => {
                    this.notifservice.countObservable = this.notifservice.countUserNotifications().subscribe(
                        (res: any) => {
                            return this.notifservice.countNotify = res.length;
                        }
                    )
                    this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                        if (typeof user !== 'undefined') {
                            if (!user.firstName || !user.lastName || !user.address || !user.username) {
                                let modal = await this.modalCtrl.create({
                                    component: OnboardingInfoPage,
                                    componentProps: {
                                        firstName: user.firstName,
                                        lastName: user.lastName,
                                        address: user.address,
                                        placeId : user.placeId,
                                        zipCode: user.zipCode,
                                        latitude: user.latitude,
                                        longitude: user.longitude,
                                        city: user.city,
                                        country: user.country
                                    },
                                    backdropDismiss: false
                                });
                                await modal.present();
                                this.loading.dismiss();
                            } else {
                                this.router.navigate(['tabs']);
                                this.loading.dismiss();
                            }
                        }
                    }).catch(err => {
                        console.log(err)
                        this.loading.dismiss();
                    })
                }
                ).catch(err => {
                    this.loading.dismiss();
                    this.errorMessage = err.message;
                })
        }
    }

    loginWithFacebook() {
        this.presentLoading();
        if (this.platform.is('cordova')) {
            this.authService.nativeFacebookLogin()
                .then(usr => {
                    if (usr) {
                        this.notifservice.countObservable = this.notifservice.countUserNotifications().subscribe(
                            (res: any) => {
                                return this.notifservice.countNotify = res.length;
                            }
                        )
                        this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                            if (typeof user !== 'undefined') {
                                if (!user.firstName || !user.lastName || !user.address || !user.username) {
                                    let modal = await this.modalCtrl.create({
                                        component: OnboardingInfoPage,
                                        componentProps: {
                                            firstName: usr.user.displayName.split(' ', 2)[0],
                                            lastName: usr.user.displayName.split(' ', 2)[1],
                                            address: user.address,
                                            placeId : user.placeId,
                                            zipCode: user.zipCode,
                                            latitude: user.latitude,
                                            longitude: user.longitude,
                                            city: user.city,
                                            country: user.country
                                        },
                                        backdropDismiss: false
                                    });
                                    await modal.present();
                                    this.loading.dismiss();
                                } else {
                                    this.router.navigate(['tabs']);
                                    this.loading.dismiss();
                                }
                            }
                        }).catch(err => {
                            console.log(err)
                            this.loading.dismiss();
                        })
                    } else {
                    }
                }, err => {
                    this.loading.dismiss();
                    if (err.code == 'auth/account-exists-with-different-credential') {
                        this.errorMessage = this.translate.instant('common.ACCOUNT_ALREADY_EXISTS');
                    }
                    else {
                        this.errorMessage = err.message;
                    }
                })
        } else {
            this.authService.webFacebookLogin()
                .then((res) => {
                    this.notifservice.countObservable = this.notifservice.countUserNotifications().subscribe(
                        (res: any) => {
                            return this.notifservice.countNotify = res.length;
                        }
                    )
                    this.authService.getUserInfo(this.authService.getCurrentUserId()).then(async user => {
                        if (typeof user !== 'undefined') {
                            if (!user.firstName || !user.lastName || !user.address || !user.username) {
                                let modal = await this.modalCtrl.create({
                                    component: OnboardingInfoPage,
                                    componentProps: {
                                        firstName: user.firstName,
                                        lastName: user.lastName,
                                        address: user.address,
                                        placeId : user.placeId,
                                        zipCode: user.zipCode,
                                        latitude: user.latitude,
                                        longitude: user.longitude,
                                        city: user.city,
                                        country: user.country
                                    },
                                    backdropDismiss: false
                                });
                                await modal.present();
                                this.loading.dismiss();
                            } else {
                                this.router.navigate(['tabs']);
                                this.loading.dismiss();
                            }
                        }
                    }).catch(err => {
                        console.log(err)
                        this.loading.dismiss();
                    })
                }
                ).catch(err => {
                    this.loading.dismiss();
                    this.errorMessage = err.message;
                })
        }
    }



}
