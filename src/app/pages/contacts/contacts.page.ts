import { Component, OnInit } from '@angular/core';
import { Contacts, Contact, ContactField, ContactFieldType, ContactName, ContactFindOptions } from '@ionic-native/contacts/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { Platform, AlertController, LoadingController, NavParams, ModalController} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from "@ngx-translate/core";
import { OpenNativeSettings } from '@ionic-native/open-native-settings/ngx';
import { AuthService } from '../../providers/auth/auth.service';
import { FriendsService } from '../../providers/friends/friends.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.page.html',
  styleUrls: ['./contacts.page.scss'],
})
export class ContactsPage implements OnInit {

  localContacts = [];
  displayedContacts = [];
  selectedContacts = [];
  displaySendButton = false;
  searchContactString: "";
  loader: any;
  contactsFromApp: boolean;

  constructor(
    private modalCtrl: ModalController,
    private loadingController: LoadingController,
    private alertController: AlertController,
    public navParams: NavParams,
    private contacts: Contacts,
    private diagnostic: Diagnostic,
    private translate: TranslateService,
    private storage: Storage,
    private openNativeSettings: OpenNativeSettings,
    public platform: Platform,
    public authService: AuthService,
    public friendsService: FriendsService,
    private socialSharing: SocialSharing,
    private db: AngularFirestore,
  ) {
    this.contactsFromApp = this.navParams.get('contactsFromApp');
     }

  ngOnInit() {
  }

  ionViewDidEnter() {
    // setTimeout(this.checkContactsAuthorization(), 100);
    this.checkContactsAuthorization();
  }

  ionViewWillLeave(){
    if (this.loader != undefined) {
      this.loader.dismiss();
    }
  }

  checkContactsAuthorization() {
    this.diagnostic.isContactsAuthorized().then((data) => {
      if (data) {
        this.retrieveContacts();
      } else {
        this.askContactsAuthorization();
      }
    }).catch((error) => {
      console.log("Access contact error", error);
    })
  }

  /* Ask user if not asked or go to settings */
  async askContactsAuthorization(){

    const alert = await this.alertController.create({
      header: this.translate.instant('contacts.ACCES_CONTACT_TITLE'),
      message: this.translate.instant('contacts.ACCES_CONTACT_MESSAGE'),
      buttons: [
        {
          text: this.translate.instant('common.LATER'),
          handler: data => console.log('Cancel clicked')
        },
        {
          text: this.translate.instant('common.YES'),
          handler: data => {
            // Ask user permission
            this.diagnostic.getContactsAuthorizationStatus().then((state) => {
              if (state == this.diagnostic.permissionStatus.NOT_REQUESTED) {
                this.diagnostic.requestContactsAuthorization().then((data) => { this.retrieveContacts(); }).catch((error) => console.log(error));
              } else {
                this.openNativeSettings.open("application_details");
                this.closeModal();
              }
            })
          }
        }
      ]
    });

    await alert.present();

  }


  async retrieveContacts(){
    if (this.platform.is("cordova")){

      var options      = new ContactFindOptions();
      options.filter   = "";
      options.multiple = true;
      //options.desiredFields = [navigator.contacts.fieldType.id];
      options.hasPhoneNumber = true;
      let fields: ContactFieldType[] = ['displayName', 'name'];
      
      this.loader = await this.loadingController.create({
        message: this.translate.instant('common.LOADING'),
        spinner: 'crescent',
        duration: 30000
      });
      await this.loader.present();
    

      this.contacts.find(fields, options)
      .then( async res => {
        res.forEach(contact => {
          Object.assign(contact, {checked: false});
        });

        res.sort((a,b) => {
          if(a.name.formatted < b.name.formatted)
            return -1;
          if(a.name.formatted > b.name.formatted)
            return 1;
          return 0;   // a's name == b's name
        });

        if(this.contactsFromApp){
          let currentUserEmail;
          this.authService.getCurrentUser().then((usr) => {
            var docRef = this.db.collection("users").doc(usr.uid);
                  docRef.ref.get().then(doc => {
                      if (doc.exists) {
                         currentUserEmail =  doc.data()['email'];
                         let tmpContacts = [];
                         res.forEach(contact => {
                          if(contact.emails){
                            if(contact.emails[0].value != currentUserEmail){
                             this.checkUserHasApp(contact.emails[0].value).then((userHasApp) => {
                               if(userHasApp){
                                 this.checkAlreadyFriends(userHasApp[0]).then((isFriend) => {
                                    this.checkAlreadyRequested(userHasApp[0]).then((isAlreadyRequested) => {
                                        let newFriend = Object.assign(contact, {firebaseId: userHasApp[0]}, {isAlreadyFriend: isFriend}, {isAlreadyRequested: isAlreadyRequested});
                                        tmpContacts.push(newFriend);
                                    });
                                 });
                               }
                             });
                           }
                          }
                        });
                         this.localContacts = tmpContacts;
                         this.displayedContacts = tmpContacts;
                      } else {
                          console.log("error user missing in database");
                      }
                  }).catch(err => {
                      throw new err;
                });

            }, (error) => {
                  throw new error;
            });
        } else {
          this.localContacts = res;
          this.displayedContacts = res;
        }
        
      await this.loader.dismiss();
      })
      .catch( err => {
        console.log("error : ", err);
      })
    }
  }

  checkUserHasApp(email) {
    return new Promise(resolve=>{
        this.authService.checkUserHasApp(email)
         .subscribe(
            (data:any) => {
                if(data.length == 0){
                   resolve(false);
                } else {
                   resolve([data[0].id, true]);
                }
         })
    })
}

  checkAlreadyFriends(friendId) {
    return new Promise(resolve=>{
      let userId = this.authService.getCurrentUserId();
        this.friendsService.checkUserIsAlreadyFriend(userId,friendId)
         .subscribe(
            (data:any) => {
                if(data.length == 0){
                   resolve(false);
                } else {
                   resolve(true);
                }
         })
    })
  }

  checkAlreadyRequested(friendId) {
    return new Promise(resolve=>{
      let userId = this.authService.getCurrentUserId();
        this.friendsService.checkUserAlreadyFriendRequested(userId,friendId)
         .subscribe(
            (data:any) => {
                if(data.length == 0){
                   resolve(false);
                } else {
                   resolve(true);
                }
         })
    })
}

sendFriendRequest(contact){
  this.friendsService.sendFriendRequest(contact);
}

  searchContact() {
    if (this.searchContactString.trim() != '') {
      let str = this.searchContactString.toLowerCase();
      
      this.displayedContacts = this.localContacts.filter(
        contact => {
          return (contact.name.givenName && contact.name.givenName.toLowerCase().indexOf(str) > -1)
          || (contact.name.familyName && contact.name.familyName.toLowerCase().indexOf(str) > -1)
        }
      ); 

    } else {
      this.displayedContacts = this.localContacts;
    }

  }

  // Check or uncheck contact
  toggleContact(contact) {
    // if (contact.checked && !(this.selectedContacts.filter(sc => sc.email === contact.email).length > 0)) {
    if (!contact.checked) {
      //contact.checked = true;
      this.selectedContacts.push(contact);
    } else {
      let index = this.selectedContacts.indexOf(contact);
      if (index > -1) {
        this.selectedContacts.splice(index, 1);
        //contact.checked = false;
      }
    }
    this.toggleSendButton();
  }

  // Show or hide send button 
  toggleSendButton() {
    if (this.selectedContacts.length >= 1) {
      this.displaySendButton = true;
    } else {
      this.displaySendButton = false;
    }
  }

  saveSelection(){
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    let phoneNumbers = [];
    this.selectedContacts.forEach(element => {
      if(element.phoneNumbers){
        phoneNumbers.push(element.phoneNumbers[0].value.split(' ').join(''));
      }
    });

    this.authService.getCurrentUser().then((res) => {
      var docRef = this.db.collection("users").doc(res.uid);
            docRef.ref.get().then(doc => {
                if (doc.exists) {
                  this.translate.get("contacts.SHARE_MESSAGE_SMS", { username: doc.data()['username'] }).subscribe((res: string) => {
                    this.socialSharing.shareViaSMS(res, phoneNumbers.toString());
                  });
                } else {
                    console.log("error user missing in database");
                }
            }).catch(err => {
                throw new err;
          });

      }, (error) => {
            throw new error;
      });
    this.modalCtrl.dismiss();
  }

  closeModal(){
    this.modalCtrl.dismiss();
  }
 
}




