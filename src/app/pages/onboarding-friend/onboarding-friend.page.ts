import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AddFriendsPage } from '../add-friends/add-friends.page';
import { Router } from '@angular/router';

@Component({
    selector: 'app-onboarding-friend',
    templateUrl: './onboarding-friend.page.html',
    styleUrls: ['./onboarding-friend.page.scss'],
})
export class OnboardingFriendPage implements OnInit {

    constructor(private modalCtrl: ModalController, private router: Router) { }

    ngOnInit() {
    }

    forward() {
        this.modalCtrl.dismiss();
        this.router.navigate(['/tabs']);
    }

    async openFriend() {
        this.modalCtrl.dismiss();
        let modal = await this.modalCtrl.create({
            component: AddFriendsPage
        });
        await modal.present()
        modal.onWillDismiss().then(() => {
            this.modalCtrl.dismiss();
            this.router.navigate(['tabs']);
        })
    }

}
