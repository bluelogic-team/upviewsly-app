import { Component, OnInit } from '@angular/core';
import { GroupService } from '../../providers/group/group.service';
import { map } from 'rxjs/operators';
import { AuthService } from '../../providers/auth/auth.service';
import { ModalController, AlertController } from '@ionic/angular';
import { TranslateService } from "@ngx-translate/core";

import { AddFriendsPage } from '../add-friends/add-friends.page';
import { FriendsService } from '../../providers/friends/friends.service';
import { NotificationsService } from 'src/app/providers/notifications/notifications.service';
import { UserProfilePage } from '../user-profile/user-profile.page';

@Component({
    selector: 'app-friends',
    templateUrl: './friends.page.html',
    styleUrls: ['./friends.page.scss'],
})
export class FriendsPage implements OnInit {
    public displayType = "friends";
    groups: any[];
    userId: string;
    private friends;
    public friendRequests;
    public displayedFriends;
    public filteredFriends;
    public filteredGroups;
    public searchUserString: any;
    public searchGroupString: any;
    user: any;

    constructor(
        private groupService: GroupService,
        public authService: AuthService,
        public modalController: ModalController,
        public alertController: AlertController,
        public translate: TranslateService,
        public friendsService: FriendsService,
        public notifService: NotificationsService
    ) { }

    async ngOnInit() {
        this.user = await this.authService.getCurrentUser();
        this.getFriends();
        this.getFriendRequests();
    }

    async ionViewWillEnter() {

    }

    ionViewDidEnter() {
        this.getFriends();
        this.getFriendRequests();
        this.loadGroups(this.user);
    }
    
    async showUser(userId: string) {
        let modal = await this.modalController.create({
            component: UserProfilePage,
            componentProps: {
                userId: userId
            }
        });
        modal.present();
    }

    async loadGroups(user) {
        let groupCollection = this.groupService.getUserGroups(user.uid);

        let obsGroup = groupCollection.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            })
        )
        obsGroup.subscribe((value) => {
            this.groups = value;
            this.filteredGroups = this.groups;
        });
    }

    remove(item) {
        this.groupService.removeGroup(item.id);
    }

    async presentAddFriends() {
        const modal = await this.modalController.create({
            component: AddFriendsPage
        });
        await modal.present();
    }

    acceptRequest(request) {
        this.friendsService.acceptRequest(request);
        this.getFriends();
    }

    rejectRequest(request) {
        this.friendsService.rejectRequest(request);
        this.getFriends();
    }

    async removeFriend(friend) {
        const alert = await this.alertController.create({
            message: this.translate.instant('friends.CONFIRM_DELETE'),
            buttons: [
                {
                    text: this.translate.instant('common.CANCEL'),
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: this.translate.instant('common.DELETE'),
                    handler: async () => {
                        await this.friendsService.removeFriend(friend);
                        this.getFriends();
                    }
                }
            ]
        });
        await alert.present();
    }

    async getFriendRequests() {
        await this.friendsService.checkFriendRequest().then(res => {
            res.subscribe(val => {
                this.friendRequests = val.map((request: any) => {
                    let requester = this.authService.getUserInfo(request.requester).then(res => {
                        const data = { user: res, request: request };
                        return { res, ...data }
                    })
                    return requester
                })
            })
        });
    }

    async getFriends() {
        let friends = [];
        let tmpFriends = [];
        return this.friendsService.getUserFriends().then(async data => {
            data.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
                friends.push({docId : doc.id, uid: doc.data()['uid']});
            });

            let that = this;
            friends.forEach(function(friend, i){
                that.authService.getUserInfo(friend.uid).then(res => {
                    let user = Object.assign(res, { firebaseId: friend.uid });
                    tmpFriends.push(user);
                    if(i == friends.length-1){
                        tmpFriends.sort(function(a, b){
                            if(a.firstName < b.firstName) { return -1; }
                            if(a.firstName > b.firstName) { return 1; }
                            return 0;
                        })
                        that.filteredFriends = tmpFriends;
                        that.friends = tmpFriends;
                    }
                })
            });
        });
    }

    searchFriend(ev: any) {
        // set val to the value of the searchbar
        const val = ev.detail.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.filteredFriends = this.friends.filter((item) => {
                if (item.displayName) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                else if (item.firstName) {
                    return (item.firstName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                else {
                    return (item.lastName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
            })
        } else {
            this.filteredFriends = this.friends;
        }
    }

    searchGroup(ev: any) {

        // set val to the value of the searchbar
        const val = ev.target.value;
        this.filteredGroups = this.groups;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.filteredGroups = this.filteredGroups.filter((item) => {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        } else {
            this.filteredGroups = this.groups;
        }
    }
}
