import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { GroupService } from '../../providers/group/group.service';
import { AuthService } from '../../providers/auth/auth.service';
import { FavoriteService } from "../../providers/favorite/favorite.service"
import { ActivatedRoute } from "@angular/router";
import { EditFavoritePage } from '../edit-favorite/edit-favorite.page';
import { AlertController } from '@ionic/angular';
import { TranslateService } from "@ngx-translate/core";
import { Router } from '@angular/router';
import * as Config from '../../../utils/config';
import { NotificationsService } from 'src/app/providers/notifications/notifications.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

declare var google;
declare var MarkerClusterer: any;
declare var mapIcons;
let map: any;
let infowindow: any;
let placeInfo: any;

@Component({
    selector: 'app-map',
    templateUrl: './map.page.html',
    styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

    public displayType = "map";

    @ViewChild('map') mapElement: ElementRef;

    coords = [];
    members = [];
    groupId = null //'oBLLtcfCqwPl52wmNvLh'; //this.route.snapshot.paramMap.get("groupId")
    menuIcon = 'map';
    favoriteList = [];
    tagList;

    eventPlace: any;
    pos: any;
    zoom: any;
    autoComplete: any
    selectedPlace: any;
    placeId: any
    streetNumber: any;
    street: any;
    city: any;
    zipCode: any;
    country: any;
    positionObj: any;
    markers = new Array()
    markersPositions: any = new Array();
    positionDefine: boolean;
    //Google maps center coord dyn
    google_lat: number;
    google_lng: number;
    markerCluster: any;
    lat: any;
    lng: any;
    isService: boolean = false;

    // Loading pois
    public isLoading: boolean = false;

    // Options cluster
    mcOptions = {
        gridSize: 100,
        minimumClusterSize: 4,
        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
    };

    constructor(
        private favoriteService: FavoriteService,
        private groupService: GroupService,
        private authService: AuthService,
        private translate: TranslateService,
        private router: Router,
        private alertController: AlertController,
        private route: ActivatedRoute,
        public loadingController: LoadingController,
        private modalCtrl: ModalController,
        public notifService: NotificationsService,
        private ngZone: NgZone,
        private geolocation: Geolocation
    ) {
    }

    async ngOnInit() {

        const loading = await this.loadingController.create({
            message: this.translate.instant('common.LOADING')
        });
        await loading.present();

        this.coords = []
        this.members = []

        if (this.groupId) {

            new Promise<string>(() => {
                this.getGroupMembers(this.groupId).then(async () => {
                    this.members.forEach(async memberId => {
                        await this.setCoord(memberId);
                    });
                }).then(async () => {
                    await loading.dismiss();
                });
            });
        }
        else {

            this.setCoord(this.authService.getCurrentUserId()).then(async () => {
            }).then(async () => {
                await loading.dismiss();
            })

        }
        await this.initAutocomplete()
            .then(async () => {
                this.authService.getUserCoords(this.authService.getCurrentUserId()).then(res => {
                    if (res) {
                        if (res.latitude != '' && res.longitude != '') {
                            this.pos = { lat: res.latitude, lng: res.longitude }
                            this.zoom = Config.DEFAULT_ZOOM_HIGH;
                            this.positionObj = this.pos;
                        } else {
                            this.pos = { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG }
                            this.zoom = Config.DEFAULT_ZOOM_LOW;
                        }
                    } else {
                        this.pos = { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG }
                        this.zoom = Config.DEFAULT_ZOOM_LOW;
                    }
                }).then(async () => {
                    this.initMap();
                });
                await loading.dismiss();
            });

    }

    ionViewWillLeave() {
        this.menuIcon = 'map';
    }

    ionViewWillEnter() {
        if (this.route.snapshot.paramMap.get('placeId') !== null) {
            this.placeId = this.route.snapshot.paramMap.get('placeId');
        }
    }

    ionViewDidEnter() {
        this.getAllFavorite();

        if (this.route.snapshot.paramMap.get('placeId') === null) {
            this.geolocation.getCurrentPosition().then(res => {
                if (res) {
                    let that = this;
                    this.clearMarkers();
                    this.markers = [];
                    if (typeof this.markerCluster !== 'undefined') {
                        this.markerCluster.clearMarkers();
                    }
                    this.positionObj.lat = res.coords.latitude;
                    this.positionObj.lng = res.coords.longitude;
                    this.lat = res.coords.latitude;
                    this.lng = res.coords.longitude;
                    let newCenter = { lat: res.coords.latitude, lng: res.coords.longitude }
                    this.createPositionMarker(newCenter, 'GPS');
                    map.setCenter(newCenter);
                    var service = new google.maps.places.PlacesService(map);
                    var bounds = new google.maps.LatLngBounds(map.getBounds().getSouthWest(), map.getBounds().getNorthEast());
                    service.nearbySearch({
                        //location: { lat: res.coords.latitude, lng: res.coords.longitude },
                        //radius: 500,
                        bounds: bounds,
                        types: ['restaurant', 'bar', 'cafe']
                    }, (results, status) => {
                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                            for (var i = 0; i < results.length; i++) {
                                that.createMarker(results[i]);
                                if (i + 1 === results.length) {
                                    setTimeout(() => { that.markerCluster = new MarkerClusterer(map, that.markers, that.mcOptions);}, 500);
                                }
                            }
                        }
                    });
                    map.setZoom(Config.DEFAULT_ZOOM_HIGH);
                }
                // res && setTimeout(() => this.createPositionMarker(res.coords, 'GPS'), 2000);
            }).catch(() => {
                this.authService.getUserCoords(this.authService.getCurrentUserId()).then(res => {
                    if (res) {
                        if (res.latitude != '' && res.longitude != '') {
                            this.pos = { lat: res.latitude, lng: res.longitude }
                            this.zoom = Config.DEFAULT_ZOOM_HIGH;
                            return this.positionObj = this.pos;
                        } else {
                            this.pos = { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG }
                            this.zoom = Config.DEFAULT_ZOOM_LOW;
                            return this.positionObj = this.pos;
                        }
                    } else {
                        this.pos = { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG }
                        this.zoom = Config.DEFAULT_ZOOM_LOW;
                        return this.positionObj = this.pos;
                    }
                }).then(async (res) => {
                    // Set newCenter if user change address
                    if (typeof config.positionDefine === 'undefined') {
                        let that = this;
                        this.positionDefine = true;
                        this.lat = await res.lat;
                        this.lng = await res.lng;
                        let newCenter = { lat: await res.lat, lng: await res.lng }
                        this.createPositionMarker(this.pos, 'HOME');
                        await map.setCenter(newCenter);
                        var service = new google.maps.places.PlacesService(map);
                        var bounds = new google.maps.LatLngBounds(map.getBounds().getSouthWest(), map.getBounds().getNorthEast());
                        await service.nearbySearch({
                            //location: { lat: res.lat, lng: res.lng },
                            //radius: 500,
                            bounds: bounds,
                            types: ['restaurant', 'bar', 'cafe']
                        }, (results, status) => {
                            if (status === google.maps.places.PlacesServiceStatus.OK) {
                                for (var i = 0; i < results.length; i++) {
                                    that.createMarker(results[i]);
                                    if (i + 1 === results.length) {
                                        setTimeout(() => { that.markerCluster = new MarkerClusterer(map, that.markers, that.mcOptions);}, 500);
                                    }
                                }
                            }
                        });

                        await map.setZoom(Config.DEFAULT_ZOOM_HIGH);
                    }
                    if (config.positionDefine) {
                        if (this.positionObj.lat != this.lat && this.positionObj.lng != this.lng) {
                            let that = this;
                            this.clearMarkers();
                            this.markers = [];
                            if (typeof this.markerCluster !== 'undefined') {
                                this.markerCluster.clearMarkers();
                            }
                            this.positionObj.lat = res.lat;
                            this.positionObj.lng = res.lng;
                            this.lat = res.lat;
                            this.lng = res.lng;
                            let newCenter = { lat: res.lat, lng: res.lng }
                            map.setCenter(newCenter);
                            var service = new google.maps.places.PlacesService(map);
                            var bounds = new google.maps.LatLngBounds(map.getBounds().getSouthWest(), map.getBounds().getNorthEast());
                            service.nearbySearch({
                                //location: { lat: res.lat, lng: res.lng },
                                //radius: 500,
                                bounds: bounds,
                                types: ['restaurant', 'bar', 'cafe']
                            }, (results, status) => {
                                if (status === google.maps.places.PlacesServiceStatus.OK) {
                                    for (var i = 0; i < results.length; i++) {
                                        that.createMarker(results[i]);
                                        if (i + 1 === results.length) {
                                            setTimeout(() => { that.markerCluster = new MarkerClusterer(map, that.markers, that.mcOptions);}, 500);
                                        }
                                    }
                                }
                            });
                            map.setZoom(Config.DEFAULT_ZOOM_HIGH);
                        }
                    }
                });
            })

            let config = { positionDefine: this.positionDefine }
        }

    }

    // ******** FAVORITES ********** //

    async initAutocomplete() {

        if (document.getElementById('input-address-map')) {

            var input = document.getElementById('input-address-map').getElementsByTagName('input')[0];
            var options = {
                types: ['geocode', 'establishment'],
                componentRestrictions: { country: "fr" }
            };

            this.autoComplete = new google.maps.places.Autocomplete(input, options);

            this.autoComplete.addListener('place_changed', async () => {
                if (this.isLoading) {
                    return
                }

                this.clearMarkers();
                this.markers = [];
                if (typeof this.markerCluster !== 'undefined') {
                    this.markerCluster.clearMarkers();
                }

                this.selectedPlace = this.autoComplete.getPlace();
                this.eventPlace = '';
               
                this.placeId = this.autoComplete.getPlace().place_id;

                // this.setAddress(this.autoComplete.getPlace());
                this.markers = [];
                var service = new google.maps.places.PlacesService(map);

                let that = this;
                service.getDetails({
                    placeId: this.autoComplete.getPlace().place_id
                }, async function(result) {
                    that.selectedPlace = await result
                    that.setAddress(result);
                    let newCenter = { lat: result.geometry.location.lat(), lng: result.geometry.location.lng() }
                    map.setCenter(newCenter);
                    map.setZoom(Config.DEFAULT_ZOOM_HIGH);
                    if(result.types.includes('restaurant') || result.types.includes('bar') || result.types.includes('cafe')){
                        // Display marker of selected place
                        that.createMarker(result);
                    } else {
                        that.createMarker(result);
                        var bounds = new google.maps.LatLngBounds(map.getBounds().getSouthWest(), map.getBounds().getNorthEast());
                        // Nearbysearch
                        service.nearbySearch({
                            bounds: bounds,
                            //location: { lat: result.geometry.location.lat(), lng: result.geometry.location.lng() },
                            //radius: 1000,
                            //bounds: that.getDistanceM(map.getBounds().getNorthEast().lat(), map.getBounds().getNorthEast().lng(), map.getBounds().getSouthWest().lat(), map.getBounds().getSouthWest().lng()),
                            types: ['restaurant', 'bar', 'cafe']
                        }, (results, status, pagination) => {
                            if (status === google.maps.places.PlacesServiceStatus.OK) {
                                that.isLoading = true;
                                for (var i = 0; i < results.length; i++) {
                                    // bounds.extend(new google.maps.LatLng(results[i].geometry.location.lat(), results[i].geometry.location.lng()))
                                    that.createMarker(results[i]);

                                    // On ending request for one query
                                    if (i + 1 === results.length) {
                                        if (pagination.hasNextPage) {
                                            pagination.nextPage();
                                        }

                                        if (!pagination.hasNextPage) {
                                            that.isLoading = false;
                                            // map.fitBounds(bounds)
                                            setTimeout(() => { that.markerCluster = new MarkerClusterer(map, that.markers, that.mcOptions);}, 500);
                                        }
                                    }
                                }
                            }
                        });

                    }
                })
            });
            // this.initMap();
        }
    }

    setAddress(place) {

        let isEstablishment = false;
        this.eventPlace = '';
        this.streetNumber = '';
        this.street = '';
        this.city = '';
        if (place.types.indexOf("establishment") != -1) {
            isEstablishment = true;
            this.eventPlace = place.name
        }

        for (let i = 0; i < place.address_components.length; i++) {
            let component = place.address_components[i];
            let addressType = component.types[0];

            switch (addressType) {
                case 'street_number':
                    this.streetNumber = component.long_name;
                    break;
                case 'route':
                    this.street = component.short_name;
                    break;
                case 'locality':
                    this.city = component.long_name;
                    break;
                case 'postal_code':
                    this.zipCode = component.long_name;
                    break;
                case 'country':
                    this.country = component.long_name;
                    break;
            }
        }

        if (!this.streetNumber) { this.streetNumber = null }
        if (!this.street) { this.street = null }
        if (!this.city) { this.city = null }
        if (!this.zipCode) { this.zipCode = null }
        if (!this.country) { this.country = null }

        if (this.streetNumber != null && this.street != null && this.city != null) {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.streetNumber + ' ' + this.street + ', ' + this.city;
            } else {
                this.eventPlace = this.streetNumber + ' ' + this.street + ', ' + this.city;
            }
        }
        else if (this.street != null && this.city != null) {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.street + ', ' + this.city
            } else {
                this.eventPlace = this.street + ', ' + this.city
            }
        }
        else if (this.city != null) {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.city;
            } else {
                this.eventPlace = this.city;
            }
        }
        else {
            this.eventPlace = null;
        }
    }

    getAllFavorite() {
            this.favoriteService.getUserFavoritePlaces().subscribe(data => {
                this.favoriteList = data.map(e => {
                    this.tagList = e.payload.doc.data()['tags'];
                    return {
                        docId: e.payload.doc.id,
                        id: e.payload.doc.data()['id'],
                        name: e.payload.doc.data()['name'],
                        address: e.payload.doc.data()['address'],
                        lat: e.payload.doc.data()['lat'],
                        lng: e.payload.doc.data()['lng'],
                        tags: e.payload.doc.data()['tags']
                    };
                })
            });
    }

    async editFavorite(item, type) {
        let modalEdit = await this.modalCtrl.create({
            component: EditFavoritePage,
            componentProps: {
                place: item,
                tagList: item.tags,
                type: type
            }
        });
        await modalEdit.present();
        await modalEdit.onWillDismiss().then(async value => {
            // true if validation button is clicked, false if back button is pressed
        })
    }

    async removeFavorite(placeId) {

        const alert = await this.alertController.create({
            message: this.translate.instant('map.CONFIRM_DELETE'),
            buttons: [
                {
                    text: this.translate.instant('common.CANCEL'),
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: () => {
                        console.log('Confirm Cancel');
                    }
                }, {
                    text: this.translate.instant('common.DELETE'),
                    handler: () => {
                        this.favoriteService.removeUserFavoritePlace(placeId);
                        console.log('Confirm Okay');
                    }
                }
            ]
        });
        await alert.present();
    }

    // ******** MAP ********** //

    // get group members id
    getGroupMembers(groupId) {

        this.coords = []

        if (groupId != null) {
            return new Promise<any>((resolve, reject) => {
                this.groupService.getGroupInfo(groupId)
                    .then(res => {
                        res.members.forEach(element => {
                            this.members.push(element);
                        });
                        resolve(res);
                    }, err => reject(err))
            });
        }
    }

    async setCoord(userId) {

        await this.authService.getUserCoords(userId).then(res => {
            if (res.latitude && res.longitude) {
                let userCoord = { name: res.displayName, lat: res.latitude, lng: res.longitude }
                this.coords.push(userCoord)
            }
        });
    }

    // group center definition
    getDataMap() {

        var centerLat = 0;
        var centerLng = 0;
        var zoom = Config.DEFAULT_ZOOM_HIGH;
        var radius = 500;

        let mapViewBox = [
            { latSw: 0, lngSw: 0 },
            { latNe: 0, lngNe: 0 }
        ];

        // there is at least 1 member in the group
        if (this.coords.length > 0) {
            // get NE and SW points for viewbox
            mapViewBox[0].latSw = Math.min.apply(Math, this.coords.map(function(o) { return o.lat; }));
            mapViewBox[0].lngSw = Math.min.apply(Math, this.coords.map(function(o) { return o.lng; }));
            mapViewBox[1].latNe = Math.max.apply(Math, this.coords.map(function(o) { return o.lat; }));
            mapViewBox[1].lngNe = Math.max.apply(Math, this.coords.map(function(o) { return o.lng; }));

            // Calculate the map center
            centerLng = (mapViewBox[0].lngSw + mapViewBox[1].lngNe) / 2;
            centerLat = (mapViewBox[0].latSw + mapViewBox[1].latNe) / 2;

            if (this.coords.length > 1) {

                // Calculate zoom level and radius zone
                zoom = this.getZoom(mapViewBox);
                radius = this.getRadius(mapViewBox);
            }
        }
        else { // default point for display map, if there is no point 
            centerLat = Config.DEFAULT_CENTER_LAT;
            centerLng = Config.DEFAULT_CENTER_LNG;
            zoom = Config.DEFAULT_ZOOM_HIGH;
        }

        let dataMap = {
            lat: centerLat,
            lng: centerLng,
            zoom: zoom,
            radius: radius
        };
        return dataMap;
    }

    // Center control map
    centerControl(controlDiv, map) {
        let self = this;
        this.isLoading = false;
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';
        controlUI.style.textAlign = 'center';
        controlUI.title = this.translate.instant('common.SEARCH_POI');
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '16px';
        controlText.style.lineHeight = '38px';
        controlText.style.paddingLeft = '5px';
        controlText.style.paddingRight = '5px';
        controlText.innerHTML = this.translate.instant('common.SEARCH_POI');
        controlUI.appendChild(controlText);
        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
            if (self.isLoading) {
                return
            }
            controlText.innerHTML = self.translate.instant('common.LOADING');
            var getNextPage = null;
            let dataMap = self.getDataMap(); // get new map datas
            var service = new google.maps.places.PlacesService(map);
            self.clearMarkers();
            self.markers = [];
            if (typeof self.markerCluster !== 'undefined') {
                self.markerCluster.clearMarkers();
            }
            var bounds = new google.maps.LatLngBounds(map.getBounds().getSouthWest(), map.getBounds().getNorthEast());
            return service.nearbySearch({
                //location: { lat: map.getCenter().lat(), lng: map.getCenter().lng() },
                //radius: self.getDistanceM(map.getBounds().getNorthEast().lat(), map.getBounds().getNorthEast().lng(), map.getBounds().getSouthWest().lat(), map.getBounds().getSouthWest().lng()),
                bounds: bounds,
                types: ['restaurant', 'bar', 'cafe']
            }, (results, status, pagination) => {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    self.isLoading = true;
                    for (var i = 0; i < results.length; i++) {
                        self.createMarker(results[i]);
                        // bounds.extend(new google.maps.LatLng(results[i].geometry.location.lat(), results[i].geometry.location.lng()))
                        // On ending request for one query
                        if (i + 1 === results.length) {
                            if (pagination.hasNextPage) {
                                pagination.nextPage();
                            }

                            if (!pagination.hasNextPage) {
                                // map.fitBounds(bounds)
                                controlText.innerHTML = self.translate.instant('common.SEARCH_POI');
                                self.isLoading = false;
                                setTimeout(() => { self.markerCluster = new MarkerClusterer(map, self.markers, self.mcOptions);}, 500);
                            }
                        }
                    }

                } else {
                    controlText.innerHTML = self.translate.instant('common.SEARCH_POI');
                }
            })
        });
    }

    clearMarkers() {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
    }

    async initMap(lat?: any, lng?: any) {
        let dataMap = this.getDataMap(); // get new map datas
        map = new google.maps.Map(this.mapElement.nativeElement, {
            center: { lat: lat ? lat : dataMap.lat, lng: lng ? lng : dataMap.lng },
            zoom: dataMap.zoom,
            streetViewControl: false,
            zoomControl: false,
            mapTypeControl: false,
            styles: [
                {
                    featureType: 'poi',
                    stylers: [{ visibility: 'off' }]
                },
                {
                    featureType: 'transit',
                    stylers: [{ visibility: 'off' }]
                }
            ]
        });


        let that = this;

        // Create the DIV to hold the control and call the CenterControl()
        // constructor passing in this DIV.
        var centerControlDiv: any = document.createElement('div');
        that.centerControl(centerControlDiv, map);

        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(centerControlDiv);

        this.createPositionMarker(this.pos, 'HOME');

        let isInit = false;
        google.maps.event.addListener(map, 'bounds_changed', function() {
            if(!isInit){
                infowindow = new google.maps.InfoWindow();
                var service = new google.maps.places.PlacesService(map);

                var icon = '<span class="map-icon map-icon-night-club"></span>';

                var image = {
                    path: mapIcons.shapes.MAP_PIN,
                    fillColor: '#820080',
                    fillOpacity: 1,
                    strokeColor: '',
                    strokeWeight: 0,
                    scale: 1
                };

                var bounds = new google.maps.LatLngBounds(map.getBounds().getSouthWest(), map.getBounds().getNorthEast());

                if (that.placeId) {
                    var service = new google.maps.places.PlacesService(map);
                    // that.geolocation.getCurrentPosition().then(res => {
                    //     if (res) {
                    //         let coord = { lat: res.coords.latitude, lng: res.coords.longitude }
                    //         that.createPositionMarker(coord, 'GPS');
                    //     }
                    // }).catch(() => {
                    //     that.createPositionMarker(this.pos, 'HOME');
                    // })
                    that.geolocation.watchPosition().toPromise().then(res => {
                        if (res) {
                            let coord = { lat: res.coords.latitude, lng: res.coords.longitude }
                            that.createPositionMarker(coord, 'GPS');
                        }
                    }).catch(() => {
                        that.createPositionMarker(this.pos, 'HOME');
                    })

                    service.getDetails({
                        placeId: that.placeId
                    }, async function(result) {
                        that.selectedPlace = result
                        that.setAddress(result);
                        if (that.route.snapshot.paramMap.get('placeId') === null) {
                            await service.nearbySearch({
                                //location: { lat: lat ? lat : await result.geometry.location.lat(), lng: lng ? lng : await result.geometry.location.lng() },
                                //radius: that.getDistanceM(await map.getBounds().getNorthEast().lat(), await map.getBounds().getNorthEast().lng(), await map.getBounds().getSouthWest().lat(), await map.getBounds().getSouthWest().lng()),
                                bounds: bounds,
                                types: ['restaurant', 'bar', 'cafe']
                            }, (results, status, pagination) => {
                                if (status === google.maps.places.PlacesServiceStatus.OK) {
                                    for (var i = 0; i < results.length; i++) {
                                        that.createMarker(results[i]);

                                        // On ending request for one query
                                        if (i + 1 === results.length) {
                                            if (pagination.hasNextPage) {
                                                pagination.nextPage();
                                            }

                                            if (!pagination.hasNextPage) {
                                                // map.fitBounds(bounds)
                                                setTimeout(() => { that.markerCluster = new MarkerClusterer(map, that.markers, that.mcOptions);}, 500);
                                            }
                                        }
                                    }
                                }
                            });
                        } else {
                            that.createMarker(result)
                        }
                        let newCenter = { lat: lat ? lat : await result.geometry.location.lat(), lng: lng ? lng : await result.geometry.location.lng() }
                        await map.setCenter(newCenter);
                        await map.setZoom(Config.DEFAULT_ZOOM_HIGH);
                    });
                } else {
                    service.nearbySearch({
                        //location: { lat: lat ? lat : dataMap.lat, lng: lng ? lng : dataMap.lng },
                        //radius: that.getDistanceM(await map.getBounds().getNorthEast().lat(), await map.getBounds().getNorthEast().lng(), await map.getBounds().getSouthWest().lat(), await map.getBounds().getSouthWest().lng()),
                        bounds: bounds,
                        types: ['restaurant', 'bar', 'cafe']
                    }, (results, status, pagination) => {
                        if (status !== 'OK') return;
                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                            for (var i = 1; i < results.length; i++) {
                                that.createMarker(results[i]);
                                // On ending request for one query
                                if (i + 1 === results.length) {
                                    if (pagination.hasNextPage) {
                                        pagination.nextPage();
                                    }

                                    if (!pagination.hasNextPage) {
                                        // map.fitBounds(bounds)
                                        setTimeout(() => { that.markerCluster = new MarkerClusterer(map, that.markers, that.mcOptions);}, 500);
                                    }
                                }
                            }
                        }
                    })
                }
            }
            isInit = true;
        });
    }

    checkFavoriteFromPoi(place) {
        if (typeof this.favoriteList !== 'undefined') {
            if (this.favoriteList.length != 0) {
                for (let i = 0; i < this.favoriteList.length; i++) {
                    if (place.reference === this.favoriteList[i].id) {
                        return true;
                    }
                }
                return false;
            }
        }
    }

    async createPositionMarker(place, type) {
        if (this.markersPositions != 0) {
            let j = 0;
            for (let i = 0; i < this.markersPositions.length; i++) {
                j++;
                this.markersPositions[i].setMap(null);
                if (j === i) {
                    this.markersPositions = [];
                }
            }
        }
        let pos;
        if (place.lat && place.lng) {
            pos = { lat: place.lat, lng: place.lng }
        }
        var image = {
            path: mapIcons.shapes.MAP_PIN,
            fillColor: '#F400FE',
            fillOpacity: 1,
            strokeColor: '',
            strokeWeight: 0,
            scale: 1
        };

        switch (type) {
            case 'HOME':
                var marker = new mapIcons.Marker({
                    map: map,
                    position: pos,
                    zIndex: 9,
                    icon: image,
                    map_icon_label: '<span class="map-icon map-icon-insurance-agency"></span>'
                });

                this.markersPositions.push(marker)
                break;
            case 'GPS':
                var marker = new mapIcons.Marker({
                    map: map,
                    position: pos,
                    zIndex: 9,
                    icon: image,
                    map_icon_label: '<span class="map-icon map-icon-postal-code"></span>'
                });

                this.markersPositions.push(marker)
                break;
        }
    };

    async createMarker(place) {
        let that = this;
        var icon = this.getIcon(place);
        var placeLoc = place.geometry.location;
        var btn = '';
        var bounds = new google.maps.LatLngBounds();

        var isFavorite = this.checkFavoriteFromPoi(place);

        var markerIcon;
        var image = {
            path: mapIcons.shapes.MAP_PIN,
            fillColor: isFavorite ? '#F400FE' : '#820080',
            fillOpacity: 1,
            strokeColor: '',
            strokeWeight: 0,
            scale: 1
        };

        var marker;
        if(this.isService || isFavorite){
            marker = new mapIcons.Marker({
                map: map,
                position: placeLoc,
                zIndex: 9,
                icon: image,
                map_icon_label: icon
            });
        } else {
            marker = new google.maps.Marker({
                map: map,
                position: placeLoc,
                zIndex: 9,
                icon: icon,
            });
        }
       

        this.checkFavoriteFromPoi(place)

        this.markers.push(marker);

        await google.maps.event.addListener(marker, 'click', function() {
            if (that.favoriteList.length != 0) {
                for (let i = 0; i < that.favoriteList.length; i++) {
                    if (place.place_id === that.favoriteList[i].id) {
                        btn = '<li style="margin: 0 0 .6rem 0;"><a href="#" class="ion-no-padding ion-button" size="small" fill="clear" id="addButton_' + place.place_id + '"disabled style="font-weight: 400; text-decoration: none;">';
                        break;
                    } else {
                        btn = '<li style="margin: 0 0 .6rem 0;"><a href="#" class="ion-no-padding ion-button" size="small" fill="clear" id="addButton_' + place.place_id + '" style="font-weight: 400; text-decoration: none;">';
                        continue;
                    }
                }
            }
            else {
                btn = '<li style="margin: 0 0 .6rem 0;"><ion-button class="ion-no-padding" size="small" fill="clear" id="addButton_' + place.place_id + '" style="height: 1.6em; font-weight: 400; margin-top: -7px; margin-left: 0; margin-bottom: 0;">';
            }

            function round(value, precision) {
                var multiplier = Math.pow(10, precision || 0);
                return Math.round(value * multiplier) / multiplier;
            }

            if (place.price_level) {
                var priceLevel;
                switch (place.price_level) {
                    case 0:
                        priceLevel = "€";
                        break;
                    case 1:
                        priceLevel = "€";
                        break;
                    case 2:
                        priceLevel = "€€";
                        break;
                    case 3:
                        priceLevel = "€€€";
                        break;
                    case 5:
                        priceLevel = "€€€€";
                        break;
                    default:
                        priceLevel = "€";
                        break;
                }
            }

            let ratingHtml = "";
            if(place.rating){
                ratingHtml = that.translate.instant('map.RATING') + round(place.rating, 1).toFixed(1) + ' / 5  ' + '(' + place.user_ratings_total +
                    that.translate.instant('map.RATING_NUMBER') + ')' + " &bull; ";
                if(place.price_level){
                    ratingHtml += priceLevel;
                }
            }

                var infowindowContent =
                    '<div class="info-window-content" style="min-width: 185px; min-height: 80px; padding: 0 10px 0 0;">' +
                    '<strong style="display: inline-block; margin: 2px 0 6px;">' + place.name + '</strong><br>' +
                    place.vicinity + '<br>' +
                    ratingHtml +
                    '<ul class="info-window--list-actions" style="list-style: none; margin: .75em 0; padding: 0;"><li style="margin: 0 0 .6rem 0;"><ion-text><a style="font-weight: 400; text-decoration: none" class="font-size-0_8" target="_blank" href="https://www.google.com/maps/search/?api=1&query=' + place.name + '&query_place_id=' + place.place_id + '">'
                    + 'Voir sur Google' + '</a></ion-text></li>' +
                    btn +
                    that.translate.instant('map.ADD_FAVORITE') +
                    '</a></li>' + 
                    '<li style="margin: 0 0 .6rem 0;"><a class="ion-no-margin ion-no-padding ion-button" size="small" fill="clear" color="primary" id="createButton_' + place.place_id + '" style="font-weight: 400; text-decoration: none;">' + that.translate.instant('map.CREATE_EVENT') + '</a></li>' +
                    '</ul></div>';

            infowindow.setContent(infowindowContent);
            infowindow.open(map, this);


            google.maps.event.addListenerOnce(infowindow, 'domready', () => {

                document.getElementById('addButton_' + place.place_id).addEventListener("click", async (e) => {
                    e.preventDefault();

                    placeInfo = {
                        docId: null,
                        id: await place.place_id,
                        address: await place.vicinity,
                        name: await place.name,
                        lat: await place.geometry.location.lat(),
                        lng: await place.geometry.location.lng(),
                        types: await place.types,
                        tags: []
                    }
                    await that.editFavorite(placeInfo, 'create');
                    infowindow.close()
                });
            });

            google.maps.event.addListenerOnce(infowindow, 'domready', () => {

                document.getElementById('createButton_' + place.place_id).addEventListener("click", () => {

                    that.ngZone.run(() => { that.router.navigate(['tabs/add-event', place.place_id]) });
                });
            });
        })
    }

    getIcon(place) {
        var types = place.types;
        var iconBase = '../../../assets/images/icon-upviewsly.svg';

        var typeRestaurant = (types.indexOf("restaurant") > -1);
        var typeBar = (types.indexOf("bar") > -1);
        var typeCafe = (types.indexOf("cafe") > -1);


        if (typeBar) {
            iconBase = '<span class="map-icon map-icon-night-club"></span>';
            this.isService = true;
        }
        else if (typeRestaurant) {
            iconBase = '<span class="map-icon map-icon-restaurant"></span>';
            this.isService = true;
        }
        else if (typeCafe) {
            iconBase = '<span class="map-icon map-icon-cafe"></span>';
            this.isService = true;
        } else {
            this.isService = false;
        }
        return iconBase;
    }

    // get radius size
    getRadius(mapViewBox) {

        return this.getDistanceM(mapViewBox[0].latSw, mapViewBox[0].lngSw, mapViewBox[1].latNe, mapViewBox[1].lngNe);
    }

    // zoom definition
    getZoom(mapViewBox) {

        var GLOBE_WIDTH = 256; // a constant in Google's map projection
        var PIXEL_SCREEN = 300; // constant mid size smartphone display
        var west = mapViewBox[0].lngSw;
        var east = mapViewBox[1].lngNe;
        var angle = east - west;
        if (angle < 0) {
            angle += 360;
        }
        return Math.round(Math.log(PIXEL_SCREEN * 360 / angle / GLOBE_WIDTH) / Math.LN2);
    }

    deg2rad(x) {
        return Math.PI * x / 180;
    }

    // Radius definition
    getDistanceM($lat1, $lng1, $lat2, $lng2) {
        let $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
        let $rlo1 = this.deg2rad($lng1);    // CONVERSION
        let $rla1 = this.deg2rad($lat1);
        let $rlo2 = this.deg2rad($lng2);
        let $rla2 = this.deg2rad($lat2);
        let $dlo = ($rlo2 - $rlo1) / 2;
        let $dla = ($rla2 - $rla1) / 2;
        let $a = (Math.sin($dla) * Math.sin($dla)) + Math.cos($rla1) * Math.cos($rla2) * (Math.sin($dlo) * Math.sin($dlo));
        let $d = 2 * Math.atan2(Math.sqrt($a), Math.sqrt(1 - $a));
        return ($earth_radius * $d);
    }

    // ******** SEGMENT ********** //

    // Segment handling
    segmentChanged(ev: any) {

        if (this.menuIcon == "favorite") {
            this.getAllFavorite();
            document.getElementById('map').hidden = true;
            document.getElementById('favorite').hidden = false;
            document.getElementById('search').hidden = true;
        }
        else {
            document.getElementById('map').hidden = false;
            document.getElementById('favorite').hidden = true;
            document.getElementById('search').hidden = false;
        }
    }
}
