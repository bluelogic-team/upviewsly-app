import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../providers/auth/auth.service';
import { Platform, ModalController, ActionSheetController, ToastController, LoadingController } from '@ionic/angular';
import { TranslateService } from "@ngx-translate/core";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { OnboardingFriendPage } from '../onboarding-friend/onboarding-friend.page';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';

@Component({
  selector: 'app-onboarding-add-image',
  templateUrl: './onboarding-add-image.page.html',
  styleUrls: ['./onboarding-add-image.page.scss'],
})
export class OnboardingAddImagePage implements OnInit {

    imageURL: any;
    public base64Image;
    public imageChanged = false;

  constructor(public authService: AuthService, public platform: Platform,
        public actionSheetCtrl: ActionSheetController, private modalCtrl: ModalController,
        private translate: TranslateService, private camera: Camera, private db: AngularFirestore,
        private toastCtrl: ToastController, private loadingCtrl: LoadingController) { }

  ngOnInit() {
  }

  async editPicture() {
        var me = this;
        if (this.platform.is('cordova')) {
            const actionSheet = await this.actionSheetCtrl.create({
                header: this.translate.instant('common.EDIT_PICTURE_HEADER'),
                buttons: [
                    {
                        text: this.translate.instant('common.EDIT_PICTURE_PHOTO'),
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.CAMERA)
                        }
                    },
                    {
                        text: this.translate.instant('common.EDIT_PICTURE_GALLERY'),
                        handler: () => {
                            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY)
                        }
                    },
                    {
                        text: this.translate.instant('common.CANCEL'),
                        role: 'cancel',
                        handler: () => {
                        }
                    }]
            });
            await actionSheet.present();
        }
        else {
            let input = document.createElement('input');
            input.type = 'file';
            input.click();
            var file;
            var reader = new FileReader();

            input.onchange = function() {
                file = input.files[0];
                if (file) {
                    reader.readAsDataURL(file);
                }
            };
            reader.onload = function() {
                me.base64Image = reader.result;
                me.imageChanged = true;
            };
        }
    }

    takePicture(sourceType) {
        const options: CameraOptions = {
            quality: 60,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: sourceType,
            correctOrientation: true,
        }

        this.camera.getPicture(options).then((imageData) => {
            this.base64Image = "data:image/jpeg;base64," + imageData;
            this.imageChanged = true;
        }, (err) => {
            throw new err;
        });
    }

    async forward() {
    	if (this.imageChanged) {
    		const loading = await this.loadingCtrl.create({
            	message: this.translate.instant('common.LOADING')
        	});
        	await loading.present();
            const picture = firebase.storage().ref('users/avatars/' + 'user_avatar_' + this.authService.getCurrentUserId());
            await picture.putString(this.base64Image, 'data_url');
            let downloadUrl = await picture.getDownloadURL();
            this.imageURL = downloadUrl;

            firebase.firestore().collection('users').doc(this.authService.getCurrentUserId()).update({ "imageURL": this.imageURL }).then(async () => {
                loading.dismiss();
                this.modalCtrl.dismiss();
                let modal = await this.modalCtrl.create({
                    component: OnboardingFriendPage,
                    backdropDismiss: false
                });
                await modal.present();
            });
        } else {
            let toast = await this.toastCtrl.create({ 
                message: this.translate.instant('onboarding.INVALID_IMAGE'),
                duration: 3000
            });
            await toast.present();
        }
    }

}
