import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NotificationsService } from '../../providers/notifications/notifications.service';
import { AuthService } from '../../providers/auth/auth.service';
import { EventService } from '../../providers/event/event.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { database } from 'firebase';
import { ToastController } from '@ionic/angular';

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.page.html',
    styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

    notifications: any;

    month = [
        this.translate.instant('dates.JANUARY'), this.translate.instant('dates.FEBRUARY'),
        this.translate.instant('dates.MARCH'), this.translate.instant('dates.APRIL'),
        this.translate.instant('dates.MAY'), this.translate.instant('dates.JUNE'),
        this.translate.instant('dates.JULY'), this.translate.instant('dates.AUGUST'),
        this.translate.instant('dates.SEPTEMBER'), this.translate.instant('dates.OCTOBER'),
        this.translate.instant('dates.NOVEMBER'), this.translate.instant('dates.DECEMBER')
    ];

    constructor(
        private _location: Location,
        private notificationService: NotificationsService,
        private eventService: EventService,
        private router: Router,
        private translate: TranslateService,
        public toastController: ToastController
    ) { }

    ngOnInit() {
    }

    async ionViewWillEnter() {

        await this.getUserNotifications(true).then(() => {
             
        })
    }

    async getUserNotifications(clear) {
        await this.notificationService.getUserNotifications().subscribe(data => {
            let notif = data.map(e => {
                const data = e.payload.doc.data();
                const id = e.payload.doc.id;

                return { id, ...data };
            });
            this.notifications = notif;
            if(clear){
                this.notifications.forEach(async notif => {
                    if (notif.isRead == false) {
                        this.updateNotification(notif.id, notif);
                    }
                });
            }
            return this.notifications;
          });
    }

    async showNotification(eventId, notifDocId, notification) {
            if (eventId != null) {
                this.eventService.getOneEvent(eventId).subscribe(async data => {
                    if (data.payload.data()) {
                        return this.router.navigate([notification.link, eventId]);
                    }
                    else {
                        await this.presentToast();
                    }
                })
            }
            else {
                return this.router.navigate([notification.link]);
            }
    }

    async updateNotification(notifDocId, notification) {

        notification.isRead = true;
        notification.createdAt = notification.createdAt;

        await this.notificationService.updateNotification(notifDocId, notification);
    }

    async removeNotification(docId) {

        await this.notificationService.deleteNotification(docId).then(() => {
            this.getUserNotifications(false);
        })
    }

    async presentToast() {

        let msg = this.translate.instant('notifications.NO_EVENT');

        const toast = await this.toastController.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    /*
      removeAllNotifications(){
        this.notificationService.removeAllNotifications();
      }
    */

    back() {
        this._location.back();
    }
}
