import { Component } from '@angular/core';
import { ChatService } from 'src/app/providers/chat/chat.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(public chatService: ChatService) {
  }

}
