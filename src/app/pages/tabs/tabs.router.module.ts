import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'transport-details/:eventId/:transportId/:userStatus',
        children: [
          {
            path: '',
            loadChildren: '../transport-details/transport-details.module#TransportDetailsPageModule'
          }
        ]
      },
      {
        path: 'add-transport/:eventId/:date',
        children: [
          {
            path: '',
            loadChildren: '../add-transport/add-transport.module#AddTransportPageModule'
          }
        ]
      },
      {
        path: 'notifications',
        children: [
          {
            path: '',
            loadChildren: '../notifications/notifications.module#NotificationsPageModule'
          }
        ]
      },
      {
        path: 'add-event/:placeId/:type/:docId',
        children: [
          {
            path: '',
            loadChildren: '../add-event/add-event.module#AddEventPageModule'
          }
        ]
      },
      {
        path: 'add-event/:placeId',
        children: [
          {
            path: '',
            loadChildren: '../add-event/add-event.module#AddEventPageModule'
          }
        ]
      },
      {
        path: 'add-event',
        children: [
          {
            path: '',
            loadChildren: '../add-event/add-event.module#AddEventPageModule'
          }
        ]
      },
      {
        path: 'event-details/:eventId/:notification',
        children: [
          {
            path: '',
            loadChildren: '../event-details/event-details.module#EventDetailsPageModule'
          }
        ]
      },
      {
        path: 'event-details/:eventId',
        children: [
          {
            path: '',
            loadChildren: '../event-details/event-details.module#EventDetailsPageModule'
          }
        ]
      },
      {
        path: 'event-details',
        children: [
          {
            path: '',
            loadChildren: '../event-details/event-details.module#EventDetailsPageModule'
          }
        ]
      },
      {
        path: 'events',
        children: [
          {
            path: '',
            loadChildren: '../events/events.module#EventsPageModule'
          }
        ]
      },
      {
        path: 'map',
        children: [
          {
            path: '',
            loadChildren: '../map/map.module#MapPageModule'
          }
        ]
      },
      {
        path: 'map/:placeId',
        children: [
          {
            path: '',
            loadChildren: '../map/map.module#MapPageModule'
          }
        ]
      },
      {
        path: 'chat-list',
        children: [
          {
            path: '',
            loadChildren: '../chat-list/chat-list.module#ChatListPageModule'
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: '../profile/profile.module#ProfilePageModule'
          }
        ]
      },
      {
        path: 'friends',
        children: [
          {
            path: '',
            loadChildren: '../friends/friends.module#FriendsPageModule'
          }
        ]
      },      
      {
        path: '',
        redirectTo: 'events',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'events',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
