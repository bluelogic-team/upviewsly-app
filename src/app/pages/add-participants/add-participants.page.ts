import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, LoadingController, AlertController } from '@ionic/angular';
import { TranslateService } from "@ngx-translate/core";
import { GroupService } from "../../providers/group/group.service";
import { map } from 'rxjs/operators';
import { FriendsService } from '../../providers/friends/friends.service';
import { AuthService, User } from '../../providers/auth/auth.service';
import { AddFriendsPage } from '../add-friends/add-friends.page';

@Component({
    selector: 'app-add-participants',
    templateUrl: './add-participants.page.html',
    styleUrls: ['./add-participants.page.scss'],
})
export class AddParticipantsPage implements OnInit {

    public displayType = "friends";
    groupList: any;
    participants = [];
    eventParticipants = this.navParams.get('eventParticipants');
    alreadyParticipants: boolean = false;
    type: any;
    chats: any;
    public displayedFriends;
    private selectedUsers = [];
    private friends;
    public searchUserString: any;

    constructor(
        private modalCtrl: ModalController,
        private translate: TranslateService,
        private groupService: GroupService,
        private authService: AuthService,
        private friendsService: FriendsService,
        private loadingController: LoadingController,
        private navParams: NavParams,
        private alertCtrl: AlertController
    ) { }

    ngOnInit() {
    }

    async ionViewDidEnter() {
        this.type = await this.navParams.get('type');
        this.chats = await this.navParams.get('chats');
       

        if (this.groupList == undefined || this.groupList.length == 0) {
            let user = await this.authService.getCurrentUser();

            await this.loadGroups(user);
        }
        await this.loadFriends();
    }

    isAlreadyParticipant(friend: any) {
        let result = this.eventParticipants.find(p => {
            return p.uid == friend.uid;
        })

        return result;
    }

    async loadFriends() {
        let tmpFriends = [];
        let friends = [];
        await this.friendsService.getUserFriends().then(async data => {
            data.forEach(function(doc) {
                friends.push({docId : doc.id, uid: doc.data()['uid']});
            });

            let that = this;
            friends.forEach(function(friend, i){
                if (typeof that.eventParticipants !== 'undefined' && that.eventParticipants) {
                        that.authService.getUserInfo(friend.uid).then(res => {
                            let user = Object.assign(res, { firebaseId: friend.uid });
                            if (!that.isAlreadyParticipant(friend)) {
                                that.alreadyParticipants = false;
                                tmpFriends.push(user);
                            } else {
                                that.alreadyParticipants = true;
                            }
                            if(i == friends.length-1){
                                tmpFriends.sort(function(a, b){
                                    if(a.firstName < b.firstName) { return -1; }
                                    if(a.firstName > b.firstName) { return 1; }
                                    return 0;
                                })
                                that.displayedFriends = tmpFriends;
                                that.friends = tmpFriends;
                            }
                        });
                } else {
                    that.alreadyParticipants = false;
                    that.authService.getUserInfo(friend.uid).then(res => {
                        let user = Object.assign(res, { firebaseId: friend.uid });
                        tmpFriends.push(user);
                        if(i == friends.length-1){
                                tmpFriends.sort(function(a, b){
                                    if(a.firstName < b.firstName) { return -1; }
                                    if(a.firstName > b.firstName) { return 1; }
                                    return 0;
                                })
                                that.displayedFriends = tmpFriends;
                                that.friends = tmpFriends;
                            }
                    });
                }
            });
        });
    }

    async loadGroups(user) {
        let groupCollection = await this.groupService.getUserGroups(user.uid);
        let pushable = true;

        let obsGroup = groupCollection.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            })
        )
        obsGroup.subscribe(async (groups) => {
            this.groupList = groups;
        });
    }

    async addParticipants(selectedGroup) {
        this.selectedUsers = [];
        // if(!this.type) {
        //     const alert = await this.alertCtrl.create({
        //         message: this.translate.instant("add-event.GROUP_CONFIRM"),
        //         buttons: [
        //             {
        //                 text: this.translate.instant("common.CANCEL"),
        //                 role: "cancel",
        //                 cssClass: "secondary",
        //                 handler: () => {
        //                     console.log('Confirm cancel');
        //                 }
        //             }, {
        //                 text: this.translate.instant("common.VALIDATE"),
        //                 handler: async () => {
        //                     await selectedGroup.members.forEach(async element => {
        //                         let user = Object.assign(element, { firebaseId: element });
        //                         this.selectedUsers.push(user);
        //                     });
        //                     this.modalCtrl.dismiss({
        //                         members: this.selectedUsers,
        //                         groupName: selectedGroup.name,
        //                         groupId: selectedGroup.id
        //                     });
        //                 }
        //             }
        //         ]
        //     })
    
        //     await alert.present();
        // } else {
            await selectedGroup.members.forEach(async element => {
                let user = Object.assign(element, { firebaseId: element });
                this.selectedUsers.push(user);
            });
            
            this.modalCtrl.dismiss({
                members: this.selectedUsers,
                groupName: selectedGroup.name,
                groupId: selectedGroup.id
            });
        // }
    }

    cancel() {

        this.modalCtrl.dismiss();
    }

    // Check or uncheck friend
    toggleUser(user) {
        if (!user.checked) {
            //contact.checked = true;
            this.selectedUsers.push(user);
            this.participants = this.selectedUsers;
        } else {
            let index = this.selectedUsers.indexOf(user);
            if (index > -1) {
                this.selectedUsers.splice(index, 1);
                this.participants = this.selectedUsers;
                //contact.checked = false;
            }
        }

    }

    async validateModal() {

        if (this.type == 'chat') {

            let members = [];

            this.selectedUsers.forEach(element => {
                members.push(element.firebaseId);
            });

            await this.modalCtrl.dismiss(members);
        }
        else {
            await this.modalCtrl.dismiss(this.selectedUsers);
        }
    }

    async presentAddFriends() {
        const modal = await this.modalCtrl.create({
            component: AddFriendsPage
        });
        await modal.present();
    }

    searchFriend(ev: any) {
        // set val to the value of the searchbar
        const val = ev.detail.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.displayedFriends = this.friends.filter((item) => {
                if (item.displayName) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                else if (item.firstName) {
                    return (item.firstName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                else {
                    return (item.lastName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
            })
        } else {
            this.displayedFriends = this.friends;
        }
    }

}
