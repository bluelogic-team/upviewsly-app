import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, NavParams } from '@ionic/angular';
import { TranslateService } from "@ngx-translate/core";
import { FriendsService } from '../../providers/friends/friends.service';
import { AuthService, User } from '../../providers/auth/auth.service';
import { AddFriendsPage } from '../add-friends/add-friends.page';



@Component({
    selector: 'app-select-friends',
    templateUrl: './select-friends.page.html',
    styleUrls: ['./select-friends.page.scss'],
})
export class SelectFriendsPage implements OnInit {

    public displayedFriends;
    private selectedUsers = [];
    private participants: [];
    public alreadyParticipants: boolean = false;
    constructor(public modalCtrl: ModalController,
        public translate: TranslateService,
        public friendsService: FriendsService,
        public authService: AuthService,
        private navCtrl: NavParams) { }

    async ngOnInit() {
        this.participants = this.navCtrl.get('participants');
        let friends = [];
        let tmpFriends = [];
        this.friendsService.getUserFriends().then(data => {
            data.forEach(function(doc) {
            // doc.data() is never undefined for query doc snapshots
                friends.push({docId : doc.id, uid: doc.data()['uid']});
            });
            let that = this;
            friends.forEach(function(friend, i) {
                if (typeof that.participants !== 'undefined' && that.participants) {
                    that.authService.getUserInfo(friend.uid).then(res => {
                        let user = Object.assign(res, { firebaseId: friend.uid });

                        if (!that.isAlreadyParticipant(friend)) {
                            that.alreadyParticipants = false;
                            tmpFriends.push(user);
                        } else {
                            that.alreadyParticipants = true;
                        }
                        if(i == friends.length - 1){
                            tmpFriends.sort(function(a, b){
                                if(a.firstName < b.firstName) { return -1; }
                                if(a.firstName > b.firstName) { return 1; }
                                return 0;
                            })
                            that.displayedFriends = tmpFriends;
                        }
                    });
                } else {
                    that.alreadyParticipants = false;
                    that.authService.getUserInfo(friend.uid).then(res => {
                        let user = Object.assign(res, { firebaseId: friend.uid });
                        tmpFriends.push(user);
                    });
                }
            });
            this.displayedFriends = tmpFriends;
        });
    }

    isAlreadyParticipant(friend: any) {
        let result = this.participants.find((p: any) => {
            return p.firebaseId == friend.uid;
        })

        return result;
    }

    closeModal() {
        this.modalCtrl.dismiss(false);
    }

    async presentAddFriends() {
        const modal = await this.modalCtrl.create({
            component: AddFriendsPage
        });
        await modal.present();
    }

    // Check or uncheck friend
    toggleUser(user) {
        if (!user.checked) {
            //contact.checked = true;
            this.selectedUsers.push(user);
        } else {
            let index = this.selectedUsers.indexOf(user);
            if (index > -1) {
                this.selectedUsers.splice(index, 1);
                //contact.checked = false;
            }
        }
    }

    validateModal() {
        this.modalCtrl.dismiss(this.selectedUsers);
    }

    searchFriend(ev: any) {
        // set val to the value of the searchbar
        const val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.displayedFriends = this.displayedFriends.filter((item) => {
                if (item.displayName) {
                    return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                else if (item.firstName) {
                    return (item.firstName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
                else {
                    return (item.lastName.toLowerCase().indexOf(val.toLowerCase()) > -1);
                }
            })
        } else {
            this.ngOnInit();
        }
    }

}
