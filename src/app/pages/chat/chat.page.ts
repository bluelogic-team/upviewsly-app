import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ChatService } from '../../providers/chat/chat.service';
import { AuthService } from '../../providers/auth/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { AddLocationPage } from '../add-location/add-location.page';
import { AddDatePage } from '../add-date/add-date.page';
import { ModalController, Platform, AlertController } from '@ionic/angular';
import { UserProfilePage } from '../user-profile/user-profile.page';
import { FcmService } from 'src/app/providers/fcm/fcm.service';
import { NotificationsService } from "../../providers/notifications/notifications.service";

@Component({
    selector: 'app-chat',
    templateUrl: './chat.page.html',
    styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

    @ViewChild('scrollMe') private myScrollContainer: ElementRef;

    chat$: Observable<any>;
    chatId: string;
    chatLength: number;
    newMsg: string;
    chat: any;
    uid: any;

    baseEventDate: any;
    baseEventHour: any;
    eventDate: any;
    participants = new Array();
    chatsObservable: any;

    // Options to format date
    options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

    constructor(
        public chatService: ChatService,
        private route: ActivatedRoute,
        private translate: TranslateService,
        private modalCtrl: ModalController,
        private authService: AuthService,
        private platform: Platform,
        private alertCtrl: AlertController,
        private router: Router,
        private fcmService: FcmService,
        public notifService: NotificationsService,
    ) { }

    ngOnInit() {
        this.platform.pause.subscribe(() => {
            this.updateTimeStampFromUsers();
        })
    }

    async showUser(userId: string) {
        let modal = await this.modalCtrl.create({
            component: UserProfilePage,
            componentProps: {
                userId: userId
            }
        });
        modal.present();
    }

    async ionViewWillEnter() {
        this.chatId = this.route.snapshot.paramMap.get('id');
        this.uid = this.authService.getCurrentUserId();
        const source = this.chatService.getMessages(this.chatId, false);

        this.chatService.getChat(this.chatId).subscribe(async e => {
            if (e.payload.data()['eventDate']) {
                var date = new Date(e.payload.data()['eventDate']);
                var hour = new Date(e.payload.data()['eventHour']);
                this.baseEventDate = e.payload.data()['eventDate'];
                this.baseEventHour = e.payload.data()['eventHour'];

                this.eventDate = {
                    hour: hour.getHours() + ':' + (hour.getMinutes() < 10 ? '0' + hour.getMinutes() : hour.getMinutes()),
                    date: new Date(date).toLocaleDateString('fr-FR', this.options)
                }
            } else {
                this.eventDate = {
                    hour: null,
                    date: null
                }
            }

            this.chat = {
                title: e.payload.data()['title'],
                hour: this.eventDate.hour,
                date: this.eventDate.date,
                createdAt: e.payload.data()['createdAt'],
                eventId: e.payload.data()['eventId'],
                participants: e.payload.data()['participants'],
                lastViewed: e.payload.data()['lastViewed'],
                groupId: e.payload.data()['groupId']
            }

        });

        this.chat$ = this.chatService.joinUsers(source);

        this.updateTimeStampFromUsers();
        this.chatsObservable = this.chat$.subscribe(
            res => {
                if (res) {
                    this.scrollToBottom();
                }
            }
        )

        if (typeof this.chat !== 'undefined') {
            this.scrollToBottom();
        }
    }

    updateTimeStampFromUsers() {
        // Update timestamp from current user
        this.chatService.updateLastViewed(this.chatId);
    }

    ionViewWillLeave() {
        this.updateTimeStampFromUsers();
        this.chatsObservable && this.chatsObservable.unsubscribe();
    }

    ionViewDidEnter() {
        this.notifService.updateNotificationMessagesIsRead(this.chatId, this.authService.getCurrentUserId());
    }

    async exitDiscution() {
        let alert = await this.alertCtrl.create({
            message: this.translate.instant('chat.EXIT'),
            backdropDismiss: false,
            buttons: [{
                text: this.translate.instant('common.NO'),
                role: 'cancel'
            }, {
                text: this.translate.instant('common.YES'),
                handler: async () => {
                    this.chat.participants;
                    let arr_participants = [];
                    // Delete me from user-participants
                    // let index = this.chat.participants.findIndex(async (u, i) => {
                    //     if (this.authService.getCurrentUserId() === u) {
                    //     }
                    // })

                    await this.chat.participants.forEach(p => {
                        if(this.authService.getCurrentUserId() !== p) {
                            arr_participants.push(p);
                        }
                    });

                    // Update chat here !
                    setTimeout(() => {
                        this.chatService.updateChatParticipants(this.chatId, arr_participants);
                        this.chatService.removeLastViewed(this.chatId);
                        this.router.navigate(['/tabs/chat-list'])
                    }, 500)
                }
            }]
        });

        await alert.present();
    }

    submit() {
        if (this.newMsg && this.newMsg.trim() !== '') {
            this.chatService.sendMessage(this.chatId, this.newMsg)
                .then(() => {
                    let arrayDevice = new Array();
                    this.authService.getUserInfo(this.authService.getCurrentUserId()).then(
                        sender => {
                            let title = this.translate.instant("notifications.NEW_MESSAGE") + sender.firstName;
                            let body = this.newMsg;
                            let link = '/chat/';
                            let chatId = this.chatId;
                            this.newMsg = "";
                            this.chat.participants.forEach((participant,i) => {
                                if(participant != this.authService.getCurrentUserId()){
                                    this.notifService.createNewNotification(participant, title, body, link, this.chat.eventId, true, chatId);
                                    this.fcmService.getDeviceInfo(participant).then(
                                        (deviceToken: any) => {
                                            this.fcmService.sendPushNotification(title,body,
                                                deviceToken.data()['deviceToken'],participant,
                                                    { payload: { type: 'message', link: link, linkData: chatId } });
                                        });  
                                }
                            });
                    
                    });
                })
        } else {
            return;
        }
    }

    trackByCreated(i, msg) {
        return msg.createdAt;
    }

    async scrollToBottom() {
        try {
            // Hack to scroll to bottom with interval to 1 second and clear this after passed time
            let _scroll = setInterval(() => this.myScrollContainer.nativeElement.scrollIntoView(false), 200)
            setTimeout(() => { clearInterval(_scroll) }, 1000)
        } catch (err) { }
    }

    async addLocation() {

        let msg;

        let modalEdit = await this.modalCtrl.create({
            component: AddLocationPage,
        });
        await modalEdit.present();
        await modalEdit.onWillDismiss().then(async newLocation => {
            if (newLocation.data) {
                msg = await this.translate.instant('chat.LOCATION_PROPOSAL') + newLocation.data.address;
                await this.chatService.sendMessage(this.chatId, { msg: msg, type: 'location', placeId: newLocation.data.placeId })
                .then(() => {
                    let arrayDevice = new Array();
                    this.authService.getUserInfo(this.authService.getCurrentUserId()).then(
                        sender => {
                            let title = this.translate.instant("notifications.NEW_MESSAGE") + sender.firstName;
                            let body = msg;
                            let link = '/chat/';
                            let chatId = this.chatId;
                            this.chat.participants.forEach((participant,i) => {
                                if(participant != this.authService.getCurrentUserId()){
                                    this.notifService.createNewNotification(participant, title, body, link, this.chat.eventId, true, chatId);
                                    this.fcmService.getDeviceInfo(participant).then(
                                        (deviceToken: any) => {
                                            this.fcmService.sendPushNotification(title,body,
                                                deviceToken.data()['deviceToken'],participant,
                                                    { payload: { type: 'message', link: link, linkData: chatId } });
                                        });  
                                }
                            });
                    
                    });
                });
            }
        });
    }

    async addDate() {

        let msg;

        let modalEdit = await this.modalCtrl.create({
            component: AddDatePage,
            componentProps: {
                date: this.baseEventDate
            }
        });
        await modalEdit.present();
        await modalEdit.onWillDismiss().then(async newDate => {

            if (newDate.data) {

                let tempDate = new Date(newDate.data.date);

                let date = new Date(tempDate).toLocaleDateString('fr-FR', this.options);
                let hour = tempDate.getHours() + ':' + (tempDate.getMinutes() < 10 ? '0' + tempDate.getMinutes() : tempDate.getMinutes());

                msg = this.translate.instant('chat.DATE_PROPOSAL') + this.translate.instant('add-event.DATE_DISPLAY') +
                    date + this.translate.instant('add-event.HOUR_DISPLAY') + hour;

                this.chatService.sendMessage(this.chatId, msg)
                .then(() => {
                    let arrayDevice = new Array();
                    this.authService.getUserInfo(this.authService.getCurrentUserId()).then(
                        sender => {
                            let title = this.translate.instant("notifications.NEW_MESSAGE") + sender.firstName;
                            let body = msg;
                            let link = '/chat/';
                            let chatId = this.chatId;
                            this.chat.participants.forEach((participant,i) => {
                                if(participant != this.authService.getCurrentUserId()){
                                    this.notifService.createNewNotification(participant, title, body, link, this.chat.eventId, true, chatId);
                                    this.fcmService.getDeviceInfo(participant).then(
                                        (deviceToken: any) => {
                                            this.fcmService.sendPushNotification(title,body,
                                                deviceToken.data()['deviceToken'],participant,
                                                    { payload: { type: 'message', link: link, linkData: chatId } });
                                        });  
                                }
                            });
                    
                    });
                });
            }
        });
    }

    // Load more messages when user scroll over thoses messages
    loadMoreMessages(ev: any) {
        this.chatService.getMessages(this.chatId, true);
        setTimeout(() => { ev.target.complete() }, 1000);
    }
}