import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../providers/auth/auth.service';
import { EventService } from '../../providers/event/event.service';
import * as firebase from 'firebase';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastController, LoadingController, ModalController } from '@ionic/angular';
import { TranslateService } from "@ngx-translate/core";
import * as Config from '../../../utils/config';
import { ChatService } from 'src/app/providers/chat/chat.service';
import { NotificationsService } from 'src/app/providers/notifications/notifications.service';
import { FcmService } from 'src/app/providers/fcm/fcm.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { EditFavoritePage } from '../edit-favorite/edit-favorite.page';
import { UserProfilePage } from '../user-profile/user-profile.page';
import { FavoriteService } from 'src/app/providers/favorite/favorite.service';

declare var google;
let mapEvent: any;
declare var mapIcons;

@Component({
    selector: 'app-event-details',
    templateUrl: './event-details.page.html',
    styleUrls: ['./event-details.page.scss'],
})

export class EventDetailsPage implements OnInit {

    @ViewChild('mapEventDetails') mapElement: ElementRef;

    uid: any;
    event: any;
    place: any;
    eventId: any;
    eventLocation: any;
    chatId: any;
    beginDate: any;
    beginHour: any;
    participants: any;
    transportList: any;
    ownerName: any;
    userStatus: any;
    members: any;
    zoom: any;
    date: any;
    hour: any;
    pastEvent: boolean = false;
    pastEventChats: boolean = false;
    isService: boolean = false;
    favoriteList = [];
    // Options to format date
    options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

    constructor(
        private authService: AuthService,
        private eventService: EventService,
        private chatService: ChatService,
        private route: ActivatedRoute,
        private router: Router,
        private _location: Location,
        public toastController: ToastController,
        private translate: TranslateService,
        public loadingController: LoadingController,
        private notificationService: NotificationsService,
        private fcmService: FcmService,
        private launchNavigator: LaunchNavigator,
        private modalCtrl: ModalController,
        private favoriteService: FavoriteService
    ) { }

    ngOnInit() {
    }

    async ionViewWillEnter() {

        this.getAllFavorite();

        const loading = await this.loadingController.create({
            message: this.translate.instant('common.LOADING')
        });
        await loading.present();

        this.eventId = this.route.snapshot.paramMap.get('eventId');
        this.uid = this.authService.getCurrentUserId();

        this.transportList = await this.getUserTransport()

        await this.getEvent(this.eventId).then(res => {
            if (typeof res !== 'undefined') {
                this.chatService.getChatFromEvent(this.eventId).subscribe(
                    // return chat.id
                    (res: any) => {
                        res.forEach(el => {
                            el.id && (this.chatId = el.id);
                        });
                    }
                );
                this.event = res;
                this.participants = res.participants;
                this.date = res.date;
                let newDate = new Date(res.date)
                let dateNow = new Date();
                this.beginDate = new Date(newDate).toLocaleDateString('fr-FR', this.options);
                // Check date if date event is greather or equal than today + 1 hour
                if (newDate.getTime() + 3600000 >= dateNow.getTime()) {
                    this.pastEvent = false;
                } else {
                    this.pastEvent = true;
                }

                // Check date if date event is greather or equal than today + 1 day to hide chat icon
                if (newDate.getTime() + 86400000 >= dateNow.getTime()) {
                    this.pastEventChats = false;
                } else {
                    this.pastEventChats = true;
                }
            } else {
                return 'error'
            }
        }).then(async (res) => {
            if (res !== 'error') {
                await this.getUserParticipation();

                let finalGroup = [];

                await this.participants.forEach(async  element => {
                    await this.getParticipant(element).then(res => {
                        finalGroup.push(res)
                    });
                });
                this.members = finalGroup
                return true;
            }
        }).then(
            res => {
                if (typeof res !== 'undefined') {
                    this.initMap().then(() => {
                        loading.dismiss();
                    })
                } else {
                    loading.dismiss();
                    this.presentToast(this.translate.instant('notifications.NO_EVENT')).then(
                        res => {
                            this.router.navigate(['/tabs'])
                        }
                    );
                }
            }
        )
    }

    async showUser(user) {
        if (typeof user.uid !== 'undefined') {
            if(user.uid !== this.authService.getCurrentUserId()) {
                let modal = await this.modalCtrl.create({
                    component: UserProfilePage,
                    componentProps: {
                        userId: user.uid
                    }
                });
                modal.present();
            } else {
                this.router.navigate(['tabs/profile']);
            }
        }
    }

    async addFavorite() {
        let modal = await this.modalCtrl.create({
            component: EditFavoritePage,
            componentProps: {
                place: {
                    address: this.event.eventAddress,
                    name: this.place.name,
                    id: this.event.eventPlaceId
                },
                tagList: [],
                type: 'create'
            }
        });
        modal.present();
    }

    checkFavoriteFromPoi() {
        if (typeof this.favoriteList !== 'undefined') {
            if (this.favoriteList.length != 0) {
                for (let i = 0; i < this.favoriteList.length; i++) {
                    if (this.event.eventPlaceId === this.favoriteList[i].id) {
                        return true;
                    }
                }
                return false;
            }
        }
    }

    getAllFavorite() {
            this.favoriteService.getUserFavoritePlaces().subscribe(data => {
                this.favoriteList = data.map(e => {
                    return {
                        docId: e.payload.doc.id,
                        id: e.payload.doc.data()['id'],
                        name: e.payload.doc.data()['name'],
                        address: e.payload.doc.data()['address'],
                        lat: e.payload.doc.data()['lat'],
                        lng: e.payload.doc.data()['lng'],
                        tags: e.payload.doc.data()['tags']
                    };
                })
            });
    }

    async getUserParticipation() {

        let obj = await this.event.participants.find(obj => obj.uid == this.authService.getCurrentUserId());
        this.userStatus = obj.invitationAccepted;
    }

    async setUserParticipation(state) {

        this.authService.getUserInfo(this.event.owner).then(
           owner => {
               this.authService.getUserInfo(this.authService.getCurrentUserId()).then(
                   participant => {
                        let recipient = owner.firebaseId;
                        let title = this.translate.instant("notifications.EVENT_PARTICIPATION");
                        let body;
                        if(state == "REJECTED"){
                            body = participant.displayName + this.translate.instant("notifications.REJECT_PARTICIPATION") + this.event.title;
                        } else {
                            body = participant.displayName + this.translate.instant("notifications.ACCEPT_PARTICIPATION") + this.event.title;
                        }
                        let link = '/tabs/event-details/';
                        let eventId = this.eventId;

                        this.notificationService.createNewNotification(recipient, title, body, link, eventId, false, null);
                        this.fcmService.getDeviceInfo(recipient).then(
                            (deviceToken: any) => {
                                this.fcmService.sendPushNotification(title,
                                    body,
                                    deviceToken.data()['deviceToken'],
                                    recipient,
                                    { payload: { type: 'participation', link: link, linkData: eventId } }
                            )
                        });
                    });
               
        });

        let eventUpdated = this.event;

        await eventUpdated.participants.forEach(async element => {

            if (element.uid == this.authService.getCurrentUserId()) {
                element.invitationAccepted = state;
                this.userStatus = state;
            }
        });

        if (state == "REJECTED") {
            let i = 0;
            let p = 0;
            let passenger;
            let arrayDevice = new Array();
            await this.getUserTransport().then(res => {
                if (typeof res !== 'undefined') {
                    p++;

                    this.transportList.forEach(async element => {
                        if (element.userId == this.authService.getCurrentUserId() && element.type == 'CAR' ||
                            element.userId == this.authService.getCurrentUserId() && element.type == 'MOTO') {
                            await this.eventService.removeTransport(this.eventId, element.transportDocId);
                        }
                        if (element.passengers.length >= 0) {
                            for (let i = 0; element.passengers.length > i; i++) {
                                if (element.passengers[i].uid == this.authService.getCurrentUserId()) {
                                    element.passengers.splice(i)
                                }
                                passenger = element.passengers;
                            }

                            this.authService.getUserInfo(this.authService.getCurrentUserId()).then(
                                owner => {
                                    return owner;
                                }
                            ).then(owner => {
                                element.passengers.forEach(element => {
                                    if (element.uid != this.uid) {
                                        let recipient = element.uid;
                                        let title = this.translate.instant("notifications.DEL_TRANSPORT");
                                        let body = this.translate.instant("notifications.CURRENT_TRANSPORT") + owner.displayName + this.translate.instant("notifications.DELETE_CURRENT_TRANSPORT") + this.event.title + this.translate.instant("notifications.FROM_DATE") + new Date(this.date).toLocaleDateString('fr-FR', this.options);
                                        let link = '/tabs/event-details/';
                                        let eventId = this.eventId;

                                        this.notificationService.createNewNotification(recipient, title, body, link, eventId, false, null);

                                        this.fcmService.getDeviceInfo(recipient).then(
                                            (deviceToken: any) => {
                                                this.fcmService.sendPushNotification(title,
                                                        body,
                                                        deviceToken.data()['deviceToken'],
                                                        recipient,
                                                        { payload: { type: 'transport', link: link, linkData: eventId } }
                                                    )
                                            }
                                        )
                                    }
                                });
                            })

                            await this.eventService.updateTransport(this.eventId, element.transportDocId, element);
                        }
                    });
                }
            });
        } else {
            this.getUserTransport()
        }

        await this.eventService.updateEvent(this.eventId, eventUpdated);
        await this.ionViewWillEnter();
        await this.presentToast();
    }

    async getUserTransport() {
        let i = 0;
        let p = 0;
        return new Promise<any>(async (resolve) => {
            await this.eventService.getTransports(this.eventId).subscribe(data => {

                p++;
                let list = [];
                data.map(async e => {
                    await this.authService.getUserInfo(e.payload.doc.data()['userId']).then(e => {
                        i++;
                        this.ownerName = e.displayName;
                    })

                    let newDate = new Date(e.payload.doc.data()['transportDate'])

                    list.push({
                        ownerName: this.ownerName,
                        fromAddress: e.payload.doc.data()['fromAddress'],
                        passengers: e.payload.doc.data()['passengers'],
                        seats: e.payload.doc.data()['seats'],
                        date: newDate,
                        transportDate: new Date(newDate).toLocaleDateString('fr-FR', this.options),
                        type: e.payload.doc.data()['type'],
                        userId: e.payload.doc.data()['userId'],
                        transportDocId: e.payload.doc.id
                    })

                    if (i === p) {
                        resolve(list)
                    }
                });

                if (data.length == 0) {
                    resolve(list);
                }
            });
        });
    }

    async presentToast(message?) {

        let msg = this.translate.instant(message ? message : 'event-details.SUCCESS');

        const toast = await this.toastController.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    async getEvent(eventDocId) {

        return new Promise<any>(async (resolve) => {
            await this.eventService.getOneEvent(eventDocId).subscribe(data => {
                let event = data.payload.data()
                resolve(event)
            });
        });
    }

    async getParticipant(member) {
        let participant = {
            firstName: null,
            lastName: null,
            imageURL: null,
            status: member.invitationAccepted,
            uid: member.uid
        }

        return new Promise<any>(async (resolve) => {
            await this.authService.getUserInfo(member.uid).then(res => {
                participant.imageURL = res.imageURL
                participant.firstName = res.firstName;
                participant.lastName = res.lastName;
                resolve(participant)
            });
        });
    }

    getIcon(place) {
        var types = place.types;
        var iconBase = '../../../assets/images/icon-upviewsly.svg';

        var typeRestaurant = (types.indexOf("restaurant") > -1);
        var typeBar = (types.indexOf("bar") > -1);
        var typeCafe = (types.indexOf("cafe") > -1);

        if (typeBar) {
            iconBase = '<span class="map-icon map-icon-night-club"></span>';
            this.isService = true;
        }
        else if (typeRestaurant) {
            iconBase = '<span class="map-icon map-icon-restaurant"></span>';
            this.isService = true;
        }
        else if (typeCafe) {
            iconBase = '<span class="map-icon map-icon-cafe"></span>';
            this.isService = true;
        } else {
            this.isService = false;
        }
        return iconBase;
    }

    async initMap() {

        mapEvent = await new google.maps.Map(this.mapElement.nativeElement, {
            center: { lat: Config.DEFAULT_CENTER_LAT, lng: Config.DEFAULT_CENTER_LNG },
            zoom: Config.DEFAULT_ZOOM_LOW,
            streetViewControl: false,
            zoomControl: true,
            mapTypeControl: false,
            draggable: true,
            styles: [
                {
                    featureType: 'poi',
                    stylers: [{ visibility: 'off' }]
                },
                {
                    featureType: 'transit',
                    stylers: [{ visibility: 'off' }]
                }
            ]
        });

        let that = this;

        var image = {
            path: mapIcons.shapes.MAP_PIN,
            fillColor: '#820080',
            fillOpacity: 1,
            strokeColor: '',
            strokeWeight: 0,
            scale: 1
        };

        var infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(mapEvent);

        service.getDetails({
            placeId: that.event.eventPlaceId
        },
            async function(result) {

                that.place = result;

                var icon = that.getIcon(result);
                function round(value, precision) {
                    var multiplier = Math.pow(10, precision || 0);
                    return Math.round(value * multiplier) / multiplier;
                }

                if (result.price_level) {
                var priceLevel;
                switch (result.price_level) {
                    case 0:
                        priceLevel = "€";
                        break;
                    case 1:
                        priceLevel = "€";
                        break;
                    case 2:
                        priceLevel = "€€";
                        break;
                    case 3:
                        priceLevel = "€€€";
                        break;
                    case 5:
                        priceLevel = "€€€€";
                        break;
                    default:
                        priceLevel = "€";
                        break;
                }
            }

            let ratingHtml = "";
            if(result.rating){
                ratingHtml = that.translate.instant('map.RATING') + round(result.rating, 1).toFixed(1) + ' / 5  ' + '(' + result.user_ratings_total +
                    that.translate.instant('map.RATING_NUMBER') + ')' + " &bull; ";
                if(result.price_level){
                    ratingHtml +=  priceLevel;
                }
            }

            var infowindowContent =
                    '<div class="info-window-content" style="min-width: 185px; min-height: 80px; padding: 0 10px 0 0;">' +
                    '<strong style="display: inline-block; margin: 2px 0 6px;">' + result.name + '</strong><br>' +
                    result.vicinity + "<br>" +
                    ratingHtml +
                    '<ul class="info-window--list-actions" style="list-style: none; margin: .75em 0; padding: 0;"><li style="margin: 0 0 .3rem 0;"><ion-text><a style="font-weight: 400; text-decoration: none;" class="font-size-0_8" target="_blank" href="https://www.google.com/maps/search/?api=1&query=' + result.name + '&query_place_id=' + result.place_id + '">'
                    + 'Voir sur Google' + '</a></ion-text></li></ul></div>';

                infowindow.setContent(infowindowContent);

                if(that.isService) {
                    var marker = await new mapIcons.Marker({
                        map: mapEvent,
                        icon: image,
                        position: result.geometry.location,
                        map_icon_label: icon,
                        zIndex: 9
                    });
                } else {
                    var marker = await new google.maps.Marker({
                        position: result.geometry.location,
                        icon: icon,
                        map: mapEvent
                    });
                }


                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(infowindowContent);
                    infowindow.open(mapEvent, marker);
                })

                google.maps.event.addListenerOnce(mapEvent, 'bounds_changed', function() {
                    infowindow.setContent(infowindowContent);
                    infowindow.open(mapEvent, marker);
                });

                let newCenter = { lat: result.geometry.location.lat(), lng: result.geometry.location.lng() }
                that.eventLocation = newCenter;
                await mapEvent.setCenter(newCenter)
                await mapEvent.setZoom(Config.DEFAULT_ZOOM_HIGH);
            });
    }

    back() {
        this._location.back();
    }

    launchNavigatorApp() {
        let options: LaunchNavigatorOptions = {
          appSelection: {
              list: [this.launchNavigator.APP.GOOGLE_MAPS, this.launchNavigator.APP.WAZE, this.launchNavigator.APP.APPLE_MAPS, this.launchNavigator.APP.CITYMAPPER ],
              dialogHeaderText: this.translate.instant('common.NAVIGATOR_PROMPT_HEADER'),
              cancelButtonText : this.translate.instant('common.CANCEL'),
              rememberChoice: {
                  prompt: {
                      headerText: this.translate.instant('common.NAVIGATOR_PROMPT_REMEMBER_HEADER'),
                      bodyText: this.translate.instant('common.NAVIGATOR_PROMPT_REMEMBER_BODY'),
                      yesButtonText: this.translate.instant('common.YES'),
                      noButtonText: this.translate.instant('common.NO')
                  }
              }
          }
        }
        this.launchNavigator.navigate([this.eventLocation.lat, this.eventLocation.lng], options)
            .then(
                success => console.log('Launched navigator'),
                error => console.log('Error launching navigator', error)
            );
    }

}
