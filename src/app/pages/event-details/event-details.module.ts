import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { EventDetailsPage } from './event-details.page';
import { UserProfilePageModule } from '../user-profile/user-profile.module';

const routes: Routes = [
  {
    path: '',
    component: EventDetailsPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        IonicModule,
        TranslateModule,
        RouterModule.forChild(routes),
        UserProfilePageModule
    ],
    declarations: [EventDetailsPage]
})
export class EventDetailsPageModule {}