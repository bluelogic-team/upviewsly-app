import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { TagInputModule } from 'ngx-chips';

import { IonicModule } from '@ionic/angular';

import { EditFavoritePage } from './edit-favorite.page';

const routes: Routes = [
  {
    path: '',
    component: EditFavoritePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    TagInputModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EditFavoritePage]
})
export class EditFavoritePageModule {}
