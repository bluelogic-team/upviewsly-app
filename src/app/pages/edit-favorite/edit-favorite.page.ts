import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Platform, ModalController, NavParams, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TranslateService } from "@ngx-translate/core";
import { FavoriteService } from "../../providers/favorite/favorite.service";
import * as Config from '../../../utils/config';
import * as firebase from 'firebase';
import { AuthService } from 'src/app/providers/auth/auth.service';

declare var google;
let mapEvent: any;
declare var mapIcons;
let infowindow: any;


@Component({
    selector: 'app-edit-favorite',
    templateUrl: './edit-favorite.page.html',
    styleUrls: ['./edit-favorite.page.scss'],
})
export class EditFavoritePage implements OnInit {

    place: any;
    tags: any;
    type: any;
    favori: {};
    form: FormGroup;
    placeId: any;
    zoom: any;
    pos;
    eventPlace: any;
    streetNumber: any;
    street: any;
    country: any;
    zipCode: any;
    city: any;
    selectedPlace: any;
    isService: boolean = false;

    @ViewChild('map') mapElement: ElementRef;

    constructor(
        private modalCtrl: ModalController,
        private favoriteService: FavoriteService,
        private platform: Platform,
        private navParams: NavParams,
        private formBuilder: FormBuilder,
        private translate: TranslateService,
        private toastController: ToastController,
        private authService: AuthService
    ) {

        platform.ready().then(() => {

            this.tags = this.navParams.get('tagList');
            this.place = this.navParams.get('place');
            this.type = this.navParams.get('type');
            this.form = this.formBuilder.group({
                tags: [this.tags],
            });
        });
    }

    ngOnInit() {
    }

    async ionViewWillEnter() {
        await this.initMap();
    }

    setAddress(place) {

        let isEstablishment = false;

        if (place.types.indexOf("establishment") != -1) {
            isEstablishment = true;
            this.eventPlace = place.name
        }

        for (let i = 0; i < place.address_components.length; i++) {
            let component = place.address_components[i];
            let addressType = component.types[0];

            switch (addressType) {
                case 'street_number':
                    this.streetNumber = component.long_name;
                    break;
                case 'route':
                    this.street = component.short_name;
                    break;
                case 'locality':
                    this.city = component.long_name;
                    break;
                case 'postal_code':
                    this.zipCode = component.long_name;
                    break;
                case 'country':
                    this.country = component.long_name;
                    break;
            }
        }

        if (!this.streetNumber) { this.streetNumber = null }
        if (!this.street) { this.street = null }
        if (!this.city) { this.city = null }
        if (!this.zipCode) { this.zipCode = null }
        if (!this.country) { this.country = null }

        if (this.streetNumber != null && this.street != null && this.city != null) {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.streetNumber + ' ' + this.street + ', ' + this.city;
            } else {
                this.eventPlace = this.streetNumber + ' ' + this.street + ', ' + this.city;
            }
        }
        else if (this.street != null && this.city != null) {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.street + ', ' + this.city
            } else {
                this.eventPlace = this.street + ', ' + this.city
            }
        }
        else if (this.city != null) {
            if (isEstablishment) {
                this.eventPlace += ', ' + this.city;
            } else {
                this.eventPlace = this.city;
            }
        }
        else {
            this.eventPlace = null;
        }
    }

    async initMap() {

        mapEvent = new google.maps.Map(this.mapElement.nativeElement, {
            center: { lat: this.place.lat, lng: this.place.lng },
            zoom: Config.DEFAULT_ZOOM_HIGH,
            streetViewControl: false,
            zoomControl: false,
            mapTypeControl: false,
            draggable: true,
            styles: [
                {
                    featureType: 'poi',
                    stylers: [{ visibility: 'off' }]
                },
                {
                    featureType: 'transit',
                    stylers: [{ visibility: 'off' }]
                }
            ]
        });

        

        if (this.place.id) {
            let self = this;
            var service = new google.maps.places.PlacesService(mapEvent);
            service.getDetails({
                placeId: self.place.id
            }, async function(result) {
                self.setAddress(result);

                var icon = self.getIcon(result);

                var image = {
                    path: mapIcons.shapes.MAP_PIN,
                    fillColor: '#F400FE',
                    fillOpacity: 1,
                    strokeColor: '',
                    strokeWeight: 0,
                    scale: 1
                };

                if(self.isService){
                    var marker = new mapIcons.Marker({
                        map: mapEvent,
                        icon: image,
                        position: result.geometry.location,
                        map_icon_label: icon,
                        zIndex: 9,
                        place: {
                            placeId: self.place.id,
                            location: result.geometry.location
                        }
                    });
                } else {
                    var marker = new google.maps.Marker({
                        map: mapEvent,
                        icon: icon,
                        position: result.geometry.location,
                        zIndex: 9,
                        place: {
                            placeId: self.place.id,
                            location: result.geometry.location
                        }
                    });
                }


                infowindow = new google.maps.InfoWindow();
                function round(value, precision) {
                    var multiplier = Math.pow(10, precision || 0);
                    return Math.round(value * multiplier) / multiplier;
                }

                if (result.price_level) {
                var priceLevel;
                switch (result.price_level) {
                    case 0:
                        priceLevel = "€";
                        break;
                    case 1:
                        priceLevel = "€";
                        break;
                    case 2:
                        priceLevel = "€€";
                        break;
                    case 3:
                        priceLevel = "€€€";
                        break;
                    case 5:
                        priceLevel = "€€€€";
                        break;
                    default:
                        priceLevel = "€";
                        break;
                }
            }

            let ratingHtml = "";
            if(result.rating){
                ratingHtml = self.translate.instant('map.RATING') + round(result.rating, 1).toFixed(1) + ' / 5  ' + '(' + result.user_ratings_total +
                    self.translate.instant('map.RATING_NUMBER') + ')' + " &bull; ";
                if(result.price_level){
                    ratingHtml +=  priceLevel;
                }
            }
            var infowindowContent =
                    '<div class="info-window-content" style="min-width: 185px; min-height: 80px; padding: 0 10px 0 0;">' +
                    '<strong style="display: inline-block; margin: 2px 0 6px;">' + result.name + '</strong><br>' +
                    result.vicinity + "<br>" +
                    ratingHtml +
                    '<ul class="info-window--list-actions" style="list-style: none; margin: .75em 0; padding: 0;"><li style="margin: 0 0 .3rem 0;"><ion-text><a style="font-weight: 400; text-decoration: none" class="font-size-0_8" target="_blank" href="https://www.google.com/maps/search/?api=1&query=' + result.name + '&query_place_id=' + result.place_id + '">'
                    + 'Voir sur Google' + '</a></ion-text></li></ul></div>';

                infowindow.setContent(infowindowContent);
                await new google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(mapEvent, marker);

            })
                let newCenter = { lat: result.geometry.location.lat(), lng: result.geometry.location.lng() }
                mapEvent.setCenter(newCenter);
                mapEvent.setZoom(Config.DEFAULT_ZOOM_HIGH);
            });
        }
    }

    getIcon(place) {
        var types = place.types;
        var iconBase = '../../../assets/images/icon-upviewsly.svg';

        if (typeof types !== 'undefined') {
            var typeRestaurant = (types.indexOf("restaurant") > -1);
            var typeBar = (types.indexOf("bar") > -1);
            var typeCafe = (types.indexOf("cafe") > -1);


            if (typeBar) {
                iconBase = '<span class="map-icon map-icon-night-club"></span>';
                this.isService = true;
            }
            else if (typeRestaurant) {
                iconBase = '<span class="map-icon map-icon-restaurant"></span>';
                this.isService = true;
            }
            else if (typeCafe) {
                iconBase = '<span class="map-icon map-icon-cafe"></span>';
                this.isService = true;
            } else {
                this.isService = false;
            }
        }
        return iconBase;
    }

    initPlace(place) {
        // Set default null
        let updatedPlace = {
            docId: place.docId || null,
            id: place.id,
            lat: place.lat || null,
            lng: place.lng || null,
            name: place.name,
            address: place.address,
            tags: place.tags || [],
            types: place.types || null
        }

        if (updatedPlace.tags.length == 0) {
            updatedPlace.tags = []
        }
        return updatedPlace
    }

    async presentToast(type) {

        let msg;

        switch (type) {
            case 'add':
                msg = this.translate.instant('edit-favorite.UPDATE_SUCCESS')
                break;
        }


        const toast = await this.toastController.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    }

    async uploadFavorite(place, form, type) {

        let newPlace = this.initPlace(place);

        if (type != 'create') {
            newPlace.tags = [];

            form.tags.forEach(element => {
                let type = typeof element;
                if (type == 'object') {
                    newPlace.tags.push(element.value)
                }
                else {
                    newPlace.tags.push(element)
                }
            });
            this.updateFavorite(newPlace);
        }
        else {
            newPlace.tags = this.tagArrayToString(form.tags);
            this.addFavorite(newPlace);
        }
    }

    async addFavorite(place) {

        let favori = {
            docId: await place.docId,
            id: await place.id,
            lat: await place.lat,
            lng: await place.lng,
            name: await place.name,
            address: await place.address,
            tags: await place.tags,
            types: await place.types
        }
        await this.favoriteService.addUserFavoritePlace(favori);
        this.presentToast('add');
        this.cancel();
    }

    async updateFavorite(place) {

        await this.favoriteService.updateFavoritePlace(place).then(() => {
            this.presentToast('add');
        });
        this.cancel();
    }

    tagArrayToString(tagArray: string[]) {
        if (Array.isArray(tagArray) && tagArray.length > 0) {
            const tags = tagArray.map((e: any) => `${e.value}`);
            return tags;
        } else {
            return '';
        }
    }

    cancel() {
        this.modalCtrl.dismiss(false);
    }

    async getOneFavorite(favoriteId) {

        return new Promise<any>((resolve, reject) => {
            this.favoriteService.getOneFavoritePlace(favoriteId)
                .then(res => {
                    this.favori = {
                        docId: res.docId,
                        id: res.id,
                        name: res.name,
                        lat: res.lat,
                        lng: res.lng,
                        tags: res.tags,
                        types: res.types
                    }
                    resolve(res);
                }, err => reject(err))
        });
    }
}
