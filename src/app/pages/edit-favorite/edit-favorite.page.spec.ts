import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFavoritePage } from './edit-favorite.page';

describe('EditFavoritePage', () => {
  let component: EditFavoritePage;
  let fixture: ComponentFixture<EditFavoritePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFavoritePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFavoritePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
