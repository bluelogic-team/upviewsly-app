import { Injectable } from '@angular/core';
import { FCM } from '@ionic-native/fcm/ngx';
import { AngularFirestore } from 'angularfire2/firestore';
import { Router } from '@angular/router';
// import { AuthService } from '../auth/auth.service';  
import { environment } from '../../../environments/environment';
import { HTTP } from '@ionic-native/http/ngx';
import { Badge } from '@ionic-native/badge/ngx';


@Injectable({
    providedIn: 'root'
})
export class FcmService {

    public tokenId: string;
    public redirectToHome: boolean  = false;

    constructor(
        private fcm: FCM,
        private http: HTTP,
        private db: AngularFirestore,
        private router: Router,
        private badge: Badge,
    ) { 
}

    getToken(userId: string) {
        return new Promise((resolve, reject) => {
            this.fcm.getToken().then(
                (token: any) => {
                    resolve(this.tokenId = token);
                    // get user from firestore database
                    let docRef = this.db.collection("users").doc(userId);
                    docRef.ref.get().then(async (doc) => {
                        if (doc.exists) {
                            await docRef.update({
                                deviceToken: token
                            })
                                .catch(err => {
                                    console.log(err);
                                });
                        }
                    })
                }

            ).catch(
                (err: any) => {
                    reject('Error occured : ' + err);
                }
            );
        });
    };

    clearToken(userId: string){
        let docRef = this.db.collection("users").doc(userId);
        docRef.ref.get().then(async (doc) => {
           	if (doc.exists) {
            	docRef.update({
                    deviceToken: ""
                    })
          		.catch(err => {
                    console.log(err);
                });
            }
        })
    }

    getDeviceInfo(userId: string) {
        return new Promise((resolve) => {
            this.db.collection('users').doc(userId).get().subscribe(
                res => (resolve(res))
            );
        })
    }

    subscribeToChannel(channel: string) {
        this.fcm.subscribeToTopic(channel);
    };

    unsubscribeFromChannel(channel: string) {
        this.fcm.unsubscribeFromTopic(channel);
    };
    
    getPushNotification() {
        if(!this.redirectToHome) {
            this.router.navigate(['/tabs']);
            this.redirectToHome = true;
        }

        return this.fcm.onNotification().subscribe(
            (data: any) => {
            	if(data.wasTapped && data.payload){
                    this.badge.clear();
                    this.redirectToHome = true;
            		var pushData = JSON.parse(data.payload);
            		if(pushData.linkData){
                        this.router.navigate([pushData.link, pushData.linkData]);
            		} else {
            			this.router.navigate([pushData.link]);
                    }
                    return true;
            	}
            }
        );

    };

    /**
     * 
     * @param title 
     * @param body 
     * @param token This is the token user id. Its an array of ids.
     * @param data It must be an Object and is optionnal.
     */
    sendPushNotification(title: string, body: string, recipientToken: any, recipientId: any, data?: any) {
        this.db.collection("notifications/", ref => ref.where("recipient", "==", recipientId).where("isRead", "==", false)).get().subscribe(docRef => {
          let badgeCount = docRef.docs.length;
          const options = {
            notification: {
                title: title,
                body: body,
                sound: "default",
                click_action: "FCM_PLUGIN_ACTIVITY",
                icon: "fcm_push_icon",
                badge: badgeCount
            },
            data: data,
            to: recipientToken,
            priority: "high"
            }

            const headers = {
                'Content-Type': "application/json",
                'Authorization': `key=`+ environment.FCM_KEY
            };

        this.http.setDataSerializer('json');

        this.http.post('https://fcm.googleapis.com/fcm/send', options, headers)
        .then(data => { 
        console.log(data);
            }).catch(error => {
        console.log(error);

            });
        })
    }
}