import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService, User } from '../../providers/auth/auth.service';

export interface Group {
  id?: string;
  owner: string;
  name: string;
  imageURL: string;
  members: string[];
}

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  private groupsCollection: AngularFirestoreCollection<Group>;

  private groups: Observable<Group[]>;

  constructor(
    private db: AngularFirestore,
    public authService: AuthService
    ) {
      // this.comments$ = afs.collectionGroup('Comments', ref => ref.where('user', '==', userId))
      // .valueChanges({ idField });
    
    // collection name in firebase
    this.groupsCollection = db.collection<Group>('groups');

    this.groups = this.groupsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getGroups(){
    return this.groups;
  }

  getGroup(id) {
    return this.groupsCollection.doc<Group>(id).valueChanges();
  }

  updateGroup(group: Group, id: string) {
    return this.groupsCollection.doc(id).update(group);
  }

  addGroupWithId(group: Group, id: string) {
    return this.groupsCollection.doc(id).set(group);
  }
 
  addGroup(group: Group) {
    return this.groupsCollection.add(group);
  }
 
  removeGroup(id) {
    return this.groupsCollection.doc(id).delete();
  }

  getGroupInfo(id){
    // return this.db.collection<User>("user").doc(userId).valueChanges();
    return new Promise<any>((resolve, reject) => {

      var docRef = this.db.collection("groups").doc(id);
      docRef.ref.get().then(doc => {
        resolve (doc.data());
      }).catch(err => {
        reject(err);
      });
    });
  }

  getUserGroups(ownerId): AngularFirestoreCollection {
    if(typeof ownerId !== 'undefined') {
      let res = this.db.collection('groups', 
          ref => ref.where('members', 'array-contains', ownerId));
        return res;
    }
  }
}
