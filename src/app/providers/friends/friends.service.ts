import { Injectable } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AlertController } from '@ionic/angular';
import { TranslateService } from "@ngx-translate/core";
import { map } from 'rxjs/operators';
import { NotificationsService } from '../notifications/notifications.service';
import { FcmService } from '../fcm/fcm.service';

enum RequestStatus {
    PENDING = 1,
    ACCEPTED = 2,
    REJECTED = 3,
}

@Injectable({
    providedIn: 'root'
})
export class FriendsService {

    constructor(
        public authService: AuthService,
        public afs: AngularFirestore,
        public alertController: AlertController,
        public translate: TranslateService,
        public notificationService: NotificationsService,
        private fcmService: FcmService
    ) { }

    async sendFriendRequest(friend) {
        const user = await this.authService.getCurrentUser();

        let collection = this.afs.collection('friendRequests',
            ref => ref
                .where("requester", "==", friend.firebaseId)
                .where("recipient", "==", user.uid));

        let obs = collection.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            })
        );

        let observable = obs.subscribe(async data => {
            if (data.length == 0) {
                const data = {
                    createdAt: Date.now(),
                    requester: user.uid,
                    recipient: friend.firebaseId,
                    status: RequestStatus.PENDING
                }
                friend.isAlreadyRequested = true;
                this.afs.collection('friendRequests').add(data);
                observable.unsubscribe();
            } else {
                const data = {
                    createdAt: Date.now(),
                    requester: user.uid,
                    recipient: friend.firebaseId,
                    status: RequestStatus.REJECTED
                }
                friend.isAlreadyRequested = true;
                this.afs.collection('friendRequests').add(data);
                observable.unsubscribe();
            }
            await this.createNotification(friend);
        })

    }

    async checkFriendRequest() {
        const userId = await this.authService.getCurrentUserId();

        let collection = this.afs.collection('friendRequests',
            ref => ref
                .where("recipient", "==", userId)
                .where("status", "==", RequestStatus.PENDING));

        let obs = collection.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            })
        );

        return obs;
    }

    acceptRequest(request) {
        request.status = RequestStatus.ACCEPTED;
        this.afs.collection('friendRequests').doc(request.id).update(request);
        this.afs.collection("users/" + request.recipient + "/friends/").add({ uid: request.requester });
        this.afs.collection("users/" + request.requester + "/friends/").add({ uid: request.recipient });
        this.createRequestAcceptedNotification(request);
        // TODO add friends in user collections,
        // replace checkboxes of add-fb-users page and contacts page by "send invite" buttons
        // this.afs.collection('users').doc(request.requester).update()
    }

    rejectRequest(request) {
        request.status = RequestStatus.REJECTED;
        this.afs.collection('friendRequests').doc(request.id).update(request);
    }

    getUserFriends() {
        return new Promise<any>((resolve, reject) => {
  
            var ref = this.afs.firestore.collection("users/" + this.authService.getCurrentUserId() + "/friends/");
                ref.get().then(friends => {
                    resolve (friends);
                }).catch(err => {
                    reject(err);
                });
            });
    }

    checkUserIsAlreadyFriend(userId, friendId) {
        var alreadyFriends = this.afs.collection("users/" + userId + "/friends/",
            ref => ref.where('uid', '==', friendId));
        let obs = alreadyFriends.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            })
        );
        return obs;
    }

    checkUserAlreadyFriendRequested(userId, friendId) {
        var alreadyRequested = this.afs.collection("friendRequests/",
            ref => ref.where('requester', '==', userId).where('recipient', '==', friendId));
        let obs = alreadyRequested.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            })
        );
        return obs;
    }

    async removeFriend(friend) {
        const userId = this.authService.getCurrentUserId();
        return new Promise(async resolve => {
            this.removeFriendsBfromA(userId, friend.firebaseId).then(() => {
                this.removeFriendsAfromB(friend.firebaseId, userId).then(() => {
                    this.removeFriendRequestBfromA(userId, friend.firebaseId).then(() => {
                        this.removeFriendRequestAfromB(friend.firebaseId, userId).then(() => {
                            resolve(true);
                        });
                    });
                });
            });
        });
    }

    async removeFriendsBfromA(userB, userA) {
        return new Promise(async resolve => {
            let obs = this.checkUserIsAlreadyFriend(userB, userA)
                .subscribe(
                    (data: any) => {
                        if (data.length > 0) {
                            data.forEach(doc => {
                                this.afs.collection("users/" + userB + "/friends/").doc(doc.id).delete();
                                obs.unsubscribe()
                                resolve(true);
                            });
                        } else {
                            resolve(false);
                        }
                    })
        });
    }

    async removeFriendsAfromB(userA, userB) {
        return new Promise(async resolve => {
            let obs = this.checkUserIsAlreadyFriend(userA, userB)
                .subscribe(
                    (data: any) => {
                        if (data.length > 0) {
                            data.forEach(doc => {
                                this.afs.collection("users/" + userA + "/friends/").doc(doc.id).delete();
                                obs.unsubscribe()
                                resolve(true);
                            });
                        } else {
                            resolve(false);
                        }
                    });
        });
    }

    async removeFriendRequestBfromA(userB, userA) {
        return new Promise(async resolve => {
            let obs = this.checkUserAlreadyFriendRequested(userB, userA)
                .subscribe(
                    (data: any) => {
                        if (data.length > 0) {
                            data.forEach(doc => {
                                this.afs.collection("friendRequests/").doc(doc.id).delete();
                                obs.unsubscribe()
                                resolve(true);
                            });
                        } else {
                            resolve(false);
                        }
                    })
        });
    }

    async removeFriendRequestAfromB(userA, userB) {
        return new Promise(async resolve => {
            let obs = this.checkUserAlreadyFriendRequested(userA, userB)
                .subscribe(
                    (data: any) => {
                        if (data.length > 0) {
                            data.forEach(doc => {
                                this.afs.collection("friendRequests/").doc(doc.id).delete();
                                obs.unsubscribe()
                                resolve(true);
                            });
                        } else {
                            resolve(false);
                        }
                    });
        });
    }

    async createNotification(friend) {

        let name;

        await this.authService.getUserInfo(this.authService.getCurrentUserId()).then(res => {
            name = res.displayName;
        });

        let recipient = friend.firebaseId;
        let title = this.translate.instant("notifications.FRIEND_REQUEST_TILTE");
        let body = name + this.translate.instant("notifications.FRIEND_REQUEST");
        let link = '/tabs/friends';
        await this.fcmService.getDeviceInfo(recipient).then(
            (deviceToken: any) => {
                // Substract 1 from participant to remove the owner count for match all entries on the array
                this.fcmService.sendPushNotification(
                    title,
                    body,
                    deviceToken.data()['deviceToken'],
                    recipient,
                    { payload: { type: 'friend', link: link, linkData: null } }
                )
            }
        ).then(() => this.notificationService.createNewNotification(recipient, title, body, link, null, false, null))
    }

    async createRequestAcceptedNotification(request) {

        let name;

        await this.authService.getUserInfo(this.authService.getCurrentUserId()).then(res => {
            name = res.displayName;
        });

        let recipient = request.requester;
        let title = this.translate.instant("notifications.FRIEND_ACCEPTED_TILTE");
        let body = this.translate.instant("notifications.FRIEND_ACCEPTED") + name;
        let link = '/tabs/friends';
        await this.fcmService.getDeviceInfo(recipient).then(
            (deviceToken: any) => {
                // Substract 1 from participant to remove the owner count for match all entries on the array
                this.fcmService.sendPushNotification(
                    title,
                    body,
                    deviceToken.data()['deviceToken'], recipient,
                    { payload: { type: 'friend', link: link, linkData: null } }
                )
            }
        ).then(() => this.notificationService.createNewNotification(recipient, title, body, link, null, false, null))
    }

}
