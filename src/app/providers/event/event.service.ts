import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { AngularFirePerformance } from '@angular/fire/performance';

@Injectable({
    providedIn: 'root'
})
export class EventService {

    private eventCollection: AngularFirestoreCollection;
    private events: Observable<any[]>;

    constructor(
        private db: AngularFirestore,
        private auth: AuthService,
        private perf: AngularFirePerformance
    ) { }

    createNewEvent(event) {
        return this.db.collection("events/").add(event);
    }

    updateEvent(eventId, event) {

        return this.db.collection("events/").doc(eventId).update(event);
    }

    getEvents() {
        // return new Observable((res) => res)
        this.eventCollection = this.db.collection("events", ref => ref.where("participant_id", "array-contains", this.auth.getCurrentUserId()).orderBy("date", "asc"));
        this.events = this.eventCollection.snapshotChanges().pipe(
            map(actions => {
                this.perf.trace('getEvents', { metrics: { eventSize: actions.length } })
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            }),

        );


        return this.events;
    }

    getOneEvent(eventDocId) {
        return this.db.collection("events/").doc(eventDocId).snapshotChanges();
    }

    removeEvent(eventId) {

        return this.db.collection("events/").doc(eventId).delete();
    }

    addTransport(eventId, transport) {

        return this.db.collection("events/" + eventId + "/transports/").add(transport);
    }
    removeTransport(eventId, transportId) {

        return this.db.collection("events/" + eventId + "/transports/").doc(transportId).delete();
    }

    updateTransport(eventId, transportId, transport) {

        return this.db.collection("events/" + eventId + "/transports/").doc(transportId).update(transport);
    }

    getTransports(eventId) {

        return this.db.collection("events/" + eventId + "/transports").snapshotChanges();
    }

    getOneTransport(eventId, transportId) {

        return this.db.collection("events/").doc(eventId).collection("/transports/").doc(transportId).snapshotChanges();
    }

    updateTransportSubscription(eventId, transportId, passengers) {

        return this.db.collection("events/").doc(eventId).collection("/transports/").doc(transportId).update(passengers);
    }

}
