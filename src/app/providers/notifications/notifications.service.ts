import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from '../auth/auth.service';


@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  public countObservable: any;
  public countNotify: number;

  constructor( 
    private db: AngularFirestore,
    private authService: AuthService
     ) {}

  createNewNotification(recipient, title, body, link, eventId, isMessage, chatId) {

    let notification = {
      recipient: recipient,
      title: title,
      body: body,
      link: link,
      eventId: eventId,
      createdAt: Date.now(),
      isRead: false,
      isMessage: isMessage,
      chatId: chatId
    }
    return this.db.collection("notifications/").add(notification);
  }

  getUserNotifications() {
    return this.db.collection("notifications/", ref => ref.where("recipient", "==", this.authService.getCurrentUserId()).where("isMessage", "==", false).orderBy('createdAt', 'desc')).snapshotChanges()
  }

  countUserNotifications() {
    return this.db.collection("notifications/", ref => ref.where("recipient", "==", this.authService.getCurrentUserId()).where("isRead", "==", false).where("isMessage", "==", false)).snapshotChanges()
  }

  updateNotification(notificationId, notification) {

    return this.db.collection("notifications/").doc(notificationId).update(notification);
  }

  deleteNotification(notificationId) {

    return this.db.collection("notifications/").doc(notificationId).delete();
  }


  updateNotificationMessagesIsRead(chatId, recipientId){
        this.db.collection("notifications/", ref => ref.where("recipient", "==", recipientId).where("isRead", "==", false).where("isMessage", "==", true).where("chatId", "==", chatId)).get().subscribe(data => {
          data.forEach(notif => {
            let nt = notif.data();
            let id = notif.ref.id;
            nt.isRead = true;
            this.updateNotification(id, nt);
          })
        })
    }
/*
  removeAllNotifications(){
    this.db.collection("notifications/").snapshotChanges().subscribe( data => {
      data.map( async e => {
        await this.deleteNotification(e.payload.doc.id);
      })
    })
  }
*/

}
