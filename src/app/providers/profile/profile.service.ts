import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private firestore: AngularFirestore) { }

  get_user_info(){

    //return this.firestore.collection('users').snapshotChanges();
  }

  getUserDetails(id: string) {
    return this.firestore.collection('users').doc(id).get().pipe(
      map(actions => {
        const user = {
          data: actions.data()
        };
        return { id: actions.id, ...user };
      })
    );
  };
};
