import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService, User } from '../auth/auth.service';
import { Router } from '@angular/router';
import { map, switchMap, last } from 'rxjs/operators';
import { Observable, combineLatest, of, forkJoin } from 'rxjs';
import { AngularFirePerformance } from '@angular/fire/performance';

@Injectable({
    providedIn: 'root'
})
export class ChatService {

    private chatsCollection: AngularFirestoreCollection;
    private chats: Observable<any[]>;

    private length_messages = 50;
    private lastVisible;

    public arr_messages = new Array();
    public unreadMessages: number = 0;

    public unreadChatsList = [];
    public unreadMessagesObservable: any;
    public unreadChatListObservable: any;
    public unreadLastMessageObservable: any;
    public lastReadObservable: any;

    constructor(
        private afs: AngularFirestore,
        private auth: AuthService,
        private router: Router,
        private perf: AngularFirePerformance
    ) { }

    decrementUnreadMessages(itemId) {
        if (this.unreadMessages == 0 && this.unreadChatsList.length == 0) {
            this.unreadChatsList = [];
            this.unreadMessages = 0;
        }

        if (this.unreadChatsList.includes(itemId)) {
            const idOfArray = (e) => e === itemId;
            this.unreadChatsList.splice(this.unreadChatsList.findIndex(idOfArray), 1);
            this.unreadMessages = this.unreadMessages - 1;
        }
    }

    // get all chatrooms
    getChatsList() {
        this.chatsCollection = this.afs.collection('chats', ref => ref.where('participants', 'array-contains', this.auth.getCurrentUserId()).where("eventDate", ">=", new Date().getTime() - 86400000).orderBy("eventDate", "desc"));
        var chats = this.afs.collection('chats', ref => ref.where('participants', 'array-contains', this.auth.getCurrentUserId()).where("eventId", "==", null));

        this.chats = this.chatsCollection.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            }),

            this.perf.trace('getChatList')
        );

        let chatsEvent = chats.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            }),
        )

        return combineLatest(this.chats, chatsEvent).pipe(
            map(actions => {
                return actions[0].concat(actions[1]);
            })
        );
    }

    // Get chat from event
    getChatFromEvent(eventId: string) {
        this.chatsCollection = this.afs.collection('chats', ref => ref.where('eventId', '==', eventId));
        this.chats = this.chatsCollection.snapshotChanges().pipe(
            map(action => {
                return action.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                })
            })
        );

        this.perf.trace('getChatFromEvent')
        return this.chats;
    }

    async removeChat(id) {

        await this.chatsCollection.doc(id + 'messages').delete().then(() => {
            return this.chatsCollection.doc(id).delete();
        });
    }

    getChat(id) {
        const chat = this.afs.collection('chats').doc(id).snapshotChanges();
        chat.pipe(
            this.perf.trace('getChat')
        )
        return chat;
    }

    getChatLastRead(chatId) {
        return this.afs.collection('chats').doc(chatId).collection('lastRead').doc(this.auth.getCurrentUserId()).snapshotChanges();
    }

    getLastMessage(chatId) {
        return this.afs.collection('chats').doc(chatId).collection('messages', ref => ref.limit(1).orderBy('createdAt', 'desc')).valueChanges();
    }

    getUnreadMessages() {
        let chats = this.afs.collection('chats', ref => ref.where('participants', 'array-contains', this.auth.getCurrentUserId()).where("eventDate", ">=", new Date().getTime() - 86400000).orderBy("eventDate", "desc"));
        let lastMessage = this.afs.collection('chats', ref => ref.where('participants', 'array-contains', this.auth.getCurrentUserId()).where("eventId", "==", null));

        let chat = chats.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                })
            })
        )

        let message = lastMessage.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                })
            })
        )

        return combineLatest(chat, message).pipe(
            map(actions => {
                return actions[0].concat(actions[1]);
            })
        )
    }

    lastMessage(chatId) {
        return this.afs.collection('chats').doc(chatId).collection('messages', ref => ref.orderBy('createdAt', 'desc').limit(1)).snapshotChanges();
    }

    //get the messages of a chatroom
    /**
     * 
     * @param chatId id of Chat
     * @param bool get olders messages when set to true
     */
    getMessages(chatId, bool) {
        //TODO pagination ?
        let query$ = this.afs.collection('chats');

        // let old_messages = new Array();

        if (bool) {
            let first = query$.doc(chatId)
                .collection('messages', ref => ref.orderBy('createdAt', 'desc').limit(this.length_messages));

            first.get().toPromise().then(documentSnapshots => {
                this.lastVisible = documentSnapshots.docs[documentSnapshots.docs.length - 1];
                // Construct a new query starting at this document
                let next$ = query$.doc(chatId)
                    .collection('messages', ref => ref.orderBy('createdAt', 'desc')
                        .startAfter(this.lastVisible)
                        .limit(this.length_messages))
                    .get();

                // return next$.subscribe((res: any) => {
                //     for(let i = 0; i < res.docs.length; i++) {
                //         this.arr_messages.push(res.docs[i].data());
                //     }
                // });
            })
        } else {
            let messages$ = query$.doc(chatId)
                .collection('messages', ref => ref.orderBy('createdAt', 'desc').limit(this.length_messages))
                .valueChanges({});

            // messages$.subscribe((res: any) => {
            //     for(let i = 0; i < res.docs.length; i++) {
            //         this.arr_messages.push(res.docs[i].data());
            //     }
            // })

            messages$.pipe(
                this.perf.trace('getMessage')
            )

            return messages$;
        }
    }

    updateLastViewed(chatId) {
        return this.afs.collection('chats').doc(chatId).collection('lastRead').doc(this.auth.getCurrentUserId()).update({ "timestamp": new Date().getTime() })
    }

    removeLastViewed(chatId) {
        return this.afs.collection('chats').doc(chatId).collection('lastRead').doc(this.auth.getCurrentUserId()).delete()
    }

    updateChatParticipants(chatId, data) {
        const data_participants = {
            participants: data
        };

        this.afs.collection('chats').doc(chatId).update(data_participants);
    }

    async updateChat(docId, data) {
        const chat_data = {
            title: data.title,
            eventDate: data.date,
            participants: data.participants
        }
        this.afs.collection('chats', ref => ref.where("eventId", "==", docId)).get().subscribe(res => {
            res.docs.forEach(e => {
                let chatId = e.id;
                return this.afs.collection('chats').doc(chatId).update(chat_data)
            })
        })
    }

    async createChat(title, date = null, hour = null, members, eventId = null, groupId = null, type?: string) {

        const data = {
            title: title,
            participants: members,
            eventId: eventId,
            groupId: groupId,
            eventDate: date,
            createdAt: Date.now()
        }

        if (!type) {
            const docRef = await this.afs.collection('chats').add(data);
            // Initialization of lastread user timestamp used to check if he has already read the last message
            members.forEach(e => {
                this.afs.collection('chats').doc(docRef.id).collection('lastRead').doc(e).set({ "timestamp": new Date().getTime() });
            });
            return this.router.navigate(['chat', docRef.id]);
        } else {
            return this.router.navigate(['tabs/events']);
        }
    }

    async sendMessage(chatId, content) {
        return new Promise((resolve) => {
            const data = {
                uid: this.auth.getCurrentUserId(),
                content,
                createdAt: Date.now()
            };

            if (this.auth.getCurrentUserId()) {
                const ref = this.afs.collection('chats').doc(chatId).collection('messages').add(data).then(
                    res => {
                        resolve(res.id);
                    }
                ); // with sub collection 
                // return ref.update({
                //   messages: firestore.FieldValue.arrayUnion(data)
                // });
            }
        })

    }

    joinUsers(chat$: Observable<any>) {
        let chat;
        let joinKeys = {};

        return chat$.pipe(
            switchMap(messages => {
                // Unique User IDs
                chat = messages;
                const uids = Array.from(new Set(messages.map(v => v.uid)));
                // Firestore User Doc Reads
                const userDocs = uids.map(u => {
                    let res = this.afs.doc(`users/${u}`).valueChanges().pipe(map(val => {
                        Object.assign(val, { firebaseId: u });
                        return val;
                    }));

                    return res;
                }
                );
                combineLatest(userDocs).subscribe();

                return userDocs.length ? combineLatest(userDocs) : of([]);
            }),
            map(arr => { // arr = tableau des documents des utilisateurs participants
                arr.forEach((v: any) => {
                    (joinKeys[v.firebaseId] = v);
                });
                chat = chat.map(v => {
                    return { ...v, user: joinKeys[v.uid] };
                });

                return chat;
            })
        );
    }


}