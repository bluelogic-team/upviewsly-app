import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';
import AuthProvider = firebase.auth.AuthProvider;
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { GooglePlus } from '@ionic-native/google-plus/ngx';
//import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { Storage } from '@ionic/storage';
import { AlertController, ToastController } from '@ionic/angular';
import { FcmService } from '../fcm/fcm.service';

import {Network } from '@ionic-native/network/ngx';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';



export interface User {
    firstName: string;
    lastName: string;
    displayName: string;
    email: string;
    address: string;
    zipCode: string;
    city: string;
    country: string;
    imageURL: string;
}

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    public cache_events: any;
    public cache_chatslist: any;
    public cache_chats: any;
    public networkOnDisconnect: any;
    public networkOnConnect: any;
    public chatListObservable: any;
    public cacheEventsObservable: any;

    //user: Observable<firebase.User>;

    private usersCollection: AngularFirestoreCollection<User>;

    private users: Observable<User[]>;

    public userId;

    private isLostConnection: boolean = false;
    private _toast: any;

    constructor(
        //private fbAuth: FirebaseAuthentication,
        public afAuth: AngularFireAuth,
        public facebook: Facebook,
        private gplus: GooglePlus,
        private db: AngularFirestore,
        public storage: Storage,
        public alertController: AlertController,
        private fcmService: FcmService,
        private network: Network,
        private toast: ToastController,
        private translateService: TranslateService
    ) {
        // this.user = this.afAuth.authState;
    }

    async nativeGoogleLogin(): Promise<any> {
        try {

            const gplusUser = await this.gplus.login({
                'webClientId': environment.GOOGLE_WEB_CLIENT_ID,
                'offline': true,
                'scopes': 'profile email'
            })

            let res = await this.afAuth.auth.signInWithCredential(firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken));
            this.saveLoginData(res.user, null);
            return res;

        } catch (err) {
            console.log('error native google login:', err);
        }
    }

    webGoogleLogin(): Promise<void> {
        return new Promise<any>(async (resolve, reject) => {
            const provider = new firebase.auth.GoogleAuthProvider();
            const credential = await this.afAuth.auth.signInWithPopup(provider);
            this.saveLoginData(credential.user, null);
            resolve(true);
        })
            .catch(err => {
                console.log(err);
            })

    }
    webFacebookLogin() {
        return new Promise<any>(async (resolve, reject) => {
            let provider = new firebase.auth.FacebookAuthProvider();
            const credential = await this.afAuth.auth.signInWithPopup(provider);
            this.saveLoginData(credential.user, credential.credential);
            resolve(true);
        });
    }

    //open widget, asks for permissions
    nativeFacebookLogin(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            let fbLogin = this.facebook.login(['email']) // 'user_friends' enlevé car autorisations FB trop chiantes à approuver
                .then(response => {
                    const facebookCredential = firebase.auth.FacebookAuthProvider
                        .credential(response.authResponse.accessToken);

                    var result = firebase.auth().signInWithCredential(facebookCredential)
                        .then(res => {
                            // this.storage.set('facebookCredential', res.credential);
                            this.saveLoginData(res.user, res.credential);
                            return res;
                        }, error => {
                            reject(error);
                            // throw new Error(error);
                        })

                    return result;
                }, err => {
                    reject(err);
                })

            resolve(fbLogin);
        })
    }

    async deleteAuthFacebook(user: firebase.User) {
        return new Promise<any>(async (resolve, reject) => {
            try {
                let response = await this.facebook.login(['email']);

                await user.reauthenticateWithCredential(
                    firebase.auth.FacebookAuthProvider.credential(response.authResponse.accessToken));
                // need to delete firestore document before the authentication user
                await firebase.firestore().collection("users").doc(user.uid).delete();
                await user.delete();

            } catch (err) {
                throw err;
            }

            resolve(true);
        }).catch(err => {
            console.log(err);
        })

    }

    async deleteAuthGoogle(user: firebase.User) {
        return new Promise<any>(async (resolve, reject) => {

            const gplusUser = await this.gplus.login({
                'webClientId': environment.GOOGLE_WEB_CLIENT_ID,
                'offline': true,
                'scopes': 'profile email'
            })
            await user.reauthenticateWithCredential(firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken));
            // need to delete firestore document before the authentication user
            await firebase.firestore().collection("users").doc(user.uid).delete();
            await user.delete();
            resolve(true);
        }).catch(err => {
            console.log(err);
        });
    }

    async deleteAuthEmailPassword(user: firebase.User, value) {
        return new Promise<any>(async (resolve, reject) => {

            const credential = firebase.auth.EmailAuthProvider.credential(
                user.email,
                value.password
            );

            if (value.password != undefined) {
                user.reauthenticateWithCredential(credential).then(async function() {
                    // User re-authenticated.
                    await firebase.firestore().collection("users").doc(user.uid).delete();
                    await user.delete();
                    resolve(true);

                }).catch(function(error) {
                    // An error happened.
                    reject(error);
                });
            } else {
                reject("email or password undefined");
            }

        }).catch(err => {
            console.log(err);
            throw err;
        });
    }

    deleteProfile(credential) {
        return new Promise<any>(async (resolve, reject) => {

            let user: firebase.User = firebase.auth().currentUser;

            if (user.providerData[0].providerId == "google.com") {
                await this.deleteAuthGoogle(user);
            } else if (user.providerData[0].providerId == "facebook.com") {
                await this.deleteAuthFacebook(user);
            } else if (user.providerData[0].providerId == "password") {
                await this.deleteAuthEmailPassword(user, credential);
            }
            resolve(true);
        }).catch(err => {
            console.log(err);
        });

    }

    doRegister(value) {
        return new Promise<any>((resolve, reject) => {
            firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
                .then(res => {
                    this.saveLoginData(res.user, null);
                    resolve(res);
                }, err => reject(err))
        })
    }

    // userData is returned by firebase auth
    async saveLoginData(userData, fbCredential) {
        // Check network
        this.networkOnDisconnect = this.network.onDisconnect().subscribe(async () => {
            this.isLostConnection = true;
            this._toast = await this.toast.create({
                message: this.translateService.instant('common.LOST_CONNECTION')
            });
            this._toast.present();
        });

        // watch network for a connection
        this.networkOnConnect = this.network.onConnect().subscribe(() => {
            if(this.isLostConnection) {
                this.isLostConnection = false;
                this._toast && this._toast.dismiss();
            }
        });
        var fbIndex = 0; // get facebook provider index
        userData.providerData.forEach((element, i) => {
            if (element.providerId == "facebook.com") {
                fbIndex = i;
            }
        });
        // get user from firestore database
        var docRef = this.db.collection("users").doc(userData.uid);
        docRef.ref.get().then(async (doc) => {
            //user already exists, update only facebook accesstoken
            if (doc.exists) {
                if (userData.providerData[fbIndex].providerId == "facebook.com") {
                    var facebookUid: string = userData.providerData[fbIndex].uid;
                    var facebookAccessToken: string = fbCredential.accessToken;
                    await docRef.update({
                        facebookUid: facebookUid,
                        facebookAccessToken: facebookAccessToken
                    })
                        .catch(err => {
                            console.log(err);
                        });
                }

            } else { // create new user
                if (userData.displayName == null) {

                    var displayName: string = "";
                    var firstName: string = "";
                    var lastName: string = "";
                    var imageURL: string = "";
                    var facebookUid: string = "";
                    var facebookAccessToken: string = "";

                } else { //login with google or facebook
                    var displayName: string = this.capitalizeFirstLetter(userData.displayName.split(' ', 2)[0]) + this.capitalizeFirstLetter(userData.displayName.split(' ', 2)[1]);
                    var firstName: string = this.capitalizeFirstLetter(userData.displayName.split(' ', 2)[0]);
                    var lastName: string = this.capitalizeFirstLetter(userData.displayName.split(' ', 2)[1]);

                    if (userData.providerData[fbIndex].providerId == "facebook.com") {
                        // get better quality profile picture 
                        let str = "https://graph.facebook.com/" + userData.providerData[fbIndex].uid + "/picture?type=large";
                        var firebaseURL = await this.setProfilePic(str, userData.uid);
                        // save fb uid and access token
                        var facebookUid: string = userData.providerData[fbIndex].uid;
                        var facebookAccessToken: string = fbCredential.accessToken;

                    } else { // google provider or other
                        var firebaseURL = await this.setProfilePic(userData.photoURL, userData.uid);
                        var facebookUid: string = "";
                        var facebookAccessToken: string = "";

                    }

                    var imageURL: string = firebaseURL;
                }
                await docRef.set({
                    displayName: displayName,
                    firstName: firstName ? firstName : '',
                    lastName: lastName ? lastName : '',
                    email: userData.email,
                    imageURL: imageURL,
                    address: "",
                    city: "",
                    country: "",
                    zipCode: "",
                    latitude: "",
                    longitude: "",
                    facebookUid: facebookUid,
                    facebookAccessToken: facebookAccessToken
                }, {
                        merge: true
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }

            // Initialize token to listen notification
            this.fcmService.getToken(this.getCurrentUserId());

            // Listen notification with tokenId
            this.fcmService.getPushNotification();
        }).catch(err => {
            console.log("err getting doc", err);
        })

    }

    capitalizeFirstLetter(string) {
        if (typeof string !== "undefined") {
            return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
        }
    }

    setProfilePic(photoURL, uid): Promise<any> {
        return new Promise<any>(async (resolve) => {
            try {
                var file: any = await this.urlToBlob(photoURL);
                const pictures = await firebase.storage().ref('users/avatars/' + 'user_avatar_' + uid);
                await pictures.put(file);
                var downloadUrl = await pictures.getDownloadURL();
                resolve(downloadUrl);

            } catch (e) {
                console.log("setProfilePic error", e);
                resolve("");
            }

        }).catch(err => {
            console.log("error, set imageURL as ''", err);
        })
    }

    urlToBlob(url) {
        return new Promise((resolve, reject) => {

            var xhr = new XMLHttpRequest();
            xhr.onerror = reject;
            xhr.onreadystatechange = () => {

                if (xhr.readyState === 4) {
                    if (xhr.status == 200) {
                        resolve(xhr.response);
                    } else {
                        throw new Error("xhr status != 200");
                    }
                }
            };
            xhr.open('GET', url);

            xhr.responseType = 'blob'; // convert type
            xhr.send();
        }).catch(err => {
            console.log("blob promise", err);
        })
    }

    doLogin(value) {
        return new Promise<any>((resolve, reject) => {
            firebase.auth().signInWithEmailAndPassword(value.email, value.password)
                .then(res => {
                    this.saveLoginData(res.user, null);
                    resolve(res);
                }, err => reject(err))
        })
    }

    doLogout() {
        return new Promise((resolve, reject) => {
            if (firebase.auth().currentUser) {
                this.fcmService.clearToken(this.userId);
                //this.fcmService.getPushNotification().unsubscribe();
                this.userId = null;
                this.cache_events = [];
                this.cache_chats = [];
                this.cache_chatslist = [];
                this.getStatusUser();
                this.cacheEventsObservable && this.cacheEventsObservable.unsubscribe();
                this.chatListObservable && this.chatListObservable.unsubscribe();
                this.networkOnDisconnect && this.networkOnDisconnect.unsubscribe();
                this.networkOnConnect && this.networkOnConnect.unsubscribe();
                this.afAuth.auth.signOut();
                this.gplus.logout();
                resolve();
            }
            else {
                reject();
            }
        });
    }

    getCurrentUser() {
        return new Promise<any>((resolve, reject) => {
            var user = firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                    resolve(user);
                } else {
                    reject('No user logged in');
                }
            })
        })
    }

    getCurrentUserObj() {
        return firebase.auth().currentUser;
    }

    /**
     * @description
     * This function will be generated an Token uid. 
     * It get also the new token if user log in other account
     */
    getCurrentUserId() {
        return !this.userId ? this.userId = firebase.auth().currentUser.uid : this.userId;
    }

    // This function return the status connected or not connected and make an observer to subscribe in future usages
    getStatusUser() {
        return new Observable<any>(observer => {
            this.userId ? (observer.next(true), observer.complete()) : (observer.next(false), observer.complete());
        });
    }

    getUserInfo(userId) {

        return new Promise<any>((resolve, reject) => {
            var docRef = this.db.collection("users").doc(userId);
            docRef.ref.get().then(doc => {
                let user = Object.assign({}, doc.data(), { firebaseId: userId });
                resolve(user);
            }).catch(err => {
                reject(err);
            });
        });
    }

    getUserCoords(userId) {

        return new Promise<any>((resolve, reject) => {
            this.getUserInfo(userId).then(user => {
                if (user) {
                    let userPos = { name: '', lat: '', lng: '' };
                    userPos.name = user.displayName;
                    userPos.lat = user.latitude;
                    userPos.lng = user.longitude;
                    resolve(user);
                }
            }).catch(err => {
                reject(err);
            });
        })
    }

    updateCurrentUser(value) {
        return new Promise<any>((resolve, reject) => {
            var user = firebase.auth().currentUser;
            user.updateProfile({
                displayName: value.name,
                photoURL: user.photoURL
            }).then(res => {
                resolve(res);
            }, err => reject(err))
        })
    }

    resetPassword(emailAddress) {
        return new Promise<any>((resolve, reject) => {
            firebase.auth().useDeviceLanguage();
            firebase.auth().sendPasswordResetEmail(emailAddress).then(function() {
                resolve(true);
            }).catch(function(error) {
                reject(error);
            });
        });
    }

    checkUserHasApp(email) {
        var userHasApp = this.db.collection("users/",
            ref => ref.where('email', '==', email));
        let obs = userHasApp.snapshotChanges().pipe(
            map(actions => {
                return actions.map(a => {
                    const data = a.payload.doc.data();
                    const id = a.payload.doc.id;
                    return { id, ...data };
                });
            })
        );
        return obs;
    }


    ////////////////////////
    // google auth
    // https://github.com/chemerisuk/cordova-plugin-firebase-authentication/issues/17
    // https://github.com/EddyVerbruggen/cordova-plugin-googleplus
    ////////////////////////


}
