import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {

  constructor(
    private db: AngularFirestore,
    private authService: AuthService
  ) { }

  getOneFavoritePlace(favoriId){

    return new Promise<any>((resolve, reject) => {
  
      var docRef = this.db.collection("users/" + this.authService.getCurrentUserId() + "/favorites/").doc(favoriId);
  
      docRef.ref.get().then(doc => {
        resolve (doc.data());
      }).catch(err => {
        reject(err);
      });
    });
  }
  
  getUserFavoritePlaces(){
  
    return this.db.collection("users/" + this.authService.getCurrentUserId() + "/favorites/").snapshotChanges();
  }
  
  updateFavoritePlace(place){
  
    return this.db.collection("users/" + this.authService.getCurrentUserId() + "/favorites/").doc(place.docId).update(place);
  }
  
  addUserFavoritePlace(place){
  
    return this.db.collection("users/" + this.authService.getCurrentUserId() + "/favorites/").add(place);
  }
  
  removeUserFavoritePlace(favoriteId){
  
    return this.db.collection("users/" + this.authService.getCurrentUserId() + "/favorites/").doc(favoriteId).delete();
  }
  
}