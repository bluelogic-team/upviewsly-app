import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'reset-password', loadChildren: './pages/reset-password/reset-password.module#ResetPasswordPageModule' },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
  { path: 'group-details/:id', loadChildren: './pages/group-details/group-details.module#GroupDetailsPageModule' },
  { path: 'group-details', loadChildren: './pages/group-details/group-details.module#GroupDetailsPageModule' },
  { path: 'edit-profile', loadChildren: './pages/edit-profile/edit-profile.module#EditProfilePageModule' },
  { path: 'map', loadChildren: './pages/map/map.module#MapPageModule' },
  { path: 'map/:groupId', loadChildren: './pages/map/map.module#MapPageModule' },
  { path: 'add-user', loadChildren: './pages/add-user/add-user.module#AddUserPageModule' },
  { path: 'edit-favorite', loadChildren: './pages/edit-favorite/edit-favorite.module#EditFavoritePageModule' },
  { path: 'chat/:id', loadChildren: './pages/chat/chat.module#ChatPageModule' },
  { path: 'add-friends', loadChildren: './pages/add-friends/add-friends.module#AddFriendsPageModule' },
  { path: 'events', loadChildren: './pages/events/events.module#EventsPageModule' },
  { path: 'add-participants', loadChildren: './pages/add-participants/add-participants.module#AddParticipantsPageModule' },
  { path: 'notifications', loadChildren: './pages/notifications/notifications.module#NotificationsPageModule' },
  { path: 'select-friends', loadChildren: './pages/select-friends/select-friends.module#SelectFriendsPageModule' },
  { path: 'add-transport', loadChildren: './pages/add-transport/add-transport.module#AddTransportPageModule' },
  { path: 'transport-details', loadChildren: './pages/transport-details/transport-details.module#TransportDetailsPageModule' },
  { path: 'friends', loadChildren: './pages/friends/friends.module#FriendsPageModule' },
  { path: 'add-location', loadChildren: './pages/add-location/add-location.module#AddLocationPageModule' },
  { path: 'add-date', loadChildren: './pages/add-date/add-date.module#AddDatePageModule' },
  { path: 'add-favorite-event', loadChildren: './pages/add-favorite-event/add-favorite-event.module#AddFavoriteEventPageModule' },
  { path: 'user-profile/:id', loadChildren: './pages/user-profile/user-profile.module#UserProfilePageModule' },
  { path: 'onboarding-info', loadChildren: './pages/onboarding-info/onboarding-info.module#OnboardingInfoPageModule' },
  { path: 'onboarding-username', loadChildren: './pages/onboarding-username/onboarding-username.module#OnboardingUsernamePageModule' },
  { path: 'onboarding-friend', loadChildren: './pages/onboarding-friend/onboarding-friend.module#OnboardingFriendPageModule' },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  { path: 'onboarding-add-image', loadChildren: './pages/onboarding-add-image/onboarding-add-image.module#OnboardingAddImagePageModule' },

  { path: '', redirectTo: '', pathMatch: 'full' }



];
@NgModule({
  imports: [
    RouterModule.forRoot(routes,
       //{ preloadingStrategy: PreloadAllModules }
       //{ enableTracing: true } // <-- debugging purposes only
       )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
